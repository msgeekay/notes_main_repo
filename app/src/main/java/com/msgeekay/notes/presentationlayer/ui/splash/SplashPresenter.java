package com.msgeekay.notes.presentationlayer.ui.splash;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.msgeekay.notes.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.notes.presentationlayer.MyApp;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 22/09/17.
 */

@InjectViewState
public class SplashPresenter extends MvpPresenter<SplashView>
{
  private ServiceCommunicationManager serviceCommunicationManager;

  @Inject
  public SplashPresenter(ServiceCommunicationManager serviceCommunicationManager)
  {
    this.serviceCommunicationManager = serviceCommunicationManager;

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void startTracking()
  {
    serviceCommunicationManager.startTracking();
  }
}
