package com.msgeekay.notes.presentationlayer.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;

import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.MyApp;

/**
 * Created by grigoriykatz on 25/09/17.
 */

public class DrawableUtils
{
  private final static String TAG = "DrawableUtils";

  public static Drawable getLabelDrawable(int color)
  {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
    {
      if (Parameters.DEBUG)
        Log.d(TAG, "CODE >= LOLLIPOP" );
      Drawable dd = ContextCompat.getDrawable(MyApp.getInstance(), R.drawable.white_label);

      DrawableCompat.setTint(dd, color);
      return dd;
    }
    else
    {
      if (Parameters.DEBUG)
        Log.d(TAG, "CODE < LOLLIPOP" );
      Drawable logo_q = getWrappedDrawable(MyApp.getInstance(), R.drawable.ic_bookmark_white_24dp);
      //int color = CommonUtils.getColor(App.getInstance(), R.color.white);

      DrawableCompat.setTint(logo_q, color);
      return logo_q;
    }
  }

  /**
   * Gets a reference to a given drawable and prepares it for use with tinting through.
   *
   * @param resId the resource id for the given drawable
   * @return a wrapped drawable ready fo use
   * with {@link android.support.v4.graphics.drawable.DrawableCompat}'s tinting methods
   * @throws Resources.NotFoundException
   */
  public static Drawable getWrappedDrawable(Context context, @DrawableRes int resId) throws Resources.NotFoundException {
    return DrawableCompat.wrap(ResourcesCompat.getDrawable(context.getResources(),
            resId, null));
  }
}
