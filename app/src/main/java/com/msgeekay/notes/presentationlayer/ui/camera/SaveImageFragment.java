package com.msgeekay.notes.presentationlayer.ui.camera;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.ui.camera.custom.ImageParameters;
import com.msgeekay.notes.presentationlayer.ui.camera.custom.ImageUtility;
import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.geometry.Point;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.ToneCurveSubfilter;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class SaveImageFragment extends Fragment
{

  public static final String TAG = SaveImageFragment.class.getSimpleName();
  public static final String BITMAP_KEY = "bitmap_byte_array";
  public static final String ROTATION_KEY = "rotation";
  public static final String IMAGE_INFO = "image_info";

  private ImageView photoImageView;
  private SeekBar seekBar;
  private int seekProgress = 0;
  private static Bitmap inBitmap;
  private static Bitmap outBitmap;

  private static final int REQUEST_STORAGE = 1;

  public static Fragment newInstance(byte[] bitmapByteArray, int rotation,
                                     @NonNull ImageParameters parameters) {
    Fragment fragment = new SaveImageFragment();

    Bundle args = new Bundle();
    args.putByteArray(BITMAP_KEY, bitmapByteArray);
    args.putInt(ROTATION_KEY, rotation);
    args.putParcelable(IMAGE_INFO, parameters);

    fragment.setArguments(args);
    return fragment;
  }

  public SaveImageFragment() {}

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_save_image, container, false);

    photoImageView = (ImageView) v.findViewById(R.id.photo);

    seekBar = (SeekBar) v.findViewById(R.id.seekBar);
    seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

    return v;
  }

  private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
          new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
              // TODO Auto-generated method stub
              seekProgress = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
              // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
              // TODO Auto-generated method stub
              applyBrightnessFilter();
            }
          };

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    int rotation = getArguments().getInt(ROTATION_KEY);
    byte[] data = getArguments().getByteArray(BITMAP_KEY);

    ImageParameters imageParameters = getArguments().getParcelable(IMAGE_INFO);

    if (imageParameters == null) {
      return;
    }


    int width = getResources().getDisplayMetrics().widthPixels;
    photoImageView.getLayoutParams().width = width;
    photoImageView.getLayoutParams().height = (int)(6*width/10);

    imageParameters.mIsPortrait =
            getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

    final View topView = view.findViewById(R.id.topView);
    if (imageParameters.mIsPortrait) {
      topView.getLayoutParams().height = imageParameters.mCoverHeight*2;
    } else {
      topView.getLayoutParams().width = imageParameters.mCoverWidth;
    }

    rotatePicture(rotation, data, photoImageView);

    view.findViewById(R.id.save_photo).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        savePicture();
      }
    });
    view.findViewById(R.id.share).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        saveAndShare();
      }
    });
  }

  private void rotatePicture(int rotation, byte[] data, ImageView photoImageView) {
    inBitmap = ImageUtility.decodeSampledBitmapFromByte(getActivity(), data);
//        Log.d(TAG, "original bitmap width " + bitmap.getWidth() + " height " + bitmap.getHeight());
    if (rotation != 0) {
      Bitmap oldBitmap = inBitmap;

      Matrix matrix = new Matrix();
      matrix.postRotate(rotation);

      inBitmap = Bitmap.createBitmap(
              oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, false
      );



      oldBitmap.recycle();
    }



    Point[] rgbKnots;
    rgbKnots = new Point[3];
    rgbKnots[0] = new Point(0, 0);
    rgbKnots[1] = new Point(175, 139);
    rgbKnots[2] = new Point(255, 255);




    applyBrightnessFilter();

  }

  private void applyBrightnessFilter() {
    Filter myFilter = new Filter(); //SampleFilters.getAweStruckVibeFilter();
    myFilter.addSubFilter(new BrightnessSubfilter(seekProgress));

    outBitmap =  inBitmap.copy(Bitmap.Config.ARGB_8888, true);
    outBitmap = myFilter.processFilter(outBitmap);

    photoImageView.setImageBitmap(outBitmap);
  }

  private void savePicture() {
    //need to request for permissions at runtime

    final View view = getView();
    if (view != null)
    {
      ImageView photoImageView = (ImageView) view.findViewById(R.id.photo);

      Bitmap bitmap = ((BitmapDrawable) photoImageView.getDrawable()).getBitmap();
      String photoPath = ImageUtility.savePicture(getActivity(), bitmap, true, null);

      ((CameraActivity) getActivity()).returnPhotoPaths(photoPath);
    }
  }

  private void saveAndShare() {
    final View view = getView();
    if (view != null)
    {
      ImageView photoImageView = (ImageView) view.findViewById(R.id.photo);

      Bitmap bitmap = ((BitmapDrawable) photoImageView.getDrawable()).getBitmap();
      String photoPath = ImageUtility.savePicture(getActivity(), bitmap, true, null);
      String thumbnailPath = photoPath; //ImageUtility.extractThumbnail(photoPath);

      ((CameraActivity) getActivity()).returnAndShare(photoPath, thumbnailPath);
    }

  }

}
