package com.msgeekay.notes.presentationlayer.ui.main.map;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;

import com.msgeekay.notes.presentationlayer.ui.common.views.StartOffsetItemDecoration;
import com.msgeekay.notes.presentationlayer.ui.main.MainView;
import com.msgeekay.notes.presentationlayer.ui.main.ModelCallbackProvider;
import com.msgeekay.notes.presentationlayer.ui.main.list.NotesAdapterList;
import com.msgeekay.notes.presentationlayer.utils.CustomUtils;
import android.transition.Scene;
//import com.transitionseverywhere.Scene;

import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.msgeekay.notes.R;
import com.msgeekay.notes.domainlayer.exception.ErrorBundle;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpFragment;
import com.msgeekay.notes.presentationlayer.ui.common.transitions.ScaleDownImageTransition;
import com.msgeekay.notes.presentationlayer.ui.common.transitions.TransitionUtils;
import com.msgeekay.notes.presentationlayer.ui.common.views.TranslateItemAnimator;
import com.msgeekay.notes.presentationlayer.ui.main.MainActivity;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.HorizontalRecyclerViewScrollListener;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.MapBitmapCache;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.MapsUtil;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.PulseOverlayLayout;

import java.util.List;

import butterknife.BindView;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class CommonFragment extends BaseMvpFragment implements CommonView, NotesAdapter.OnNoteClickListener,
        HorizontalRecyclerViewScrollListener.OnItemCoverListener
        //, OnMapReadyCallback
{
  public static final String TAG = CommonFragment.class.getName();

  @BindView(R.id.recyclerview) RecyclerView recyclerView;
  @BindView(R.id.container) FrameLayout containerLayout;
  @BindView(R.id.mapOverlayLayout) PulseOverlayLayout mapOverlayLayout;
  @BindView(R.id.recyclerviewList) RecyclerView recyclerViewList;
  @BindView(R.id.containerList) FrameLayout containerLayoutList;

  @InjectPresenter(type = PresenterType.LOCAL)
  CommonPresenter mPresenter;

  @ProvidePresenter(type = PresenterType.LOCAL)
  CommonPresenter provideCommonPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideCommonPresenter();
  }

  public class ViewVersion
  {
    public static final int viewVertical = 0;
    public static final int viewHorizontalWMap = 1;
  }

  private List<Note> notesList;
  private NotesAdapter notesAdapter;
  private NotesAdapterList notesAdapterList;
  private String currentTransitionName;
  private Scene detailsScene;
  private GoogleMap gMap;
  private int currentView = ViewVersion.viewVertical;
  private boolean isSwiping = false;
  private SnapHelper snapHelper = null;
  private boolean notifyAdaptersAfterSceneEnd = false;
  private HorizontalRecyclerViewScrollListener horizontalRecyclerViewScrollListener
                                                = new HorizontalRecyclerViewScrollListener(this);


  //FeedAdapter mFeedAdapter;

  public static Fragment newInstance(final Context ctx) {
    CommonFragment fragment = new CommonFragment();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
    {
      ScaleDownImageTransition transition = new ScaleDownImageTransition(ctx, MapBitmapCache.instance().getBitmap());
      transition.addTarget(ctx.getString(R.string.mapPlaceholderTransition));
      transition.setDuration(600);
      fragment.setEnterTransition(transition);
    }
    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_common, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);

    setupRecyclerView();

    setupMapAndDataSync();
//    setupData();
//
//    Handler h = new Handler();
//    h.postDelayed(new Runnable()
//    {
//      @Override
//      public void run()
//      {
//
//        setupMapFragment();
//      }
//    }, 2500);


  }

  @Override
  public void onStop()
  {
    if (mPresenter != null)
      mPresenter.stop();
    super.onStop();
  }

  /** Step 1 **/
  private void setupRecyclerView()
  {
    recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext(),
            LinearLayoutManager.VERTICAL,
            false));
    StartOffsetItemDecoration itemDecoration = new StartOffsetItemDecoration(CustomUtils.dpToPx(60));
    recyclerViewList.addItemDecoration(itemDecoration);
    notesAdapterList = new NotesAdapterList(this, getActivity(), recyclerViewList);

    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
            LinearLayoutManager.HORIZONTAL,
            false));
    notesAdapter = new NotesAdapter(this, getActivity());

    if (getActivity() != null && getActivity() instanceof ModelCallbackProvider)
    {
      notesAdapter.setButtonsClickListener(((ModelCallbackProvider)getActivity()).provideModelCallback());
      notesAdapterList.setButtonsClickListener(((ModelCallbackProvider)getActivity()).provideModelCallback());
    }
  }

  private void setupData()
  {
    if (mPresenter != null)
      mPresenter.getNotes();
  }

  private void setupMapFragment() {

//    if (mPresenter != null)
//      mPresenter.initGoogleMap(mapFragment);
    //.getMapAsync(this);
  }

  public void refreshData()
  {
    if (mPresenter != null)
      mPresenter.complexInitNotesRefresh();
      //mPresenter.getNotes();
  }

  private void setupMapAndDataSync()
  {
    SupportMapFragment mapFragment =
            ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.mapFragment));
    if (mPresenter != null)
      mPresenter.complexInit(mapFragment);
  }

  /** Step 2 **/
  @Override
  public void setNotes(List<Note> noteList)
  {
    notesAdapter.setNoteList(noteList);
    notesAdapterList.setNoteList(noteList);

    if (detailsScene != null)
      notifyAdaptersAfterSceneEnd = true;
    else
      notifyAdapters();


//    if (recyclerView.getAdapter() != null && noteList != null && noteList.size() > 0)
//    {
//      recyclerView.addOnScrollListener(horizontalRecyclerViewScrollListener);
//    }

    //notesAdapter.notifyDataSetChanged();
  }

  private void notifyAdapters()
  {
    if (notesAdapter != null)
      notesAdapter.notifyDataSetChanged();
    if (notesAdapterList != null)
      notesAdapterList.notifyDataSetChanged();
  }

  /** Step 3 **/
  public void onMapReady(GoogleMap googleMap)
  {
    this.gMap = googleMap;
    this.gMap.setMaxZoomPreference(15f);
    postMapSetup(googleMap);
  }

  private void postMapSetup(GoogleMap googleMap)
  {
    mapOverlayLayout.setupMap(googleMap);
    setupGoogleMap(googleMap);
    addDataToRecyclerView();
  }

  private void setupGoogleMap(final GoogleMap googleMap)
  {
    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
            mPresenter.provideLatLngBoundsForAllPlaces(),
            MapsUtil.calculateWidth(getActivity().getWindowManager()),
            MapsUtil.calculateHeight(getActivity().getWindowManager(),
                    getResources().getDimensionPixelSize(R.dimen.map_margin_bottom)), 150));

    googleMap.setOnMapLoadedCallback(() ->
    {
      googleMap.snapshot(mPresenter::saveBitmap);
    });

    googleMap.snapshot(mPresenter::saveBitmap);
    mPresenter.moveMapAndAddMarker();
  }

  private void addDataToRecyclerView()
  {
    recyclerViewList.setAdapter(notesAdapterList);
    notesAdapterList.setNoteList(mPresenter.getLastNoteList());

    recyclerView.setItemAnimator(new TranslateItemAnimator());
    recyclerView.setAdapter(notesAdapter);
    notesAdapter.setNoteList(mPresenter.getLastNoteList());

    if (snapHelper != null)
    {
      recyclerView.clearOnScrollListeners();
      recyclerView.setOnFlingListener(null);
    }
    snapHelper = new PagerSnapHelper();
    //recyclerView.setOnFlingListener(null);
    snapHelper.attachToRecyclerView(recyclerView);

    /** moved to setData method **/
    //recyclerView.addOnScrollListener(horizontalRecyclerViewScrollListener);
  }

  /** End of Stp 3 **/

  @Override
  public void hideViewLoading()
  {

  }

  @Override
  public void showErrorMessage(ErrorBundle errorBundle)
  {
    Toast.makeText(MyApp.getInstance(), errorBundle.getErrorMessage(), Toast.LENGTH_SHORT).show();
  }


  @Override
  public void onBackPressed()
  {
    if (detailsScene != null) {
      mPresenter.onBackPressedWithScene();
    } else {
      ((MainActivity) getActivity()).superOnBackPressed();
    }
  }

  @Override
  public void onBackPressedWithScene(LatLngBounds latLngBounds)
  {
    int childPosition = TransitionUtils.getItemPositionFromTransition(currentTransitionName);
    if (currentView == ViewVersion.viewHorizontalWMap)
    {
      DetailsLayout.hideScene(getActivity(), containerLayout, getSharedViewByPosition(childPosition),
                              currentTransitionName, false);
      notifyLayoutAfterBackPress(childPosition);
      mapOverlayLayout.onBackPressed(latLngBounds);
    }
    else
    {
      DetailsLayout.hideScene(getActivity(), containerLayoutList, getSharedViewByPosition(childPosition),
                              currentTransitionName, true);

      /** waiting 'till the end of hiding animation ~300 msec **/
      Handler h = new Handler();
      h.postDelayed(new Runnable()
      {
        @Override
        public void run()
        {
          notifyLayoutAfterBackPress(childPosition);
        }
      }, 300);

    }
    detailsScene = null;
    if (notifyAdaptersAfterSceneEnd)
    {
      notifyAdaptersAfterSceneEnd = false;
      notifyAdapters();
    }

  }

  private View getSharedViewByPosition(final int childPosition)
  {
    RecyclerView rv;
    if (currentView != ViewVersion.viewHorizontalWMap)
      rv = recyclerViewList;
    else
      rv = recyclerView;
    for (int i = 0; i < rv.getChildCount(); i++) {
      if (childPosition == rv.getChildAdapterPosition(rv.getChildAt(i))) {
        return rv.getChildAt(i);
      }
    }
    return null;
  }

  private void notifyLayoutAfterBackPress(final int childPosition)
  {
    if (currentView == ViewVersion.viewHorizontalWMap)
    {
      containerLayout.removeAllViews();
      containerLayout.addView(recyclerView);
      recyclerView.requestLayout();
      notesAdapter.notifyItemChanged(childPosition);
    }
    else
    {
      containerLayoutList.removeAllViews();
      containerLayoutList.addView(recyclerViewList);
      recyclerViewList.requestLayout();
      notesAdapterList.notifyItemChanged(childPosition);
    }

  }

  @Override
  public void moveMapAndAddMaker(LatLngBounds latLngBounds)
  {
    mapOverlayLayout.moveCamera(latLngBounds);
    mapOverlayLayout.setOnCameraIdleListener(() -> {
      List<Note> noteList = mPresenter.getLastNoteList();
      if (noteList != null)
        for (int i = 0; i < noteList.size(); i++) {
          mapOverlayLayout.createAndShowMarker(i, noteList.get(i).getNoteLocation());
        }
      mapOverlayLayout.setOnCameraIdleListener(null);
    });
    mapOverlayLayout.setOnCameraMoveListener(mapOverlayLayout::refresh);
  }

  @Override
  public void updateMapZoomAndRegion(LatLng northeastLatLng, LatLng southwestLatLng)
  {
    getActivity().runOnUiThread(() -> {
      mapOverlayLayout.animateCamera(new LatLngBounds(southwestLatLng, northeastLatLng));
      mapOverlayLayout.setOnCameraIdleListener(() -> mapOverlayLayout.drawStartAndFinishMarker());
    });
  }



  @Override
  public void onNoteClicked(View sharedView, String transitionName, int position, Note note)
  {
    try
    {
      currentTransitionName = transitionName;
      if (currentView == ViewVersion.viewHorizontalWMap)
      {
        detailsScene = DetailsLayout.showScene(getActivity(), containerLayout, sharedView, transitionName,
                mPresenter.getLastNoteList().get(position), false);
        drawRoute(position);
        hideAllMarkers();
      }
      else
      {
        detailsScene = DetailsLayout.showScene(getActivity(), containerLayoutList, sharedView, transitionName,
                mPresenter.getLastNoteList().get(position), true);
      }

      if (getActivity() != null)
        ((BaseMvpActivity)getActivity()).addToStack(this);

    }
    catch (Exception ex)
    {
      Log.d(TAG, ex.toString());
      //do nothing
    }
  }

  private void drawRoute(final int position) {
    //mPresenter.drawRoute(mapOverlayLayout.getCurrentLatLng(), position);
  }

  private void hideAllMarkers() {
    mapOverlayLayout.setOnCameraIdleListener(null);
    mapOverlayLayout.hideAllMarkers();
  }

  @Override
  public void onItemCover(int position)
  {
    mapOverlayLayout.showMarker(position);
  }

  public void swapViews()
  {
    if (isSwiping)
      return;

    isSwiping = true;
    if (currentView == ViewVersion.viewVertical)
    {
      containerLayoutList.animate()
                          .setDuration(300)
                          .alpha(0)
                          .setInterpolator(new AccelerateInterpolator())
                          .setListener(new AnimatorListenerAdapter()
                          {
                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                              containerLayoutList.setVisibility(View.GONE);
                              currentView = ViewVersion.viewHorizontalWMap;
                              isSwiping = false;
                            }
                          })
                          .start();
    }
    else
    {
      containerLayoutList.animate()
                          .setDuration(300)
                           .alpha(1)
                           .setInterpolator(new AccelerateInterpolator())
                           .setListener(new AnimatorListenerAdapter()
                           {
                             @Override
                             public void onAnimationEnd(Animator animation)
                             {
                               currentView = ViewVersion.viewVertical;
                               isSwiping = false;
                             }

                             @Override
                             public void onAnimationStart(Animator animation)
                             {
                               containerLayoutList.setVisibility(View.VISIBLE);
                             }
                           })
                           .start();
    }
  }

  public boolean getIsSwaping()
  {
    return isSwiping;
  }
}
