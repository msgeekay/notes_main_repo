package com.msgeekay.notes.presentationlayer.ui.common.anim.spring.springyRecyclerView;

/**
 * Created by grigoriykatz on 15/08/17.
 */

public enum SpringyAdapterAnimationType
{
  SLIDE_FROM_BOTTOM,
  SLIDE_FROM_RIGHT,
  SLIDE_FROM_LEFT,
  SCALE
}