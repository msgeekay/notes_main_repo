package com.msgeekay.notes.presentationlayer.ui.common.mvp;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatFragment;

import butterknife.ButterKnife;
import icepick.Icepick;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public abstract class BaseMvpFragment extends MvpAppCompatFragment
{
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Icepick.restoreInstanceState(this, savedInstanceState);
  }

  @Override
  public void onSaveInstanceState(Bundle bundle) {
    super.onSaveInstanceState(bundle);
    Icepick.saveInstanceState(this, bundle);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.bind(this, view);
  }

  public abstract void onBackPressed();

  /**
   * @return true is already have permission
   */
  protected boolean askPermission(String permission, Integer requestCode) {
    if (ActivityCompat.checkSelfPermission(getContext(), permission) != PackageManager.PERMISSION_GRANTED) {

      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

        //This is called if user has denied the permission before
        //In this case I am just asking the permission again
        requestPermissions(new String[]{permission}, requestCode);
      } else {
        requestPermissions(new String[]{permission}, requestCode);
      }

      return false;
    }

    return true;
  }
}
