package com.msgeekay.notes.presentationlayer.ui.main.list;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.msgeekay.notes.R;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.ui.common.anim.spring.springyRecyclerView.SpringyAdapterAnimationType;
import com.msgeekay.notes.presentationlayer.ui.common.anim.spring.springyRecyclerView.SpringyAdapterAnimator;
import com.msgeekay.notes.presentationlayer.ui.common.transitions.TransitionUtils;
import com.msgeekay.notes.presentationlayer.ui.common.views.SettingsLayout;
import com.msgeekay.notes.presentationlayer.ui.main.map.NotesAdapter;
import com.msgeekay.notes.presentationlayer.utils.CustomUtils;
import com.msgeekay.notes.presentationlayer.utils.DrawableUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by grigoriykatz on 22/09/17.
 */

public class NotesAdapterList extends RecyclerView.Adapter<NotesAdapterList.NoteViewHolder> {

  private final NotesAdapter.OnNoteClickListener listener;
  private SpringyAdapterAnimator mAnimator;
  private Context context;
  private List<Note> noteList = new ArrayList<>();
  private static List<Long> backList = new ArrayList<>();

  private static final int ANIMATED_ITEMS_COUNT = 4;
  private int lastAnimatedPosition = -1;
  private Context animContext;

  private SettingsLayout.ButtonsClickListener mButtonsClickListeners;

  public NotesAdapterList(NotesAdapter.OnNoteClickListener listener, Context context,
                          RecyclerView container)
  {
    this.listener = listener;
    this.context = context;
    mAnimator = new SpringyAdapterAnimator(container);
    mAnimator.setSpringAnimationType(SpringyAdapterAnimationType.SLIDE_FROM_BOTTOM);
    mAnimator.addConfig(85,15);
  }

  private void runEnterAnimation(View view, int position) {
    if (position >= ANIMATED_ITEMS_COUNT - 1) {
      return;
    }

    if (position > lastAnimatedPosition) {
      lastAnimatedPosition = position;
      view.setTranslationY(CustomUtils.getScreenHeight(animContext));
      //view.setTranslationX(CustomUtils.getScreenWidth(context));
      view.animate()
              .translationY(0)
              //.translationX(0)
              .setInterpolator(new DecelerateInterpolator(3.f))
              .setDuration(700)
              .start();
    }
  }

  @Override
  public NoteViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
  {
    animContext = parent.getContext();
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_note_list, parent, false);
    mAnimator.onSpringItemCreate(itemView);
    return new NoteViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final NoteViewHolder holder, final int position)
  {
    //runEnterAnimation(holder.itemView, position);

    Note note = noteList.get(position);
    holder.title.setText(note.getNoteTitle());
    holder.body.setText(String.valueOf(note.getNoteBody()));
    if (note.getImageURI() == null
            || (note.getImageURI() != null && note.getImageURI().length() == 0))
      holder.placePhoto.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.default_profile_background));
    else
      Picasso.with(context)
              .load(new File(note.getImageURI()))
              .placeholder(R.drawable.default_profile_background)
              .error(R.drawable.default_profile_background)
              //.fit()
              //.centerInside()
              .into(holder.placePhoto);
    holder.root.setOnClickListener(view ->
            listener.onNoteClicked(holder.root, TransitionUtils.getRecyclerViewTransitionName(position),
                    position, noteList.get(position)));
    holder.label.setImageDrawable(DrawableUtils.getLabelDrawable(note.getPriority().getColor()));

    SettingsLayout.SettingsMovement settingsMovement = new SettingsLayout.SettingsMovement()
    {
      @Override
      public void onSettingsHide()
      {
        backList.remove(Long.valueOf(note.getNoteId()));
      }

      @Override
      public void onSettingsShow()
      {
        backList.add(Long.valueOf(note.getNoteId()));
      }
    };

    holder.mSettingsLayout.setButtonsClickListener(mButtonsClickListeners);
    holder.mSettingsLayout.setIsHidden(!backList.contains(Long.valueOf(note.getNoteId())));
    holder.mSettingsLayout.setSettingsMovement(settingsMovement);
    holder.mSettingsLayout.setNote(note);

    holder.extra_menu.setOnTouchListener(new View.OnTouchListener()
    {
      @Override
      public boolean onTouch(View view, MotionEvent motionEvent)
      {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
        {
          holder.mSettingsLayout.animateQ2(note, motionEvent);
        }
        return true;
      }
    });

    mAnimator.onSpringItemBind(holder.itemView, position);
  }

  @Override
  public int getItemCount() {
    return noteList == null ? 0 : noteList.size();
  }

  @Override
  public long getItemId(int position)
  {
    return position;
  }

  public void setButtonsClickListener(SettingsLayout.ButtonsClickListener listener)
  {
    this.mButtonsClickListeners = listener;
  }

  public void setNoteList(List<Note> noteList) {
    this.noteList = noteList;
    //notifyDataSetChanged();
//    for (int i = 0; i < noteList.size(); i++) {
//      notifyItemInserted(i);
//    }
  }

//  interface OnNoteClickListener {
//    void onNoteClicked(View sharedView, String transitionName,
//                       final int position, Note note);
//  }

  static class NoteViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.title) TextView title;
    @BindView(R.id.body) TextView body;
    @BindView(R.id.root) CardView root;
    @BindView(R.id.headerImage) ImageView placePhoto;
    @BindView(R.id.number) ImageView label;
    @BindView(R.id.setts) SettingsLayout mSettingsLayout;
    @BindView(R.id.extra_menu) View extra_menu;

    NoteViewHolder(final View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
