package com.msgeekay.notes.presentationlayer.ui.main;

import com.msgeekay.notes.presentationlayer.ui.common.views.SettingsLayout;

/**
 * Created by grigoriykatz on 26/09/17.
 */

public interface ModelCallbackProvider
{
  SettingsLayout.ButtonsClickListener provideModelCallback();
}
