package com.msgeekay.notes.presentationlayer.ui.common.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.animation.DynamicAnimation;
import android.support.animation.FloatPropertyCompat;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.msgeekay.notes.R;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.utils.DrawableUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import io.codetail.widget.RevealFrameLayout;

/**
 * Created by grigoriykatz on 16/09/17.
 */

public class SettingsLayout extends RevealFrameLayout
{
  public interface SettingsMovement
  {
    void onSettingsHide();
    void onSettingsShow();
  }

  public interface ButtonsClickListener
  {
    void onBtn1Clicked(Note note);
    void onBtn2Clicked(Note note);
    void onBtn3Clicked(Note note);
    void onBtn4Clicked(Note note);
    void onBtn5Clicked(Note note);

    void onBtn6Clicked(Note note);
    void onBtn7Clicked(Note note);
  }

  final static int SLOW_DURATION = 400;
  final static int FAST_DURATION = 200;

  @BindView(R.id.reveal_item) FrameLayout mRevealView;
  @BindView(R.id.settings_close) View settings_close;
  @BindView(R.id.settings_close_img) ImageView settings_close_img;
  @BindView(R.id.settings_personal) View settings_personal;

  @BindView(R.id.btn1) View view1;
  @BindView(R.id.btn2) View view2;
  @BindView(R.id.btn3) View view3;
  @BindView(R.id.btn4) View view4;
  @BindView(R.id.btn5) View view5;

  @BindView(R.id.btn6) View view6;
  @BindView(R.id.btn7) View view7;

  private boolean hidden = true;
  private boolean isFocused = false;
  private float startMotionX = 0;
  private float startMotionY = 0;
  private SettingsMovement settingsMovement = null;
  private List<View> views = new ArrayList<>();
  private ButtonsClickListener buttonsClickListener = null;

  private Note note;

  public SettingsLayout(Context context)
  {
    super(context);
    initComponent();
  }

  public SettingsLayout(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    initComponent();
  }

  public SettingsLayout(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    initComponent();
  }

  View _view;
  private void initComponent() {
    View view = inflate(getContext(), R.layout.layout_settings, this);
    _view = view;
    ButterKnife.bind(this, view);

    settings_close_img.setImageDrawable(getCloseDrawable());
    settings_close.setOnTouchListener(new View.OnTouchListener()
    {
      @Override
      public boolean onTouch(View v, MotionEvent event)
      {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
          animateQ2(null, event);
        }
        return true;
      }
    });

    views.add(view1);

    views.add(view4);
    views.add(view5);
    views.add(view2);
    views.add(view3);

    views.add(view7);
    views.add(view6);
  }

  @Override
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
  }

  @OnTouch({
          R.id.btn1,
          R.id.btn2,
          R.id.btn3, R.id.btn4, R.id.btn5,
          R.id.btn6, R.id.btn7
  })
  boolean onBtnTouched(View v, MotionEvent event)
  {
    if (event.getAction() == MotionEvent.ACTION_DOWN)
    {
      if (v == settings_close)
      {
        animateQ2(null, event);

      }
      else if (v == view1)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn1Clicked(note);
      }
      else if (v == view2)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn2Clicked(note);
      }
      else if (v == view3)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn3Clicked(note);
      }
      else if (v == view4)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn4Clicked(note);
      }
      else if (v == view5)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn5Clicked(note);
      }
      else if (v == view6)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn6Clicked(note);
      }
      else if (v == view7)
      {
        animateQ2(null, event);
        if (buttonsClickListener != null)
          buttonsClickListener.onBtn7Clicked(note);
      }
    }

    return true;
  }


  private View.OnTouchListener onTouchListener = new View.OnTouchListener()
  {
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
      if (event.getAction() == MotionEvent.ACTION_DOWN)
      {
        if (v == settings_close)
        {
          animateQ2(null, event);
        }
        else if (v == view1)
        {

        }
        else if (v == view2)
        {

        }
        else if (v == view3)
        {

        }
        else if (v == view4)
        {

        }
        else if (v == view5)
        {

        }
      }
      return true;
    }
  };

  public void setNote(Note n)
  {
    this.note = n;
  }

  public void setIsHidden(boolean isHidden)
  {
    this.hidden = isHidden;

    if (this.hidden)
      setVisibility(INVISIBLE);
    else
      setVisibility(VISIBLE);
  }

  public void setButtonsClickListener(ButtonsClickListener listener)
  {
    this.buttonsClickListener = listener;
  }

  @Override
  public void setVisibility(int visibility)
  {
    //super.setVisibility(visibility);
    mRevealView.setVisibility(visibility);
  }

  @Override
  public int getVisibility()
  {
    return mRevealView.getVisibility();
  }

  public void setSettingsMovement(SettingsMovement settingsMovement)
  {
    this.settingsMovement = settingsMovement;
  }

  private void onSettingsHide()
  {
    if (settingsMovement != null)
      settingsMovement.onSettingsHide();
  }

  private void onSettingsShow()
  {
    if (settingsMovement != null)
      settingsMovement.onSettingsShow();
  }

  public void animateButtons()
  {
    FloatPropertyCompat<View> scale = new FloatPropertyCompat<View>("")
    {
      @Override
      public float getValue(View view)
      {
        return view.getScaleX();
      }

      @Override
      public void setValue(View view, float value)
      {
        view.setScaleX(value);
        view.setScaleY(value);
      }
    };

    for (int i = 6; i > -1; i--)
    {
      int delay = 0;
      if (i == 3 ||  i == 1)
        delay = 100;
      if (i == 0 || i == 6)
        delay = 200;
      if (i == 5)
        delay = 300;

      View v = views.get(i);
      v.postDelayed(new Runnable()
      {
        @Override
        public void run()
        {
          SpringAnimation anim = new SpringAnimation(v, scale, 1f);
          anim.getSpring()
                  .setStiffness(SpringForce.STIFFNESS_LOW)
                  .setDampingRatio(0.35f);
          anim.setMinimumVisibleChange(DynamicAnimation.MIN_VISIBLE_CHANGE_SCALE)
                  .setStartVelocity(10f)
                  .start();
        }
      }, delay);

    }
  }

  private void anchorFocus()
  {
    isFocused = mRevealView.requestFocus();
    if (isFocused)
      mRevealView.setOnFocusChangeListener(new View.OnFocusChangeListener()
      {
        @Override
        public void onFocusChange(View v, boolean hasFocus)
        {
          if (!hasFocus)
          {
            MotionEvent me =
                    MotionEvent.obtain(10, 10, MotionEvent.ACTION_DOWN, startMotionX, startMotionY, 0);
            animateQ2(null, me);
          }
        }
      });
  }

  private void deAnchorFocus()
  {
    mRevealView.setOnFocusChangeListener(null);
    isFocused = false;
  }

  public void animateQ2(final Note n, MotionEvent e)
  {

    startMotionX = e.getX();
    startMotionY = e.getY();
    int px = mRevealView.getWidth() - (int)e.getX();
    int py = mRevealView.getHeight() - (int)e.getY();

    int cx = (mRevealView.getLeft() + mRevealView.getRight());
//                int cy = (mRevealView.getTop() + mRevealView.getBottom())/2;
    int cy = mRevealView.getTop();
    int radius = Math.max(mRevealView.getWidth(), mRevealView.getHeight());
    float radius2 = (float) Math.hypot(mRevealView.getWidth() / 2f, mRevealView.getHeight() / 2f);

    /***/

    final float finalRadius =
            (float) Math.hypot(mRevealView.getWidth() / 2f, mRevealView.getHeight() / 2f) + hypo(
                    mRevealView, e);

    SupportAnimator revealAnimator =
            ViewAnimationUtils.createCircularReveal(mRevealView,
                    px, py
                    //(int) e.getX(), (int) e.getY()
                    , 0, finalRadius
                    //, View.LAYER_TYPE_HARDWARE
            );

    revealAnimator.setDuration(SLOW_DURATION);
    revealAnimator.setInterpolator(new FastOutLinearInInterpolator());
    //revealAnimator.start();

    SupportAnimator animator_reverse = ((SupportAnimator)revealAnimator).reverse();

    SupportAnimator animator_reverse2 =
            ViewAnimationUtils.createCircularReveal(mRevealView,
                    px, py
                    //(int) e.getX(), (int) e.getY()
                    , finalRadius, 0
            );
    animator_reverse2.setDuration(SLOW_DURATION);
    animator_reverse2.setInterpolator(new LinearOutSlowInInterpolator());

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP || 1==1)
    {
      if (hidden) {
        mRevealView.setVisibility(View.VISIBLE);
        revealAnimator.addListener(new SupportAnimator.AnimatorListener()
        {
          @Override
          public void onAnimationStart()
          {
            animateButtons();
          }

          @Override
          public void onAnimationEnd()
          {
            hidden = false;
            onSettingsShow();
            //animateButtons();
            anchorFocus();
          }

          @Override
          public void onAnimationCancel()
          {

          }

          @Override
          public void onAnimationRepeat()
          {

          }
        });
        revealAnimator.start();



      } else {
        animator_reverse2.addListener(new SupportAnimator.AnimatorListener() {
          @Override
          public void onAnimationStart() {
            if (isFocused)
              deAnchorFocus();
          }

          @Override
          public void onAnimationEnd() {
            mRevealView.setVisibility(View.INVISIBLE);
            hidden = true;

            //backList.remove(q.getId());
            onSettingsHide();

          }

          @Override
          public void onAnimationCancel() {

          }

          @Override
          public void onAnimationRepeat() {

          }
        });
        animator_reverse2.start();

      }
    } else {
      if (hidden) {
        //Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, 0, radius2);
        Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, (int) e.getX(), (int) e.getY(), 0, finalRadius);
        mRevealView.setVisibility(View.VISIBLE);
        anim.setDuration(SLOW_DURATION);
        anim.addListener(new AnimatorListenerAdapter()
        {
          @Override
          public void onAnimationEnd(Animator animation)
          {
            super.onAnimationEnd(animation);
            hidden = false;

            //backList.add(q.getId());
            onSettingsShow();
            animateButtons();
          }
        });
        anim.start();


      } else {
        //Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, cx, cy, radius2, 0);
        Animator anim = android.view.ViewAnimationUtils.createCircularReveal(mRevealView, (int) e.getX(), (int) e.getY(), finalRadius, 0);
        anim.setDuration(SLOW_DURATION);
        anim.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            mRevealView.setVisibility(View.INVISIBLE);
            hidden = true;

            //backList.remove(q.getId());
            onSettingsHide();
          }
        });
        anim.start();

      }
    }
  }

  private float hypo(View view, MotionEvent event)
  {
    Point p1 = new Point((int) event.getX(), (int) event.getY());
    Point p2 = new Point(view.getWidth() / 2, view.getHeight() / 2);

    return (float) Math.sqrt(Math.pow(p1.y - p2.y, 2) + Math.pow(p1.x - p2.x, 2));
  }

  private Drawable getCloseDrawable()
  {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
    {
      Log.d(getClass().getName(), "CODE >= LOLLIPOP" );
      return ContextCompat.getDrawable(MyApp.getInstance(), R.drawable.clear_white);
    }
    else
    {
      Log.d(getClass().getName(), "CODE < LOLLIPOP" );
      Drawable logo_q = DrawableUtils.getWrappedDrawable(MyApp.getInstance(), R.drawable.ic_clear_black_24dp);
      int color = ContextCompat.getColor(MyApp.getInstance(), R.color.white);
      //mLeftActionIconColor = color;
      //mMenuBtnDrawable.setColor(color);
      DrawableCompat.setTint(logo_q, color);
      return logo_q;
    }
  }
}
