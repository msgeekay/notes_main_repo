package com.msgeekay.notes.presentationlayer.di.common.modules;

import com.msgeekay.notes.datalayer.net.Api;
import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.domainlayer.interactor.CreateNote;
import com.msgeekay.notes.domainlayer.interactor.ExportNote;
import com.msgeekay.notes.domainlayer.interactor.GetGoogleMapFullyReady;
import com.msgeekay.notes.domainlayer.interactor.GetNotesList;
import com.msgeekay.notes.domainlayer.interactor.UpdateNote;
import com.msgeekay.notes.domainlayer.interactor.UseCase;
import com.msgeekay.notes.domainlayer.repo.Repository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grigoriykatz on 18/05/17.
 */

@Module
public class UseCasesModule
{
  @Provides
  UseCase provideGetNotesListUseCase(Repository r, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    return new GetNotesList(r, threadExecutor, postExecutionThread);
  }

  @Provides
  UseCase provideCreateNoteUseCase(Repository r, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    return new CreateNote(r, threadExecutor, postExecutionThread);
  }

  @Provides
  UseCase provideUpdateNoteUseCase(Repository r, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    return new UpdateNote(r, threadExecutor, postExecutionThread);
  }

  @Provides
  UseCase provideGetGoogleMapFullyReadyUseCase(Repository r, ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread) {
    return new GetGoogleMapFullyReady(threadExecutor, postExecutionThread);
  }

  @Provides
  UseCase provideExportNoteUseCase(Repository r, ThreadExecutor threadExecutor,
                                   PostExecutionThread postExecutionThread) {
    return new ExportNote(r, threadExecutor, postExecutionThread);
  }

}
