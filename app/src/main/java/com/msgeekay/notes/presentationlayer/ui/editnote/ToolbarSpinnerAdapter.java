package com.msgeekay.notes.presentationlayer.ui.editnote;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.msgeekay.notes.R;
import com.msgeekay.notes.model.NotePriority;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by grigoriykatz on 22/09/17.
 */

public class ToolbarSpinnerAdapter extends BaseAdapter
{
  private List<NotePriority> mItems = new ArrayList<>();
  private LayoutInflater mInflater;
  private Context mContext;

  public ToolbarSpinnerAdapter(Context context) {
    mInflater = LayoutInflater.from(context);
    mContext = context;
  }

  public void clear() {
    mItems.clear();
  }

  public void addItem(NotePriority yourObject) {
    mItems.add(yourObject);
  }

  public void addItems(List<NotePriority> yourObjectList) {
    mItems.addAll(yourObjectList);
  }

  @Override
  public int getCount() {
    return mItems.size();
  }

  @Override
  public Object getItem(int position) {
    return mItems.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getDropDownView(int position, View view, ViewGroup parent) {
    if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
      view = mInflater.inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
      view.setTag("DROPDOWN");
    }

    TextView textView = (TextView) view.findViewById(R.id.text1);
    textView.setText(getTitle(position));
    CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.image1);
    ColorDrawable cd = new ColorDrawable(mItems.get(position).getColor());
    circleImageView.setImageDrawable(cd);
    return view;
  }

  @Override
  public View getView(int position, View view, ViewGroup parent) {
    if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
      view = mInflater.inflate(R.layout.toolbar_spinner_item_actionbar, parent, false);
      view.setTag("NON_DROPDOWN");
    }
    TextView textView = (TextView) view.findViewById(R.id.text1);
    //textView.setText(getTitle(position));
    CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.image1);
    ColorDrawable cd = new ColorDrawable(mItems.get(position).getColor());
    circleImageView.setImageDrawable(cd);
    return view;
  }

  private String getTitle(int position) {
    return position >= 0 && position < mItems.size() ? mItems.get(position).name() : "";
  }
}
