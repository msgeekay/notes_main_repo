package com.msgeekay.notes.presentationlayer.utils;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class Parameters
{
  public static final double defaultLat1 = 55.771052D;
  public static final double defaultLon1 = 37.593438D;

  public static final double defaultLat2 = 55.765596D;
  public static final double defaultLon2 = 37.599446D;

  public static final boolean DEBUG = true;
}
