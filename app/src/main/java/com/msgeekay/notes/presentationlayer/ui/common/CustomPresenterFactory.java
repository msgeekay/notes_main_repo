package com.msgeekay.notes.presentationlayer.ui.common;

import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.ui.editnote.EditPresenter;
import com.msgeekay.notes.presentationlayer.ui.main.map.CommonPresenter;
import com.msgeekay.notes.presentationlayer.ui.main.MainPresenter;
import com.msgeekay.notes.presentationlayer.ui.splash.SplashPresenter;


import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/05/17.
 */

public class CustomPresenterFactory //implements PresenterFactory //extends PresenterStore
{
  @Inject
  SplashPresenter splashPresenter;

  @Inject
  MainPresenter mainPresenter;

  @Inject
  CommonPresenter commonPresenter;

  @Inject
  EditPresenter editPresenter;

  public CustomPresenterFactory()
  {
    MyApp.get().getApplicationComponent().inject(this);
  }

  public SplashPresenter provideSplashPresenter()
  {
    return splashPresenter;
  }

  public MainPresenter provideMainPresenter()
  {
    return mainPresenter;
  }

  public CommonPresenter provideCommonPresenter() { return commonPresenter; }

  public EditPresenter provideEditPresenter() { return editPresenter; }

}