package com.msgeekay.notes.presentationlayer.ui.main.map.custom;

import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func2;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public final class OnMapAndLayoutReady {

  private OnMapAndLayoutReady() {
  }
  /*********************** V1 for MapViews ***************/
  /**
   * Converts {@link OnMapReadyCallback} to an observable.
   * Note that this method calls {@link MapView#getMapAsync(OnMapReadyCallback)} so you there is no
   * need to initialize google map view manually.
   */
  private static Observable<GoogleMap> loadMapObservable(final MapView mapView) {
    return Observable.create(new Observable.OnSubscribe<GoogleMap>() {
      @Override
      public void call(final Subscriber<? super GoogleMap> subscriber) {
        OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
          @Override
          public void onMapReady(GoogleMap googleMap) {
            subscriber.onNext(googleMap);
          }
        };
        mapView.getMapAsync(mapReadyCallback);
      }
    });
  }

  /**
   * Converts {@link ViewTreeObserver.OnGlobalLayoutListener} to an observable.
   * This methods also takes care of removing the global layout listener from the view.
   */
  private static Observable<MapView> globalLayoutObservable(final MapView view) {
    return Observable.create(new Observable.OnSubscribe<MapView>() {
      @Override
      public void call(final Subscriber<? super MapView> subscriber) {
        final ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
          @Override
          public void onGlobalLayout() {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            subscriber.onNext(view);
          }
        };
        view.getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
      }
    });
  }

  /**
   * Takes {@link #globalLayoutObservable(MapView)} and {@link #loadMapObservable(MapView)} and zips their result.
   * This means that the subscriber will only be notified when both the observables have emitted.
   */
  public static Observable<GoogleMap> onMapAndLayoutReadyObservable(final MapView mapView) {
    return Observable.zip(globalLayoutObservable(mapView), loadMapObservable(mapView), new Func2<MapView, GoogleMap, GoogleMap>()
    {
      @Override
      public GoogleMap call(MapView mapView, GoogleMap googleMap) {
        return googleMap;
      }
    });
  }

  /*********************** V2 for MapFragments ***************/
  public static Observable<GoogleMap> onMapAndLayoutReadyObservable(final SupportMapFragment map) {
    return Observable.zip(globalLayoutObservable(map), loadMapObservable(map), new Func2<View, GoogleMap, GoogleMap>()
    {
      @Override
      public GoogleMap call(View view, GoogleMap googleMap) {
        return googleMap;
      }
    });
  }

  /**
   * Converts {@link OnMapReadyCallback} to an observable.
   * Note that this method calls {@link MapView#getMapAsync(OnMapReadyCallback)} so you there is no
   * need to initialize google map view manually.
   */
  private static Observable<GoogleMap> loadMapObservable(final SupportMapFragment map) {
    return Observable.create(new Observable.OnSubscribe<GoogleMap>() {
      @Override
      public void call(final Subscriber<? super GoogleMap> subscriber) {
        OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
          @Override
          public void onMapReady(GoogleMap googleMap) {
            subscriber.onNext(googleMap);
          }
        };
        map.getMapAsync(mapReadyCallback);
      }
    });
  }

  /**
   * Converts {@link ViewTreeObserver.OnGlobalLayoutListener} to an observable.
   * This methods also takes care of removing the global layout listener from the view.
   */
  private static Observable<View> globalLayoutObservable(final SupportMapFragment map) {
    return Observable.create(new Observable.OnSubscribe<View>() {
      @Override
      public void call(final Subscriber<? super View> subscriber)
      {
        View view = map.getView();

        final ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
          @Override
          public void onGlobalLayout() {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            subscriber.onNext(view);
          }
        };
        view.getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
      }
    });
  }
}
