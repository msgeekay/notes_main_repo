package com.msgeekay.notes.presentationlayer.ui.common.mvp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.MenuItem;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.di.common.components.ApplicationComponent;
import com.msgeekay.notes.presentationlayer.navigation.Navigator;
import com.msgeekay.notes.presentationlayer.utils.ExtraTransitionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.inject.Inject;
import javax.inject.Named;

import icepick.Icepick;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public abstract class BaseMvpActivity extends MvpAppCompatActivity
{
  @Inject
  Navigator navigator;

  @Inject
  @Named("ProcName")
  String procName;

  public Stack<Fragment> fragmentStack = new Stack<>();

  public void addToStack(Fragment f)
  {
    fragmentStack.push(f);
  }

  public int getStackHeight()
  {
    return fragmentStack.size();
  }

  public Fragment popFromStack()
  {
    if (!fragmentStack.isEmpty())
      return fragmentStack.pop();
    else
      return null;
  }

  public Fragment peekFromStack()
  {
    if (!fragmentStack.isEmpty())
      return fragmentStack.peek();
    else
      return null;
  }


  @Override
  protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.getApplicationComponent().inject(this);
    Icepick.restoreInstanceState(this, savedInstanceState);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Icepick.saveInstanceState(this, outState);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case android.R.id.home:
        supportFinishAfterTransition();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

//  /**
//   * @return true is already have permission
//   */
//  protected boolean askPermission(String permission, Integer requestCode) {
//    if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
//
//      // Should we show an explanation?
//      if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//
//        //This is called if user has denied the permission before
//        //In this case I am just asking the permission again
//        ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
//
//      } else {
//
//        ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
//      }
//
//      return false;
//    }
//
//    return true;
//  }

  /**
   * Extended permissions check
   *
   */
  protected final void askForPermissions(String[] permissions, int requestCode) {
    List<String> permissionsToRequest = new ArrayList<>();
    for (String permission : permissions) {
      if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
        permissionsToRequest.add(permission);
      }
    }
    if (!permissionsToRequest.isEmpty()) {
      ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), requestCode);
    } else permissionsCheckedOk();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (grantResults.length == 0) return;
    permissionsCheckedOk();
  }

  public abstract void permissionsCheckedOk();

  /**
   * Get the Main Application component for dependency injection.
   *
   * @return {@link ApplicationComponent}
   */
  protected ApplicationComponent getApplicationComponent() {
    return ((MyApp)getApplication()).getApplicationComponent();
  }

  public Navigator getNavigator() { return navigator; }

  /**
   * Transitions
   */
  @SuppressWarnings("unchecked")
  protected void transitionTo(Intent i) {
    final Pair<View, String>[] pairs = ExtraTransitionUtils.createSafeTransitionParticipants(this, true);
    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
    startActivity(i, transitionActivityOptions.toBundle());
  }

  @SuppressWarnings("unchecked")
  protected ActivityOptionsCompat getSupportPairAsOpitions() {
    final Pair<View, String>[] pairs = ExtraTransitionUtils.createSafeTransitionParticipants(this, true);
    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
    return transitionActivityOptions;
  }

  public void Timberlg(String tag, String message, Object... args) {
    Timber.tag(procName + " " + tag).w(message, args);
  }
}
