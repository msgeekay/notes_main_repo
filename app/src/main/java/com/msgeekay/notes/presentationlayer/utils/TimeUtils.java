package com.msgeekay.notes.presentationlayer.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class TimeUtils
{
  public static String convert(long time)
  {
    return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).format(new Date(time));
  }

  public static String convertToLogs(long time)
  {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date(time));
  }

  public static String convertToUser(long time)
  {
    return new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.getDefault()).format(new Date(time));
  }

  public static String convertDateToUserWithMonth(long time)
  {
    return new SimpleDateFormat("MMM d, yyyy", Locale.getDefault()).format(new Date(time));
  }

  public static String convertToFileName(long time)
  {
    return new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss'Z'", Locale.getDefault()).format(new Date(time));
  }

  public static String convertToTrackName(long time)
  {
    return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date(time));
  }

  public static String convertToHhMm(long time)
  {
    return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date(time));
  }

  public static String prepareDurationInSecToStr(double duration)
  {
    int mins = (int) Math.ceil(duration / 60);
    int hours = (int) Math.floor(mins / 60);
    int minDiff = mins - hours * 60;
    String ret = String.format("%02d:%02d", hours, minDiff);
    return ret;
  }

  public static String prepareDurationInMilisecToStr(double duration)
  {
    return prepareDurationInSecToStr(duration / 1000);
  }
}
