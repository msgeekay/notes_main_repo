package com.msgeekay.notes.presentationlayer.ui.editnote;

import android.net.Uri;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.model.Note;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public interface EditView extends MvpView
{
  void afterNoteCreation(Note note);
  void afterNoteUpdate(Boolean isOk);
  void afterNoteExport(Uri uri);

  void showErrorMessage(DefaultErrorBundle defaultErrorBundle);
}
