package com.msgeekay.notes.presentationlayer.ui.camera;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class CameraHelper
{
  public static final int REQUEST_CAMERA_PERMISSIONS = 931;
  public static final int CAPTURE_MEDIA = 368;

  public static String[] permissions = new String[]{
          android.Manifest.permission.CAMERA,
          android.Manifest.permission.RECORD_AUDIO,
          android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
          android.Manifest.permission.READ_EXTERNAL_STORAGE};
}
