package com.msgeekay.notes.presentationlayer.ui.main.map;

import com.arellomobile.mvp.MvpView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.msgeekay.notes.domainlayer.exception.ErrorBundle;
import com.msgeekay.notes.model.Note;

import java.util.List;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public interface CommonView extends MvpView
{
  void setNotes(List<Note> noteList);
  void onMapReady(GoogleMap googleMap);

  public void hideViewLoading();
  public void showErrorMessage(ErrorBundle errorBundle);

  void refreshData();

  void onBackPressedWithScene(LatLngBounds latLngBounds);
  void moveMapAndAddMaker(LatLngBounds latLngBounds);
  void updateMapZoomAndRegion(LatLng northeastLatLng, LatLng southwestLatLng);

  //boolean getIsSwaping();
  void swapViews();

}
