package com.msgeekay.notes.presentationlayer.di;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Interface representing a contract for clients that contains a component for dependency injection.
 */

public interface HasComponent<C> {
  C getComponent();
}
