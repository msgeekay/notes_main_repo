package com.msgeekay.notes.presentationlayer.ui.common.anim.spring;

/**
 * Created by grigoriykatz on 14/08/17.
 */

public enum SpringAnimationType {
  TRANSLATEX,
  TRANSLATEY,
  ROTATEX,
  ROTATEY,
  SCALEXY,
  SCALEX,
  SCALEY,
  ALPHA,
  ROTATION
}
