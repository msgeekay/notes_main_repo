package com.msgeekay.notes.presentationlayer.ui.main.map;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import com.msgeekay.notes.presentationlayer.utils.CustomUtils;
import com.squareup.picasso.Picasso;
//import com.transitionseverywhere.Scene;
//import com.transitionseverywhere.TransitionManager;
//import com.transitionseverywhere.TransitionSet;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.msgeekay.notes.R;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.ui.main.MainActivity;
//import com.squareup.haha.perflib.Main;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by grigoriykatz on 18/09/17.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class DetailsLayout extends CoordinatorLayout
{

  @BindView(R.id.cardview)
  CardView cardViewContainer;
  @BindView(R.id.headerImage)
  ImageView imageViewPlaceDetails;
  @BindView(R.id.title)
  TextView textViewTitle;
  @BindView(R.id.description) TextView textViewDescription;

  @BindView(R.id.takeMe) TextView editNote;

  public DetailsLayout(final Context context) {
    this(context, null);
  }

  public DetailsLayout(final Context context, final AttributeSet attrs) {
    super(context, attrs);
  }

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
  }

  private void setData(Note note) {
    textViewTitle.setText(note.getNoteTitle());
    textViewDescription.setText(note.getNoteBody());
    if (note.getImageURI() == null
            || (note.getImageURI() != null && note.getImageURI().length() == 0))
      imageViewPlaceDetails.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.default_profile_background));
    else
      Picasso.with(getContext())
              .load(new File(note.getImageURI()))
              .placeholder(R.drawable.default_profile_background)
              .error(R.drawable.default_profile_background)
              //.fit()
              //.centerInside()
              .into(imageViewPlaceDetails);
  }

  public static Scene showScene(Activity activity, final ViewGroup container,
                                final View sharedView, final String transitionName,
                                final Note note, final boolean forceToSimplify)
  {
    DetailsLayout detailsLayout
        = (DetailsLayout) activity.getLayoutInflater().inflate(R.layout.layout_note_detailed,
                                                        container, false);
    detailsLayout.setData(note);

    detailsLayout.editNote.setOnClickListener(new OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        if (activity instanceof MainActivity)
        {
          ((MainActivity)activity).startEditActivity(note);
        }
      }
    });

//    TransitionSet set = new ShowDetailsTransitionSet(activity, transitionName, sharedView, detailsLayout);
//    Scene scene = new Scene(container, (View) detailsLayout);
//    TransitionManager.go(scene, set);

    Scene scene = new Scene(container, (View) detailsLayout);
    if (CustomUtils.isAndroid5() && !forceToSimplify)
    {
      TransitionSet set = new ShowDetailsTransitionSet(activity, transitionName, sharedView, detailsLayout);
      TransitionManager.go(scene, set);
    }
    else
    {
      boolean hasDetails = false;
      int idx = -1;
      for (int i = 0; i < container.getChildCount(); i++)
      {
        View v = container.getChildAt(i);
        if (v instanceof DetailsLayout)
        {
          hasDetails = true;
          idx = i;
        }
      }

      if (hasDetails && idx > 0)
        container.removeViewAt(idx);

      container.addView(detailsLayout);
      CardView cardViewContainer = (CardView) detailsLayout.findViewById(R.id.cardview);
      cardViewContainer.setTranslationY(cardViewContainer.getLayoutParams().height);

      SpringAnimation anim = new SpringAnimation(cardViewContainer, DynamicAnimation.TRANSLATION_Y, 0);
      anim.getSpring()
          .setDampingRatio(SpringForce.DAMPING_RATIO_HIGH_BOUNCY)
          .setStiffness(SpringForce.STIFFNESS_LOW);
          //.setStiffness(SpringForce.STIFFNESS_HIGH);

//      anim
//      //.setStartValue(200)
//          .setStartVelocity(10)
//          .setMinValue(-40)
//          .start();

      SpringAnimation anim2 = new SpringAnimation(cardViewContainer, DynamicAnimation.TRANSLATION_Y, 0);
      anim2.setStartVelocity(50).start();


//      cardViewContainer.animate()
//              .translationY(0)
//              .setDuration(300)
//              .setInterpolator(new AccelerateInterpolator())
//              .start();
    }

    return scene;
  }

  public static Scene hideScene(Activity activity, final ViewGroup container,
                                final View sharedView, final String transitionName,
                                boolean forceToSimplify)
  {
    DetailsLayout detailsLayout = (DetailsLayout) container.findViewById(R.id.layout_note_detailed_container);

//    TransitionSet set = new HideDetailsTransitionSet(activity, transitionName, sharedView, detailsLayout);
//    Scene scene = new Scene(container, (View) detailsLayout);
//    TransitionManager.go(scene, set);

    Scene scene = new Scene(container, (View) detailsLayout);
    if (CustomUtils.isAndroid5() && !forceToSimplify)
    {
      TransitionSet set = new HideDetailsTransitionSet(activity, transitionName, sharedView, detailsLayout);
      TransitionManager.go(scene, set);
    }
    else
    {
      CardView cardViewContainer = (CardView) detailsLayout.findViewById(R.id.cardview);
      int height = cardViewContainer.getLayoutParams().height;
      //cardViewContainer.setTranslationY(height);
      cardViewContainer.animate()
              .translationY(height)
              .setDuration(300)
              .setInterpolator(new AccelerateInterpolator())
              .setListener(new AnimatorListenerAdapter()
              {
                @Override
                public void onAnimationEnd(Animator animation)
                {
                  detailsLayout.setVisibility(GONE);
                }
              })
              .start();
    }

    return scene;
  }
}