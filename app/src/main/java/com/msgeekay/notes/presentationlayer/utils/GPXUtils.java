package com.msgeekay.notes.presentationlayer.utils;

import android.util.Log;

import com.msgeekay.notes.model.UserPoint;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class GPXUtils
{
  private static final int indent = 4;

  private static Document prepareDocument(List<UserPoint> track)
  {
    try
    {
      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
      // root elements
      Document doc = docBuilder.newDocument();
      Element rootElement = doc.createElement("gpx");
      doc.appendChild(rootElement);

      Element trk = doc.createElement("trk");
      rootElement.appendChild(trk);

      addTextNode(doc, trk, "name", "YNDXCOURIERTRK");
      addTextNode(doc, trk, "desc", "");

      Element trkseg = doc.createElement("trkseg");
      trk.appendChild(trkseg);

      for (UserPoint point : track)
      {
        Element pt = doc.createElement("trkpt");
        trkseg.appendChild(pt);

        Attr attr = doc.createAttribute("lat");
        attr.setValue(String.valueOf(point.getUserPointLatitude()));
        pt.setAttributeNode(attr);

        attr = doc.createAttribute("lon");
        attr.setValue(String.valueOf(point.getUserPointLongitude()));
        pt.setAttributeNode(attr);

        //addTextNode(doc, pt, "ele", String.valueOf(point.getUserPointAltitude()));
        addTextNode(doc, pt, "time", TimeUtils.convert(point.getUserPointTimestamp()));

      }

      return doc;
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace();
      //CrashHelper.reportCrash(e);
    }

    return null;

  }

  public static boolean Export(List<UserPoint> track, File file)
  {
    try
    {
      Document doc = prepareDocument(track);
      if (doc == null)
        return false;

      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(file);

      //transformerFactory.setAttribute("indent-number", indent);
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(indent));
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.transform(source, result);
      return true;
    }
    catch (TransformerException e)
    {
      e.printStackTrace();
    }

    return false;
  }

  public static String ExportToString(List<UserPoint> track)
  {
    String result = new String();
    Document doc = prepareDocument(track);
    if (doc == null)
      return result;

    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer transformer = null;
    try
    {
      transformer = tf.newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      StringWriter writer = new StringWriter();
      transformer.transform(new DOMSource(doc), new StreamResult(writer));
      result = writer.getBuffer().toString().replaceAll("\n|\r", "");
    }
    catch (TransformerException e)
    {
      e.printStackTrace();
      //CrashHelper.reportCrash(e);
    }

    return result;
  }

  private static void addTextNode(Document doc, Element ele, String element, String text)
  {
    Element e = doc.createElement(element);
    e.appendChild(doc.createTextNode(text));
    ele.appendChild(e);
  }

/* Usage:
          String path = Environment.getExternalStorageDirectory()+ File.separator+"20140131_085431.gpx";
          FileInputStream is = null;
          try {
            is = new FileInputStream(path);
            List<TrackPoint> list = ConvertGPX.Import(is);
          } catch (FileNotFoundException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
 */


  public static List<UserPoint> Import(InputStream inputStream)
  {
    Log.d("Parse gpx", "Start");
    List<UserPoint> result = new ArrayList<UserPoint>();

    try
    {
      Document xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
      NodeList nodes = xmldoc.getElementsByTagName("trkpt");
      for (int i = 0; i < nodes.getLength(); i++)
      {
        Node node = nodes.item(i);
        UserPoint tp = new UserPoint();
        tp.setUserPointLatitude(Double.parseDouble(node.getAttributes().getNamedItem("lat").getTextContent()));
        tp.setUserPointLongitude(Double.parseDouble(node.getAttributes().getNamedItem("lon").getTextContent()));

        if (node.hasChildNodes())
        {
          NodeList childs = node.getChildNodes();
          for (int j = 0; j < childs.getLength(); j++)
          {
            Node child = childs.item(j);
            switch (child.getNodeName())
            {
              case "ele":
                //tp.altitude = Double.parseDouble(child.getTextContent());
                break;
              case "time":
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                try
                {
                  tp.setUserPointTimestamp(sdf.parse(child.getTextContent()).getTime());
                }
                catch (ParseException e)
                {
                  e.printStackTrace();
                }
                break;
              default:
                break;
            }
          }
        }

        result.add(tp);
      }
    }
    catch (SAXException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace();
    }

    Log.d("Parse gpx", String.format("End: (%d points)", result.size()));
    return result;
  }
}
