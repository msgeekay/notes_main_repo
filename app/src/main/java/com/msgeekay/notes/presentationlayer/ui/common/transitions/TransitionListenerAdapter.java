package com.msgeekay.notes.presentationlayer.ui.common.transitions;

import android.annotation.TargetApi;
import android.os.Build;
import android.transition.Transition;
//import com.transitionseverywhere.Transition;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class TransitionListenerAdapter implements Transition.TransitionListener {
    @Override
    public void onTransitionStart(final Transition transition) {

    }

    @Override
    public void onTransitionEnd(final Transition transition) {

    }

    @Override
    public void onTransitionCancel(final Transition transition) {

    }

    @Override
    public void onTransitionPause(final Transition transition) {

    }

    @Override
    public void onTransitionResume(final Transition transition) {

    }
}
