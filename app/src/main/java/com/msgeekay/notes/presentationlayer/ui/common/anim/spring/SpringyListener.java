package com.msgeekay.notes.presentationlayer.ui.common.anim.spring;

/**
 * Created by grigoriykatz on 14/08/17.
 */

public interface SpringyListener {

  /*
  * hits when Spring is Active
  * */
  void onSpringStart();

    /*
    * hits when Spring is inActive
    * */

  void onSpringStop();
}
