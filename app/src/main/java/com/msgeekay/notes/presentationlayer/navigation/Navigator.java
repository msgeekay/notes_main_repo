package com.msgeekay.notes.presentationlayer.navigation;

/**
 * Created by grigoriykatz on 09/05/17.
 */

import android.content.Context;
import android.content.Intent;


import com.msgeekay.notes.infrastructurelayer.TrackingService;
import com.msgeekay.notes.presentationlayer.ui.main.MainActivity;
//import com.msgeekay.notes.presentationlayer.ui.mainold.MainActivity;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Class used to navigate through the application.
 */
@Singleton
public class Navigator {

  public static class RequestCodes
  {

    public static final int NewsDetails = 0;
    public static final int Tools = 1;
    public static final int ToolsDetails = 2;
    public static final int Statistics = 3;
    public static final int AboutUs = 4;
    public static final int Feedback = 5;
    public static final int OtherWebView = 7;
    public static final int AboutApp = 8;

//    public static final int DayPlanning = 8;
//    public static final int UserPoint = 9;
//    public static final int GetToPoint = 10;
//    public static final int OutOfArea = 11;
//    public static final int Sharing = 12;
  }

  @Inject
  public Navigator() {
    //empty
  }


  /**
   * Starts main location tracking service.
   *
   * @param context A Context needed to start activity.
   */
  public void startTrackService(Context context)
  {
    Intent intent = new Intent(context, TrackingService.class);
    context.startService(intent);
  }

  /**
   * Stops main location tracking service.
   *
   * @param context A Context needed to start activity.
   */
  public void stopTrackService(Context context)
  {
    Intent intent = new Intent(context, TrackingService.class);
    context.stopService(intent);
  }


  /**
   * Leads to the main screen.
   *
   * @param context A Context needed to start activity.
   */
  public void navigateToMainScreen(Context context) {
    if (context != null) {
      Intent intentToLaunch = MainActivity.getCallingIntent(context);
      context.startActivity(intentToLaunch);
    }
  }

  /*
  public void navigateToUserDetails(Context context, int userId) {
    if (context != null) {
      Intent intentToLaunch = UserDetailsActivity.getCallingIntent(context, userId);
      context.startActivity(intentToLaunch);
    }
  }

  public void navigateToNewsDetails(Context context, int newsId) {
    if (context != null) {
      int requestCode = RequestCodes.NewsDetails;
      Intent intentToLaunch = UserDetailsActivity.getCallingIntent(context, newsId);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }


  public void navigateToNewsDetails(Context context, NewsItemModel newsItemModel, int[] startingLocation, View view) {
    if (context != null) {
      int requestCode = RequestCodes.NewsDetails;
      DetailItemModelDataMapper detailItemModelDataMapper = new DetailItemModelDataMapper();
      DetailItemModel dim = detailItemModelDataMapper.transform(newsItemModel);
      //Intent intentToLaunch = NewsDetailsActivity.getCallingIntent(context, dim, startingLocation);
      Intent intentToLaunch = NewsDetailsActivity.getCallingIntent(context, dim, startingLocation, view);


      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
      {
        Pair<View, String> p1 = null;
        if (view != null)
        {
          p1 = Pair.create(view, context.getString(R.string.shared_tr_main_to_details));
        }
        final Pair<View, String>[] pairs
                = TransitionUtils.createSafeTransitionParticipants(((MainActivity) context), false,
                p1);
        ActivityOptionsCompat options
                = ActivityOptionsCompat.makeSceneTransitionAnimation(((MainActivity) context), pairs);
        ((MainActivity) context).startActivityForResult(intentToLaunch, requestCode, options.toBundle());

      }
      else
      {
        ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
        ((MainActivity)context).overridePendingTransition(0, 0);
      }
    }
  }

  public void navigateToStatistics(Context context, int newsId) {
    if (context != null) {
      int requestCode = RequestCodes.Statistics;
      Intent intentToLaunch = StatisticsActivity.getCallingIntent(context, newsId);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToUseful(Context context, int newsId) {
    if (context != null) {

      Preferences.saveToolsBreadcrumb(null);

      int requestCode = RequestCodes.Tools;

      Intent intentToLaunch = UsefulActivity.getCallingIntent(context, newsId);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToUsefulDetail(Context context, ToolPresentationModel toolPresentationModel) {
    if (context != null) {

      int requestCode = RequestCodes.ToolsDetails;

      DetailItemModelDataMapper detailItemModelDataMapper = new DetailItemModelDataMapper();
      DetailItemModel dim = detailItemModelDataMapper.transform(toolPresentationModel);

      Intent intentToLaunch = UsefulDetailsActivity.getCallingIntent(context, dim);
      ((UsefulActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToAboutUs(Context context, int newsId) {
    if (context != null) {
      int requestCode = RequestCodes.AboutUs;
      Intent intentToLaunch = AboutActivity.getCallingIntent(context, newsId);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToAboutApp(Context context) {
    if (context != null) {
      int requestCode = RequestCodes.AboutApp;
      Intent intentToLaunch = AboutAppActivity.getCallingIntent(context);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToFeedback(Context context, int newsId) {
    if (context != null) {
      int requestCode = RequestCodes.Feedback;
      Intent intentToLaunch = FeedbackActivity.getCallingIntent(context, newsId);
      ((MainActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }

  public void navigateToOtherWebView(Context context, String url) {
    if (context != null) {
      int requestCode = RequestCodes.OtherWebView;
      Intent intentToLaunch = WebViewActivity.getCallingIntent(context, url);
      ((NewsDetailsActivity)context).startActivityForResult(intentToLaunch, requestCode);
      //context.startActivity(intentToLaunch);
    }
  }


  public boolean onActivityResult(Context context, int requestCode, int resultCode, Intent data)
  {
    boolean retVal = false;

    if (requestCode == RequestCodes.Tools)
    {
      Preferences.saveToolsBreadcrumb(null);

      retVal = true;
    }


    return retVal;
  }
  */


}