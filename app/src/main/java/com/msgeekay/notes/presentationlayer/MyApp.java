package com.msgeekay.notes.presentationlayer;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.msgeekay.notes.BuildConfig;
import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.di.common.components.ApplicationComponent;
import com.msgeekay.notes.presentationlayer.di.common.components.DaggerApplicationComponent;
import com.msgeekay.notes.presentationlayer.di.common.modules.ApplicationModule;
import com.msgeekay.notes.presentationlayer.utils.CustomUtils;
import com.msgeekay.notes.presentationlayer.utils.Parameters;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.squareup.leakcanary.LeakCanary;

import java.util.regex.Pattern;

import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class MyApp extends MultiDexApplication
{
  private static MyApp sInstance;
  private ApplicationComponent applicationComponent;
  private String appProcessName = null;

  @Override public void onCreate() {
    super.onCreate();

    sInstance = this;
    this.initDatabase();
    this.initializeInjector();
    this.initializeLeakDetection();
    this.initCalligraphy();

    if (!BuildConfig.DEBUG) {
      Log.d("[TIMBER INIT]", "Timber.DebugTree()");
      Timber.plant(new CustomDebugTree());
    } else {
      Log.d("[TIMBER INIT]", "CrashReportingTree()");
      Timber.plant(new CrashReportingTree());
    }

    if (Parameters.DEBUG)
      Stetho.initializeWithDefaults(this);

  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  private void initDatabase() {
    //FlowManager.init(new FlowConfig.Builder(this).build());
    FlowManager.init(this);
  }

  private void initializeInjector() {
    this.applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();

  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }

  public static MyApp getContext(Context context) {
    return (MyApp) context.getApplicationContext();
  }

  public static MyApp getInstance()
  {
    return sInstance;
  }

  private void initializeLeakDetection() {
    if (BuildConfig.DEBUG) {
      LeakCanary.install(this);
    }
  }

  public static MyApp get()
  {
    return sInstance;
  }

  public synchronized String getAppProcessName()
  {
    if (appProcessName == null)
    {
      appProcessName = CustomUtils.getCurrentProcessName(this);
    }

    return appProcessName;
  }

  private void initCalligraphy() {
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Roboto-Regular.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build()
    );
  }

  /** A tree which logs important information for crash reporting. */
  private static class CrashReportingTree extends Timber.Tree
  {
    @Override protected void log(int priority, String tag, String message, Throwable t)
    {
      if (!BuildConfig.DEBUG)
      {
        if (priority == Log.VERBOSE || priority == Log.DEBUG)
        {
          return;
        }
      }

      FakeCrashLibrary.log(priority, tag, message);

      if (t != null)
      {
        if (priority == Log.ERROR)
        {
          FakeCrashLibrary.logError(tag, message, t);
        } else if (priority == Log.WARN)
        {
          FakeCrashLibrary.logWarning(tag, message, t);
        }
      }
    }

  }

  /** A {@link Timber.Tree Tree} for debug builds. Automatically infers the tag from the calling class. */
  public static class CustomDebugTree extends Timber.Tree
  {
    private static final int MAX_LOG_LENGTH = 4000;
    private static final int CALL_STACK_INDEX = 5;
    private static final Pattern ANONYMOUS_CLASS = Pattern.compile("(\\$\\d+)+$");



    /**
     * Break up {@code message} into maximum-length chunks (if needed) and send to either
     * {@link Log#println(int, String, String) Log.println()} or
     * {@link Log#wtf(String, String) Log.wtf()} for logging.
     *
     * {@inheritDoc}
     */
    @Override protected void log(int priority, String tag, String message, Throwable t) {
      if (message.length() < MAX_LOG_LENGTH) {
        if (priority == Log.ASSERT) {
          Log.wtf(tag, message);
        } else {
          Log.println(priority, tag, message);
        }
        return;
      }

      // Split by line, then ensure each line can fit into Log's maximum length.
      for (int i = 0, length = message.length(); i < length; i++) {
        int newline = message.indexOf('\n', i);
        newline = newline != -1 ? newline : length;
        do {
          int end = Math.min(newline, i + MAX_LOG_LENGTH);
          String part = message.substring(i, end);
          if (priority == Log.ASSERT) {
            Log.wtf(tag, part);
          } else {
            Log.println(priority, tag, part);
          }
          i = end;
        } while (i < newline);
      }
    }
  }
}
