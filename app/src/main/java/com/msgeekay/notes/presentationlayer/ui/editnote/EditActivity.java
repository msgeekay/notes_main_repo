package com.msgeekay.notes.presentationlayer.ui.editnote;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.notes.R;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.NotePriority;
import com.msgeekay.notes.presentationlayer.ui.camera.CameraActivity;
import com.msgeekay.notes.presentationlayer.ui.camera.CameraHelper;
import com.msgeekay.notes.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.memfis19.annca.Annca;
import io.github.memfis19.annca.internal.configuration.AnncaConfiguration;

import static com.msgeekay.notes.presentationlayer.ui.camera.CameraActivity.KEY_PHOTO_PATH;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class EditActivity extends BaseMvpActivity implements EditView
{

  public static final String KEY_EDIT_SCR_NOTE_ID           = "KEY_EDIT_SCR_NOTE_ID";
  public static final String KEY_EDIT_SCR_NOTE_TITLE        = "KEY_EDIT_SCR_NOTE_TITLE";
  public static final String KEY_EDIT_SCR_NOTE_BODY         = "KEY_EDIT_SCR_NOTE_BODY";
  public static final String KEY_EDIT_SCR_NOTE              = "KEY_EDIT_SCR_NOTE_RESULT";

  public static final String KEY_EDIT_SCR_NOTE_ITRNL_PRE    = "KEY_EDIT_SCR_NOTE_ITRNL_PRE";
  public static final String KEY_EDIT_SCR_NOTE_ITRNL_POST   = "KEY_EDIT_SCR_NOTE_ITRNL_POST";

  public static final int REQUEST_CODE_FOR_CAMERA           = 1012;
  public static final int RESULT_CODE_OK                    = 1011;

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.add_title) EditText add_title;
  @BindView(R.id.add_body) EditText add_body;

  /** Most old version used for canceling of last changes **/
  private Note note;
  /** Most actual version w/ last updates **/
  private Note changedNote;


  private long noteId;
  private String title;
  private String body;

  private boolean canMakePhoto = false;

  public static Intent getCallingIntent(Context context, long noteId, String title, String body)
  {
    Intent callingIntent = new Intent(context, EditActivity.class);
    if (noteId >= 0)
    {
      callingIntent.putExtra(KEY_EDIT_SCR_NOTE_ID, Long.valueOf(noteId));
      callingIntent.putExtra(KEY_EDIT_SCR_NOTE_TITLE, title);
      callingIntent.putExtra(KEY_EDIT_SCR_NOTE_BODY, body);
    }
    return callingIntent;
  }

  public static Intent getCallingIntent(Context context, Note note)
  {
    Intent callingIntent = new Intent(context, EditActivity.class);
    if (note != null)
    {
      callingIntent.putExtra(KEY_EDIT_SCR_NOTE, note);
    }
    return callingIntent;
  }

  @InjectPresenter(type = PresenterType.LOCAL)
  EditPresenter mPresenter;

  @ProvidePresenter(type = PresenterType.LOCAL)
  EditPresenter providePresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideEditPresenter();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Timberlg(getClass().getName(), "onCreate");
    setTheme(R.style.AppTheme_MainActivity);
    super.onCreate(savedInstanceState);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_edit);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    //noinspection ConstantConditions
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    Bundle extras = getIntent().getExtras();
    Intent i = getIntent();
    if (i != null && i.hasExtra(KEY_EDIT_SCR_NOTE_ID))
    {
      Long id = i.getLongExtra(KEY_EDIT_SCR_NOTE_ID, -1);
      noteId = id.longValue();
      if (id.longValue() >= 0)
      {
        title = i.getStringExtra(KEY_EDIT_SCR_NOTE_TITLE);
        if (title != null && title.length() > 0)
          add_title.setText(title);
        body = i.getStringExtra(KEY_EDIT_SCR_NOTE_BODY);
        if (body != null && body.length() > 0)
          add_body.setText(body);
      }
      initPrioritySpinner(null);
    }
    else if (i != null && i.hasExtra(KEY_EDIT_SCR_NOTE))
    {
      note = i.getParcelableExtra(KEY_EDIT_SCR_NOTE);
      if (note != null)
      {
        changedNote = new Note(note);
        if (note.getNoteTitle() != null && note.getNoteTitle().length() > 0)
          add_title.setText(note.getNoteTitle());
        if (note.getNoteBody() != null && note.getNoteBody().length() > 0)
          add_body.setText(note.getNoteBody());
      }

      initPrioritySpinner(note);
    }

    //checkCameraPermissions();

  }

  private void initPrioritySpinner(Note note)
  {
    View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.toolbar_spinner,
            toolbar, false);
    ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    toolbar.addView(spinnerContainer, lp);

    Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.toolbar_spinner);
    String [] priorityArray = getResources().getStringArray(R.array.spinner_elements);

    ToolbarSpinnerAdapter spinnerAdapter = new ToolbarSpinnerAdapter(this);
    for (int i = 0; i < 4; i++)
    {
      NotePriority np = NotePriority.getByPriority(i);
      spinnerAdapter.addItem(np);
    }

    spinner.setAdapter(spinnerAdapter);
    spinner.setPrompt(getResources().getString(R.string.edt_priority));
    spinner.setSelection(note.getPriority().getId());
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        changedNote.setPriority(NotePriority.getByPriority(i));

      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });
  }

  private void checkCameraPermissions()
  {
    if (Build.VERSION.SDK_INT > 15) {
      askForPermissions(CameraHelper.permissions, CameraHelper.REQUEST_CAMERA_PERMISSIONS);
    } else {
      permissionsCheckedOk();
    }
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState)
  {
    super.onPostCreate(savedInstanceState);

    if (!(note.getNoteId() >= 0))
    {
      if (mPresenter != null)
      {
        mPresenter.startLocationTracking();
        mPresenter.createNote(note);
      }
    }

    setupParts();
  }

  @Override
  protected void onStop()
  {
    if (mPresenter != null)
      mPresenter.stop();
    super.onStop();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_edit, menu);
    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case R.id.export_note:
        if (mPresenter !=  null)
          mPresenter.exportNote(changedNote);

        return true;

      case R.id.make_photo:
        startCameraScreen();
        return true;

      case android.R.id.home:
        prepareToFinish();
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void permissionsCheckedOk()
  {
    canMakePhoto = true;
  }

  @Override
  public void onBackPressed()
  {
    prepareToFinish();
    super.onBackPressed();
  }

  private void prepareToFinish()
  {
    Intent i = new Intent();
//    if (note == null)
//    {
//      title = add_title.getText().toString();
//      body = add_body.getText().toString();
//      i.putExtra(KEY_EDIT_SCR_NOTE_ID, noteId);
//      i.putExtra(KEY_EDIT_SCR_NOTE_TITLE, title);
//      i.putExtra(KEY_EDIT_SCR_NOTE_BODY, body);
//    }
//    else
    {
//      note.setNoteTitle(add_title.getText().toString());
//      note.setNoteBody(add_body.getText().toString());
//      i.putExtra(KEY_EDIT_SCR_NOTE, note);

      changedNote.setNoteTitle(add_title.getText().toString());
      changedNote.setNoteBody(add_body.getText().toString());
      i.putExtra(KEY_EDIT_SCR_NOTE, changedNote);
    }
    setResult(RESULT_CODE_OK, i);
    supportFinishAfterTransition();
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState)
  {
    if (savedInstanceState != null)
    {
      note = savedInstanceState.getParcelable(KEY_EDIT_SCR_NOTE_ITRNL_PRE);
      changedNote = savedInstanceState.getParcelable(KEY_EDIT_SCR_NOTE_ITRNL_POST);

      if (changedNote.getNoteTitle() != null && changedNote.getNoteTitle().length() > 0)
        add_title.setText(changedNote.getNoteTitle());
      if (changedNote.getNoteBody() != null && changedNote.getNoteBody().length() > 0)
        add_body.setText(changedNote.getNoteBody());

    }
    super.onRestoreInstanceState(savedInstanceState);
  }

  @Override
  public void onSaveInstanceState(Bundle outState)
  {
//    if (note != null)
//    {
//      changedNote = new Note(note);
//      changedNote.setNoteTitle(add_title.getText().toString());
//      changedNote.setNoteBody(add_body.getText().toString());
//    }

    outState.putParcelable(KEY_EDIT_SCR_NOTE_ITRNL_PRE, note);
    outState.putParcelable(KEY_EDIT_SCR_NOTE_ITRNL_POST, changedNote);
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == REQUEST_CODE_FOR_CAMERA && resultCode == RESULT_OK)
    {
      String filePath = data.getStringExtra(KEY_PHOTO_PATH);
//      if (note != null)
//        note.setImageURI(filePath);
      if (changedNote != null)
      {
        changedNote.setImageURI(filePath);
        if (mPresenter != null && (changedNote.getNoteId() >= 0))
          mPresenter.updateNote(changedNote);
      }
    }
  }

  private void startCameraScreen()
  {
    Intent i = CameraActivity.getCallingIntent(this);
    startActivityForResult(i, REQUEST_CODE_FOR_CAMERA);
  }


  public void setupParts()
  {
    //initCommonFragment();

  }

  @Override
  public void afterNoteCreation(Note note)
  {
    this.note = new Note(note);
    if (this.changedNote != null)
    {
      this.changedNote = new Note(note);
    }
  }

  @Override
  public void afterNoteUpdate(Boolean isOk)
  {
    Toast.makeText(this, "Note updated successfully", Toast.LENGTH_LONG).show();
  }

  @Override
  public void afterNoteExport(Uri uri)
  {
    if (uri == null)
      return;

    Toast.makeText(this, "Exported successfully", Toast.LENGTH_LONG).show();

    Intent sendIntent = new Intent();
    sendIntent.setAction(Intent.ACTION_SEND);
    sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
    sendIntent.putExtra(Intent.EXTRA_TEXT, "Note");

    sendIntent.putExtra(Intent.EXTRA_STREAM, uri);

    sendIntent.setType("text/plain");
    if (sendIntent.resolveActivity(getPackageManager()) != null)
    {
      startActivity(sendIntent);
    }
  }

  @Override
  public void showErrorMessage(DefaultErrorBundle defaultErrorBundle)
  {
    Toast.makeText(this, defaultErrorBundle.getErrorMessage(), Toast.LENGTH_SHORT).show();
  }
}
