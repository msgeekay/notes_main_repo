package com.msgeekay.notes.presentationlayer.ui.main;

import android.location.Location;

/**
 * Created by grigoriykatz on 22/09/17.
 */

public interface LocationInformer
{
  Location provideLastKnownLocation();
}
