package com.msgeekay.notes.presentationlayer.ui.main;

import com.arellomobile.mvp.MvpView;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.presentationlayer.ui.common.views.SettingsLayout;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface MainView extends MvpView
{
  void refreshChildViews();
  void showErrorMessage(DefaultErrorBundle d);

}
