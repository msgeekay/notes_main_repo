package com.msgeekay.notes.presentationlayer.ui.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.gms.maps.model.LatLng;
import com.msgeekay.notes.R;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.infrastructurelayer.TrackingService;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.presentation.MainViewBackStack;
import com.msgeekay.notes.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.notes.presentationlayer.ui.common.dialogs.DeleteDialog;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpFragment;
import com.msgeekay.notes.presentationlayer.ui.common.views.SettingsLayout;
import com.msgeekay.notes.presentationlayer.ui.editnote.EditActivity;
import com.msgeekay.notes.presentationlayer.ui.main.map.CommonFragment;
import com.msgeekay.notes.presentationlayer.ui.main.map.CommonView;
import com.msgeekay.notes.presentationlayer.utils.Parameters;

import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.msgeekay.notes.presentationlayer.ui.editnote.EditActivity.KEY_EDIT_SCR_NOTE;
import static com.msgeekay.notes.presentationlayer.ui.editnote.EditActivity.KEY_EDIT_SCR_NOTE_BODY;
import static com.msgeekay.notes.presentationlayer.ui.editnote.EditActivity.KEY_EDIT_SCR_NOTE_ID;
import static com.msgeekay.notes.presentationlayer.ui.editnote.EditActivity.KEY_EDIT_SCR_NOTE_TITLE;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class MainActivity extends BaseMvpActivity implements MainView, LocationInformer, ModelCallbackProvider
{
  private static final String TAG = MainActivity.class.getName();
  private static final int REQUEST_CODE_FOR_EDIT = 1000;

  @BindView(R.id.toolbar) Toolbar toolbar;

  private Stack<MainViewBackStack> sceneBackStack = new Stack<>();
  private Location lastKnownLocation = null;
  private SettingsLayout.ButtonsClickListener settingsClickListener = null;


  public static Intent getCallingIntent(Context context)
  {
    Intent callingIntent = new Intent(context, MainActivity.class);
    return callingIntent;
  }

  @InjectPresenter(type = PresenterType.GLOBAL)
  MainPresenter mainPresenter;

  @ProvidePresenter(type = PresenterType.GLOBAL)
  MainPresenter provideMainPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideMainPresenter();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    Timberlg(getClass().getName(), "onCreate");
    setTheme(R.style.AppTheme_MainActivity);
    super.onCreate(savedInstanceState);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);
    //noinspection ConstantConditions
    //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (getSupportActionBar() != null)
      getSupportActionBar().setDisplayShowTitleEnabled(false);

    initSettingsClickListener();
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState)
  {
    super.onPostCreate(savedInstanceState);
    setupParts();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return true;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case R.id.add_new_note:
        Note n = new Note();
        if (lastKnownLocation != null)
          n.setNoteLocation(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()));
        startEditActivity(n);
        return true;

      case R.id.swap_screens:
        swapViews();
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void permissionsCheckedOk()
  {

  }

  @Override
  protected void onStart()
  {
    super.onStart();
    try
    {
      registerReceiver(broadcastReceiver, new IntentFilter(TrackingService.INTENT_NEW_LOCATION));
    }
    catch (Exception ex)
    {
      Log.d(TAG, ex.toString());
    }
  }

  @Override
  protected void onStop()
  {
    if (mainPresenter != null)
      mainPresenter.stop();
    try
    {
      unregisterReceiver(broadcastReceiver);
    }
    catch (Exception ex)
    {
      Log.d(TAG, ex.toString());
    }
    super.onStop();
  }

  public void startEditActivity(Note note)
  {
    Intent i = EditActivity.getCallingIntent(this, note);
    startActivityForResult(i, REQUEST_CODE_FOR_EDIT);
  }

  private void startEditActivity(long noteId, String title, String body)
  {
    Intent i = EditActivity.getCallingIntent(this, noteId, title, body);
    startActivityForResult(i, REQUEST_CODE_FOR_EDIT);
  }

  public void setupParts()
  {
    initCommonFragment();

  }

  public void initCommonFragment()
  {
    if(getSupportFragmentManager().findFragmentByTag(CommonFragment.TAG) == null) {
      getSupportFragmentManager()
              .beginTransaction()
              .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
              .replace(R.id.container, CommonFragment.newInstance(this), CommonFragment.TAG)
              //.addToBackStack(CommonFragment.TAG)
              .commit();
    }
  }

  public void superOnBackPressed() {
    super.onBackPressed();
  }

  @Override
  public void onBackPressed()
  {
    //if(getSupportFragmentManager().getBackStackEntryCount() > 1)
    if(fragmentStack.size() > 0)
    {
      //triggerFragmentBackPress(getSupportFragmentManager().getBackStackEntryCount());

      ((BaseMvpFragment)popFromStack()).onBackPressed();
    }
    else
    {
      finish();
    }
  }

  private void triggerFragmentBackPress(final int count)
  {
    Fragment f = getSupportFragmentManager().findFragmentByTag(
            getSupportFragmentManager().getBackStackEntryAt(count - 1).getName());
    if (f != null && f instanceof BaseMvpFragment)
    {
      ((BaseMvpFragment)f).onBackPressed();
    }
  }

  private void initSettingsClickListener()
  {
    settingsClickListener = new SettingsLayout.ButtonsClickListener()
    {
      @Override
      public void onBtn1Clicked(Note note)
      {

      }

      @Override
      public void onBtn2Clicked(Note note)
      {



      }

      @Override
      public void onBtn3Clicked(Note note)
      {

      }

      @Override
      public void onBtn4Clicked(Note note)
      {

      }

      @Override
      public void onBtn5Clicked(Note note)
      {

      }

      @Override
      public void onBtn6Clicked(Note note)
      {

      }

      @Override
      public void onBtn7Clicked(Note note)
      {
        DeleteDialog dlg = DeleteDialog.newInstance(note);
        if (dlg != null)
        {
          dlg.setOnDeleteCallback(new DeleteDialog.OnDeleteCallback()
          {
            @Override
            public void processDeletion(Note note)
            {
              if (mainPresenter != null)
                mainPresenter.removeNote(note);
            }
          });

          dlg.show(getSupportFragmentManager(), "DDLG");
        }
      }
    };
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (requestCode == REQUEST_CODE_FOR_EDIT)
    {
      if (resultCode == EditActivity.RESULT_CODE_OK && data != null && data.getExtras() != null)
      {
        if (data.hasExtra(KEY_EDIT_SCR_NOTE_ID))
        {
          Long id = data.getLongExtra(KEY_EDIT_SCR_NOTE_ID, -1);
          String title = data.getStringExtra(KEY_EDIT_SCR_NOTE_TITLE);
          String body = data.getStringExtra(KEY_EDIT_SCR_NOTE_BODY);

          if (mainPresenter == null)
            return;

          if (id.longValue() >= 0)
          {
            Note n = new Note();
            n.setNoteId(id.longValue());
            n.setNoteTitle(title);
            n.setNoteBody(body);
            mainPresenter.updateNote(n);
          }
        }
        else if (data.hasExtra(KEY_EDIT_SCR_NOTE))
        {
          Note note = data.getParcelableExtra(KEY_EDIT_SCR_NOTE);
          if (mainPresenter == null)
            return;

          if (note.getNoteId() >= 0)
          {
            mainPresenter.updateNote(note);
          }
          else
          {
            mainPresenter.createNote(note);
          }
        }
      }
    }
    else
      super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void refreshChildViews()
  {
    CommonFragment cf = (CommonFragment)getSupportFragmentManager().findFragmentByTag(CommonFragment.TAG);
    if (cf != null && cf instanceof CommonView)
    {
      ((CommonView)cf).refreshData();
    }
  }

  @Override
  public void showErrorMessage(DefaultErrorBundle d)
  {
    Toast.makeText(this, d.getErrorMessage(), Toast.LENGTH_SHORT).show();
  }

  public void swapViews()
  {
    if(fragmentStack.size() > 0)
    {
      //triggerFragmentBackPress(getSupportFragmentManager().getBackStackEntryCount());

      ((BaseMvpFragment)popFromStack()).onBackPressed();
    }

    CommonFragment cf = (CommonFragment)getSupportFragmentManager().findFragmentByTag(CommonFragment.TAG);
    if (cf != null && cf instanceof CommonView)
    {
      ((CommonView)cf).swapViews();
    }
  }

  @Override
  public Location provideLastKnownLocation()
  {
    return lastKnownLocation;
  }

  @Override
  public SettingsLayout.ButtonsClickListener provideModelCallback()
  {
    return settingsClickListener;
  }

  /** Broadcast Receivers **/
  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      String action = intent.getAction();

      if (TrackingService.INTENT_NEW_LOCATION.equals(action))
      {
        lastKnownLocation = intent.getParcelableExtra(TrackingService.EXTRA_LOCATION);
        if (Parameters.DEBUG)
          Log.d(TAG, "new location update: " + lastKnownLocation);
      }
    }
  };


}
