package com.msgeekay.notes.presentationlayer.utils;

import timber.log.Timber;

/**
 * Created by grigoriykatz on 18/05/17.
 */

public class LogUtils
{
  public static final void Timberlg(String procName, String tag, String message, Object... args) {
    Timber.tag(procName + " " + tag).w(message, args);
  }
}
