package com.msgeekay.notes.presentationlayer.ui.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.domainlayer.interactor.CreateNote;
import com.msgeekay.notes.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.notes.domainlayer.interactor.RemoveNote;
import com.msgeekay.notes.domainlayer.interactor.UpdateNote;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.MyApp;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 17/09/17.
 */

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView>
{
  private CreateNote createNote;
  private UpdateNote updateNote;
  private RemoveNote removeNote;

  @Inject
  public MainPresenter(CreateNote createNote, UpdateNote updateNote, RemoveNote removeNote)
  {
    this.createNote = createNote;
    this.updateNote = updateNote;
    this.removeNote = removeNote;

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void createNote(Note note)
  {
    if (createNote != null)
      createNote.execute(note, new NoteUpdateSubscriber());
  }

  public void updateNote(Note note)
  {
    if (updateNote != null)
      updateNote.execute(note, new NoteUpdateSubscriber());
  }

  public void removeNote(Note note)
  {
    if (removeNote != null)
      removeNote.execute(note, new NoteRemoveSubscriber());
  }

  public void stop()
  {
    createNote.unsubscribe();
    updateNote.unsubscribe();
    removeNote.unsubscribe();
  }

  @RxLogSubscriber
  private final class NoteUpdateSubscriber extends DefaultSubscriber<Boolean>
  {

    @Override public void onCompleted() {
      //getViewState().refreshChildViews();

    }

    @Override public void onError(Throwable e) {
      getViewState().refreshChildViews();
      //getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      //getViewState().showViewRetry();
    }

    @Override public void onNext(Boolean result) {
      getViewState().refreshChildViews();
    }
  }

  @RxLogSubscriber
  private final class NoteRemoveSubscriber extends DefaultSubscriber<Note>
  {

    @Override public void onCompleted() {
      //getViewState().refreshChildViews();

    }

    @Override public void onError(Throwable e) {
      getViewState().refreshChildViews();
      getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      //getViewState().showViewRetry();
    }

    @Override public void onNext(Note result) {
      getViewState().refreshChildViews();
    }
  }
}
