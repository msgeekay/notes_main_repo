package com.msgeekay.notes.presentationlayer.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.transition.Transition;
import android.transition.TransitionSet;
//import com.transitionseverywhere.Transition;
//import com.transitionseverywhere.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by grigoriykatz on 09/05/17.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ExtraTransitionUtils {

  private ExtraTransitionUtils() { }

  public static @Nullable
  Transition findTransition(
          @NonNull TransitionSet set, @NonNull Class<? extends Transition> clazz) {
    for (int i = 0; i < set.getTransitionCount(); i++) {
      Transition transition = set.getTransitionAt(i);
      if (transition.getClass() == clazz) {
        return transition;
      }
      if (transition instanceof TransitionSet) {
        Transition child = findTransition((TransitionSet) transition, clazz);
        if (child != null) return child;
      }
    }
    return null;
  }

  public static @Nullable Transition findTransition(
          @NonNull TransitionSet set,
          @NonNull Class<? extends Transition> clazz,
          @IdRes int targetId) {
    for (int i = 0; i < set.getTransitionCount(); i++) {
      Transition transition = set.getTransitionAt(i);
      if (transition.getClass() == clazz) {
        if (transition.getTargetIds().contains(targetId)) {
          return transition;
        }
      }
      if (transition instanceof TransitionSet) {
        Transition child = findTransition((TransitionSet) transition, clazz, targetId);
        if (child != null) return child;
      }
    }
    return null;
  }

  public static List<Boolean> setAncestralClipping(@NonNull View view, boolean clipChildren) {
    return setAncestralClipping(view, clipChildren, new ArrayList<Boolean>());
  }

  private static List<Boolean> setAncestralClipping(
          @NonNull View view, boolean clipChildren, List<Boolean> was) {
    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      was.add(group.getClipChildren());
      group.setClipChildren(clipChildren);
    }
    ViewParent parent = view.getParent();
    if (parent != null && parent instanceof ViewGroup) {
      setAncestralClipping((ViewGroup) parent, clipChildren, was);
    }
    return was;
  }

  public static void restoreAncestralClipping(@NonNull View view, List<Boolean> was) {
    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      group.setClipChildren(was.remove(0));
    }
    ViewParent parent = view.getParent();
    if (parent != null && parent instanceof ViewGroup) {
      restoreAncestralClipping((ViewGroup) parent, was);
    }
  }

  public static class TransitionListenerAdapter implements Transition.TransitionListener {

    @Override public void onTransitionStart(Transition transition) { }

    @Override public void onTransitionEnd(Transition transition) { }

    @Override public void onTransitionCancel(Transition transition) { }

    @Override public void onTransitionPause(Transition transition) { }

    @Override public void onTransitionResume(Transition transition) { }
  }


  /**
   * Following are two methods for creating
   * content transitions used with {@link android.app.ActivityOptions}.
   */


  /**
   * Create the transition participants required during a activity transition while
   * avoiding glitches with the system UI.
   *
   * @param activity The activity used as start for the transition.
   * @param includeStatusBar If false, the status bar will not be added as the transition
   *        participant.
   * @return All transition participants.
   */
  public static Pair<View, String>[] createSafeTransitionParticipants(@NonNull Activity activity,
                                                                      boolean includeStatusBar, @Nullable Pair... otherParticipants) {
    // Avoid system UI glitches as described here:
    // https://plus.google.com/+AlexLockwood/posts/RPtwZ5nNebb
    View decor = activity.getWindow().getDecorView();
    View statusBar = null;
    if (includeStatusBar) {
      statusBar = decor.findViewById(android.R.id.statusBarBackground);
    }
    View navBar = decor.findViewById(android.R.id.navigationBarBackground);

    // Create pair of transition participants.
    List<Pair> participants = new ArrayList<>(3);
    addNonNullViewToTransitionParticipants(statusBar, participants);
    addNonNullViewToTransitionParticipants(navBar, participants);
    // only add transition participants if there's at least one none-null element
    if (otherParticipants != null && !(otherParticipants.length == 1
            && otherParticipants[0] == null)) {
      participants.addAll(Arrays.asList(otherParticipants));
    }
    return participants.toArray(new Pair[participants.size()]);
  }

  private static void addNonNullViewToTransitionParticipants(View view, List<Pair> participants) {
    if (view == null) {
      return;
    }
    participants.add(new Pair<>(view, view.getTransitionName()));
  }
}