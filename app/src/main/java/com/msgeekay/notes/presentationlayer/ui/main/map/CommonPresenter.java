package com.msgeekay.notes.presentationlayer.ui.main.map;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.notes.domainlayer.interactor.GetGoogleMapFullyReady;
import com.msgeekay.notes.domainlayer.interactor.GetNotesList;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.di.common.components.ApplicationComponent;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.MapBitmapCache;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.OnMapAndLayoutReady;
import com.msgeekay.notes.presentationlayer.utils.Parameters;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/09/17.
 */

@InjectViewState
public class CommonPresenter extends MvpPresenter<CommonView>
{

  private List<Note> noteList = new ArrayList<>();
  private GoogleMap googleMap;

  private GetNotesList getNotesList;
  private GetGoogleMapFullyReady getGoogleMapFullyReady;

  private AtomicBoolean mapAlreadyWaits = new AtomicBoolean(false);
  private AtomicBoolean notesListAlreadyWaits = new AtomicBoolean(false);

  private Thread worker = null;

  @Inject
  public CommonPresenter(GetNotesList getNotesList, GetGoogleMapFullyReady getGoogleMapFullyReady)
  {
    this.getNotesList = getNotesList;
    this.getGoogleMapFullyReady = getGoogleMapFullyReady;

    MyApp.get().getApplicationComponent().inject(this);
  }

  private void reInitMTFlags()
  {
    mapAlreadyWaits = new AtomicBoolean(false);
    notesListAlreadyWaits = new AtomicBoolean(false);
  }



  public void stop()
  {
    getNotesList.unsubscribe();
    getGoogleMapFullyReady.unsubscribe();
    if (worker != null && worker.isAlive())
    {
      worker.interrupt();
      worker = null;
    }
  }

  public void complexInit(SupportMapFragment mapFragment)
  {
    reInitMTFlags();
    initWaitThread(true);
    initGoogleMap(mapFragment);
    getNotes();
  }

  public void complexInitNotesRefresh()
  {
    reInitMTFlags();
    mapAlreadyWaits = new AtomicBoolean(true);

    initWaitThread(true);
    //initGoogleMap(mapFragment);
    getNotes();
  }

  public void initGoogleMap(SupportMapFragment mapFragment)
  {
    OnMapAndLayoutReady.onMapAndLayoutReadyObservable(mapFragment).subscribe(new GMapsSubscriber());
    //getGoogleMapFullyReady.execute(mapFragment, new GMapsSubscriber());
  }

  public void getNotes() {
    getNotesList.execute(null, new NoteListSubscriber());
  }

  private void initWaitThread(final boolean isRefresh)
  {
    final Handler h = new Handler(Looper.getMainLooper());

    (worker = new Thread(new Runnable()
    {
      @Override
      public void run()
      {
        int increment = 0;
        while (!(mapAlreadyWaits.get() && notesListAlreadyWaits.get()))
        {
          increment++;
          try
          {
            Thread.sleep(1000);
          }
          catch (Exception ex)
          {
            //do nothing
          }
          finally
          {
            int calcTruth = 0;
            calcTruth = (mapAlreadyWaits.get() ? 1 : 0)
                    + (notesListAlreadyWaits.get() ? 1 : 0);

            if (increment > 15 && calcTruth < 2 )
              break;

          }
        }

        h.post(new Runnable()
        {
          @Override
          public void run()
          {
            runAfterAllChecks(isRefresh);
          }
        });

        reInitMTFlags();

      }
    })).start();
  }

  private void runAfterAllChecks(final boolean isRefresh)
  {
    if (getViewState() != null)
    {
      getViewState().setNotes(noteList);
      getViewState().onMapReady(googleMap);
    }
  }



  public void onBackPressedWithScene() {
    getViewState().onBackPressedWithScene(provideLatLngBoundsForAllPlaces());
  }

  public LatLngBounds provideLatLngBoundsForAllPlaces() {
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    if (noteList != null && noteList.size() > 0)
    {
      for (Note n : noteList)
      {
        if (n.getNoteLocation() != null)
          builder.include(n.getNoteLocation());
      }
    }
    else
    {
      builder.include(new LatLng(Parameters.defaultLat1, Parameters.defaultLon1));
      builder.include(new LatLng(Parameters.defaultLat2, Parameters.defaultLon2));
    }

    return builder.build();
  }

  public List<Note> getLastNoteList()
  {
    return noteList;
  }

  public void saveBitmap(final Bitmap bitmap) {
    MapBitmapCache.instance().putBitmap(bitmap);
  }

  public void moveMapAndAddMarker() {
    getViewState().moveMapAndAddMaker(provideLatLngBoundsForAllPlaces());
  }

  @RxLogSubscriber
  private final class GMapsSubscriber extends DefaultSubscriber<GoogleMap>
  {
    @Override
    public void onCompleted()
    {
      super.onCompleted();
    }

    @Override
    public void onNext(GoogleMap googleMap)
    {
      CommonPresenter.this.googleMap = googleMap;
      mapAlreadyWaits = new AtomicBoolean(true);
    }

    @Override
    public void onError(Throwable e)
    {
      mapAlreadyWaits = new AtomicBoolean(true);
    }
  }

  @RxLogSubscriber
  private final class NoteListSubscriber extends DefaultSubscriber<List<Note>>
  {

    @Override public void onCompleted() {
      //getViewState().hideViewLoading();

    }

    @Override public void onError(Throwable e) {
      //getViewState().hideViewLoading();
      notesListAlreadyWaits = new AtomicBoolean(true);
      if (getViewState() != null)
        getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      ////getViewState().showViewRetry();
    }

    @Override public void onNext(List<Note> noteList) {

      CommonPresenter.this.noteList = noteList;
      notesListAlreadyWaits = new AtomicBoolean(true);
      //getViewState().setNotes(noteList);
    }
  }




}