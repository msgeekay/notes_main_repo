package com.msgeekay.notes.presentationlayer.ui.splash;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.msgeekay.notes.BuildConfig;
import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.notes.presentationlayer.ui.splash.common.BasePermissionsActivity;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;


/**
 * Created by grigoriykatz on 11/05/17.
 */

public class SplashActivity extends BasePermissionsActivity implements SplashView
{
  private static final int RC_LOCATION_PERMS = 123;
  private static final int RC_INTERNET_PERMS = 124;
  private static final int RC_STORAGE_PERMS = 125;

  private int permCounter = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    setTheme(R.style.AppThemeSplash_BrandedLaunch);
    super.onCreate(savedInstanceState);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_splash);
    ButterKnife.bind(this);
  }

  @InjectPresenter(type = PresenterType.GLOBAL)
  SplashPresenter splashPresenter;

  @ProvidePresenter(type = PresenterType.GLOBAL)
  SplashPresenter provideSplashPresenter() {
    CustomPresenterFactory cpf = new CustomPresenterFactory();
    return cpf.provideSplashPresenter();
  }

  @Override
  public void permissionsCheckedOk()
  {

  }

  @Override
  protected void onResume()
  {
    super.onResume();

    (new Handler()).postDelayed(new Runnable()
    {
      @Override
      public void run()
      {
        if (BuildConfig.VERSION_CODE >= Build.VERSION_CODES.M)
        {
          //storagePermissionsCheck();
          internetPermissionsCheck();
        }
        else
        {
          SplashActivity.this.getNavigator().startTrackService(SplashActivity.this.getBaseContext());
          permCounter = 3;
          finalStep();
        }

      }
    }, 1000);
  }

  @AfterPermissionGranted(RC_LOCATION_PERMS)
  public void locationPermissionsCheck()
  {
    String[] perms = {  Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE
    };
    if (EasyPermissions.hasPermissions(this, perms))
    {

      // Have permissions, do the thing!
      Timber.d("LOCATION permissions already granted");

      this.getNavigator().startTrackService(this);
      permCounter++;

      finalStep();
    }
    else
    {
      // Ask for permissions
      EasyPermissions.requestPermissions(this, getString(R.string.perms_rationale_location),
              RC_LOCATION_PERMS, perms);
    }
  }

  @AfterPermissionGranted(RC_INTERNET_PERMS)
  public void internetPermissionsCheck()
  {
    String[] perms = {  Manifest.permission.INTERNET  };
    if (EasyPermissions.hasPermissions(this, perms)) {

      // Have permissions, do the thing!
      Timber.d("INTERNET permission already granted");
      permCounter++;
      locationPermissionsCheck();
    } else {
      // Ask for permissions
      EasyPermissions.requestPermissions(this, getString(R.string.perms_rationale_inet),
              RC_INTERNET_PERMS, perms);
    }
  }

  @AfterPermissionGranted(RC_STORAGE_PERMS)
  public void storagePermissionsCheck()
  {
    String[] perms = {  Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    if (EasyPermissions.hasPermissions(this, perms)) {

      // Have permissions, do the thing!
      Timber.d("STORAGE permission already granted");
      permCounter++;
      internetPermissionsCheck();
    }
    else
    {
      // Ask for permissions
      EasyPermissions.requestPermissions(this, getString(R.string.perms_rationale_storage),
              RC_STORAGE_PERMS, perms);
    }
  }

  public void finalStep()
  {
    if (splashPresenter != null)
      splashPresenter.startTracking();

    if (permCounter == 3)
      this.getNavigator().navigateToMainScreen(this);
    finish();
  }


}
