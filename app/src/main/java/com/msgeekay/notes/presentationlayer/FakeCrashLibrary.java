package com.msgeekay.notes.presentationlayer;

/**
 * Created by grigoriykatz on 18/05/17.
 */


import android.util.Log;

/** Not a real crash reporting library! */
public final class FakeCrashLibrary {
  public static void log(int priority, String tag, String message) {
    // TODO add log entry to circular buffer.
    Log.d(tag, message);
  }

  public static void logWarning(String tag, String message, Throwable t) {
    // TODO report non-fatal warning.
    Log.w(tag, message, t);
  }

  public static void logError(String tag, String message, Throwable t) {
    // TODO report non-fatal error.
    Log.e(tag, message, t);
  }

  private FakeCrashLibrary() {
    //throw new AssertionError("No instances.");
  }
}
