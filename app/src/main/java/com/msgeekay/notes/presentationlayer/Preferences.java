package com.msgeekay.notes.presentationlayer;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Created by grigoriykatz on 09/05/17.
 */

// TODO: make preferences more secure

public class Preferences {
  private static final String SHARED_NAME = "preferences.xml";
  private static final String AUTH_TOKEN = "access_token";
  private static final String AUTH_CLIENT = "client";
  private static final String AUTH_UID = "uid";
  private static final String CURRENT_USER_ID = "current_user_id";
  private static final String FCM_TOKEN_SENT = "fcm_token_sent";
  private static final String FCM_TOKEN_NEW = "fcm_token_new";
  private static final String FCM_TOKEN_OLD = "fcm_token_old";
  private static final String ONBOARDING_SHOWN = "login_skipped";

  private static final String FLASH_MODE = "squarecamera__flash_mode";
  private static final String LAST_NOTE_ID = "LAST_NOTE_ID";

  private SharedPreferences mSharedPreferences;

  public Preferences(Context context) {
    mSharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
  }

  public void setAuthToken(String accessToken) {
    mSharedPreferences.edit().putString(AUTH_TOKEN, accessToken).apply();
  }

  public String getAuthToken() {
    return mSharedPreferences.getString(AUTH_TOKEN, "");
  }

  public void setAuthClient(String client) {
    mSharedPreferences.edit().putString(AUTH_CLIENT, client).apply();
  }

  public String getAuthClient() {
    return mSharedPreferences.getString(AUTH_CLIENT, "");
  }

  public void setAuthUid(String uid) {
    mSharedPreferences.edit().putString(AUTH_UID, uid).apply();
  }

  public String getAuthUid() {
    return mSharedPreferences.getString(AUTH_UID, "");
  }

  public boolean isAuthorized() {
    return !TextUtils.isEmpty(getAuthToken());
  }

  public void setFcmTokenSent(boolean sent) {
    mSharedPreferences.edit().putBoolean(FCM_TOKEN_SENT, sent).apply();
  }

  public boolean isFcmTokenSent() {
    return mSharedPreferences.getBoolean(FCM_TOKEN_SENT, false);
  }

  public void setFcmTokenNew(String token) {
    mSharedPreferences.edit().putString(FCM_TOKEN_NEW, token).apply();
  }

  public String getFcmTokenNew() {
    return mSharedPreferences.getString(FCM_TOKEN_NEW, null);
  }

  public void setFcmTokenOld(String token) {
    mSharedPreferences.edit().putString(FCM_TOKEN_OLD, token).apply();
  }

  public String getFcmTokenOld() {
    return mSharedPreferences.getString(FCM_TOKEN_OLD, null);
  }

  public void setCurrentUserId(long userId) {
    mSharedPreferences.edit().putLong(CURRENT_USER_ID, userId).apply();
  }

  public long getCurrentUserId() {
    return mSharedPreferences.getLong(CURRENT_USER_ID, 0);
  }

  public void setOnboardingShown(boolean shown) {
    mSharedPreferences.edit().putBoolean(ONBOARDING_SHOWN, shown).apply();
  }

  public boolean isOnboardingShown() {
    return mSharedPreferences.getBoolean(ONBOARDING_SHOWN, false);
  }

  public void clear() {
    mSharedPreferences.edit().clear().apply();
  }


  /****
   * Set of static methods
   *
   */
  public static final String IS_TRACKING = "IS_TRACK";

  public static class StaticAccess
  {
    public static boolean getBoolean(String name, Context context, boolean defaultValue)
    {
      SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
      return sharedPreferences.getBoolean(name, defaultValue);
    }

    public static void setBoolean(String name, Context context, boolean preferenceValue) {
      SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putBoolean(name, preferenceValue);
      editor.apply();
    }

  }

  public static void saveCameraFlashMode(@NonNull final Context context, @NonNull final String cameraFlashMode) {
    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

    if (preferences != null) {
      final SharedPreferences.Editor editor = preferences.edit();
      editor.putString(FLASH_MODE, cameraFlashMode);
      editor.apply();
    }
  }

  public static String getCameraFlashMode(@NonNull final Context context) {
    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());

    if (preferences != null) {
      return preferences.getString(FLASH_MODE, Camera.Parameters.FLASH_MODE_AUTO);
    }

    return Camera.Parameters.FLASH_MODE_AUTO;
  }

  public void setLastNoteId(long noteId)
  {
    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyApp.getInstance().getApplicationContext());

    if (preferences != null)
    {
      final SharedPreferences.Editor editor = preferences.edit();
      editor.putLong(LAST_NOTE_ID, noteId);
      editor.apply();
    }

  }

  public long getLastNoteId()
  {
    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyApp.getInstance().getApplicationContext());

    if (preferences != null) {
      return preferences.getLong(LAST_NOTE_ID, 1000);
    }
    return 10000;
  }
}