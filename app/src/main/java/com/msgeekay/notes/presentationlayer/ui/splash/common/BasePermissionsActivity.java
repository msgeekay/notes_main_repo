package com.msgeekay.notes.presentationlayer.ui.splash.common;

import android.content.Intent;
import android.widget.Toast;

import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;

/**
 * Created by grigoriykatz on 16/05/17.
 */

public abstract class BasePermissionsActivity extends BaseMvpActivity implements EasyPermissions.PermissionCallbacks
{
  public final static String TAG = "com.msgkatz.courier.BasePermissionsActivity";

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    // Forward results to EasyPermissions
    EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
  }

  @Override
  public void onPermissionsGranted(int requestCode, List<String> perms)
  {
    Timber.d(TAG + " onPermissionsGranted:" + requestCode + ":" + perms.size());
  }

  @Override
  public void onPermissionsDenied(int requestCode, List<String> perms)
  {
    Timber.d(TAG + " onPermissionsDenied:" + requestCode + ":" + perms.size());

    // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
    // This will display a dialog directing them to enable the permission in app settings.
    if (EasyPermissions.somePermissionPermanentlyDenied(this, perms))
    {
      new AppSettingsDialog.Builder(BasePermissionsActivity.this).build().show();
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE)
    {
      // Do something after user returned from app settings screen, like showing a Toast.
      Toast.makeText(this, R.string.perms_returned_from_app_settings_to_activity, Toast.LENGTH_SHORT)
              .show();
    }
  }
}
