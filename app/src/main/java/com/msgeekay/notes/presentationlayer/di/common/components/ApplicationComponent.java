package com.msgeekay.notes.presentationlayer.di.common.components;

import android.content.Context;

import com.msgeekay.notes.datalayer.net.Api;
import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.notes.presentationlayer.Preferences;
import com.msgeekay.notes.presentationlayer.di.common.modules.ApplicationModule;
import com.msgeekay.notes.presentationlayer.di.common.modules.UseCasesModule;
import com.msgeekay.notes.presentationlayer.ui.common.CustomPresenterFactory;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;
import com.msgeekay.notes.presentationlayer.ui.editnote.EditPresenter;
import com.msgeekay.notes.presentationlayer.ui.main.map.CommonPresenter;
import com.msgeekay.notes.presentationlayer.ui.main.map.CommonView;
import com.msgeekay.notes.presentationlayer.ui.main.MainPresenter;
import com.msgeekay.notes.presentationlayer.ui.splash.SplashPresenter;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * A component whose lifetime is the life of the application.
 */

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {ApplicationModule.class, UseCasesModule.class})
public interface ApplicationComponent
{
  void inject(BaseMvpActivity baseActivity);
  void inject(CustomPresenterFactory x);
  void inject(SplashPresenter x);
  void inject(MainPresenter x);
  void inject(CommonPresenter x);
  void inject(EditPresenter x);

  //Exposed to sub-graphs.
  Context context();
  ThreadExecutor threadExecutor();
  PostExecutionThread postExecutionThread();
  Preferences preferences();
  ServiceCommunicationManager serviceCommunicationManager();
  Api api();
}
