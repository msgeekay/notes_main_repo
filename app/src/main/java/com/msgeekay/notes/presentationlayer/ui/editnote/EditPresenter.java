package com.msgeekay.notes.presentationlayer.ui.editnote;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.msgeekay.notes.domainlayer.exception.DefaultErrorBundle;
import com.msgeekay.notes.domainlayer.interactor.CreateNote;
import com.msgeekay.notes.domainlayer.interactor.DefaultSubscriber;
import com.msgeekay.notes.domainlayer.interactor.ExportNote;
import com.msgeekay.notes.domainlayer.interactor.UpdateNote;
import com.msgeekay.notes.infrastructurelayer.ServiceCommunicationManager;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.MyApp;
import com.msgeekay.notes.presentationlayer.ui.main.MainPresenter;

import javax.inject.Inject;

/**
 * Created by grigoriykatz on 18/09/17.
 */
@InjectViewState
public class EditPresenter extends MvpPresenter<EditView>
{
  private ServiceCommunicationManager serviceCommunicationManager;
  private CreateNote createNote;
  private UpdateNote updateNote;
  private ExportNote exportNote;

  @Inject
  public EditPresenter(ServiceCommunicationManager serviceCommunicationManager,
                  CreateNote createNote, UpdateNote updateNote, ExportNote exportNote)
  {
    this.serviceCommunicationManager = serviceCommunicationManager;
    this.createNote = createNote;
    this.updateNote = updateNote;
    this.exportNote = exportNote;

    MyApp.get().getApplicationComponent().inject(this);
  }

  public void createNote(Note note)
  {
    if (createNote != null)
      createNote.execute(note, new EditPresenter.NoteCreateSubscriber());
  }

  public void updateNote(Note note)
  {
    if (updateNote != null)
      updateNote.execute(note, new EditPresenter.NoteUpdateSubscriber());
  }

  public void exportNote(Note note)
  {
    if (exportNote != null)
      exportNote.execute(note, new EditPresenter.NoteExportSubscriber());
  }

  public void stop()
  {
    createNote.unsubscribe();
    updateNote.unsubscribe();
    exportNote.unsubscribe();
  }

  public void startLocationTracking()
  {
    if (serviceCommunicationManager != null)
      serviceCommunicationManager.startTracking();
  }

  /****************
   * Set of subscribers
   */

  @RxLogSubscriber
  private final class NoteCreateSubscriber extends DefaultSubscriber<Note>
  {

    @Override public void onCompleted() {
      //getViewState().refreshChildViews();

    }

    @Override public void onError(Throwable e) {
      getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      //getViewState().showViewRetry();
    }

    @Override public void onNext(Note note) {
      getViewState().afterNoteCreation(note);
    }
  }

  /****************
   *
   */

  @RxLogSubscriber
  private final class NoteUpdateSubscriber extends DefaultSubscriber<Boolean>
  {

    @Override public void onCompleted() {
      //getViewState().refreshChildViews();

    }

    @Override public void onError(Throwable e) {
      getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      //getViewState().showViewRetry();
    }

    @Override public void onNext(Boolean result) {
      getViewState().afterNoteUpdate(result);
    }
  }

  /****************
   *
   */
  @RxLogSubscriber
  private final class NoteExportSubscriber extends DefaultSubscriber<Uri>
  {

    @Override public void onCompleted() {
      //getViewState().refreshChildViews();

    }

    @Override public void onError(Throwable e) {
      getViewState().showErrorMessage(new DefaultErrorBundle((Exception) e));
      //getViewState().showViewRetry();
    }

    @Override public void onNext(Uri result) {
      getViewState().afterNoteExport(result);
    }
  }
}
