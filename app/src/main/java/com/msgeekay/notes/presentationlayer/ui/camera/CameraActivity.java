package com.msgeekay.notes.presentationlayer.ui.camera;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.ui.common.mvp.BaseMvpActivity;

import java.util.Date;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class CameraActivity extends BaseMvpActivity
{
  public static final String TAG = CameraActivity.class.getSimpleName();
  public static final String KEY_PHOTO_PATH = "KEY_PHOTO_PATH";

  private Context mContext;

  static
  {
    System.loadLibrary("NativeImageProcessor");
  }

  public static Intent getCallingIntent(Context context) {

    return getCallingIntentBase(context);
  }

  public static Intent getCallingIntentBase(Context context) {
    Intent callingIntent = new Intent(context, CameraActivity.class);

    return callingIntent;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    setContentView(R.layout.activity_cam);

    mContext = getApplicationContext();

    if (savedInstanceState == null)
    {
      getSupportFragmentManager()
              .beginTransaction()
              .replace(R.id.fragment_container, CameraFragment.newInstance(), CameraFragment.TAG)
              .commit();
    }

  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);


  }

  @Override
  public void permissionsCheckedOk()
  {

  }

  public void returnPhotoPaths(String photoPath) {
    Intent i = new Intent();
    i.putExtra(KEY_PHOTO_PATH, photoPath);
    setResult(RESULT_OK, i);
    finish();
  }

  public void returnAndShare(String photoPath, String thumbnailPath) {

    returnPhotoPaths(photoPath);
  }

  public void onCancel(View view) {
    getSupportFragmentManager().popBackStack();
  }

  public void showResult(String title, String alertMessage) {


  }


}
