package com.msgeekay.notes.presentationlayer.ui.common.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.msgeekay.notes.R;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.presentationlayer.ui.common.anim.spring.SpringAnimationType;
import com.msgeekay.notes.presentationlayer.ui.common.anim.spring.SpringyAnimator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by grigoriykatz on 16/09/17.
 */

public class DeleteDialog extends AppCompatDialogFragment
{

  public interface OnDeleteCallback
  {
    void processDeletion(Note note);
  }

  private static final String INTENT_NOTE = "INTENT_NOTE";

  @BindView(R.id.parentLayout) LinearLayout parentLayout;
  @BindView(R.id.dialog_delete_yes_delete) Button btnDelete;
  @BindView(R.id.dialog_delete_cancel) Button btnCancel;

  private List<View> views = new ArrayList<>();
  private OnDeleteCallback onDeleteCallback = null;
  private Note note;

  public static DeleteDialog newInstance(Note note) {
    Bundle args = new Bundle();
    args.putParcelable(INTENT_NOTE, note);
    DeleteDialog dlg = new DeleteDialog();
    dlg.setArguments(args);
    return dlg;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null)
    {
      note = getArguments().getParcelable(INTENT_NOTE);
    }
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    //AppCompatDialog dialog = new AppCompatDialog(getActivity(), R.style.AppTheme_NoActionBar_DialogPopup);
    AppCompatDialog dialog = new AppCompatDialog(getActivity(), R.style.MoreDialog);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.layout_dialog_delete);
    dialog.getWindow().setBackgroundDrawableResource(R.color.background_dialog);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    ButterKnife.bind(this, dialog);
    return dialog;
  }

  @Override
  public void onStart() {
    super.onStart();

    views.clear();
    int cnt = parentLayout.getChildCount();
    for (int i = 0; i < cnt; i++)
    {
      if (parentLayout.getChildAt(i).getVisibility() == View.VISIBLE)
      {
        parentLayout.getChildAt(i).setTranslationY(getResources().getDisplayMetrics().heightPixels);
        views.add(parentLayout.getChildAt(i));
      }
    }

    final SpringyAnimator spring = new SpringyAnimator(SpringAnimationType.TRANSLATEY, 10, 5, getResources().getDisplayMetrics().heightPixels, 0);
    for (int i = 0; i < views.size(); i++)
    {
      final int count = i;
      views.get(i).postDelayed(new Runnable()
      {
        @Override
        public void run()
        {
          spring.startSpring(views.get(count));
        }
      }, 100 * i);
    }
  }

  @Override
  public void onStop() {
    super.onStop();

  }

  @OnClick(R.id.dialog_delete_yes_delete)
  public void onDeleteClick()
  {
    if (note != null)
    {

      if (onDeleteCallback != null)
      {
        onDeleteCallback.processDeletion(note);
      }
      dismiss();
    }
  }

  public void setOnDeleteCallback(OnDeleteCallback onDeleteCallback)
  {
    this.onDeleteCallback = onDeleteCallback;
  }

  @OnClick(R.id.dialog_delete_cancel)
  public void onCancelClick() {
    dismiss();
  }
}