package com.msgeekay.notes.datalayer.db;

import com.msgeekay.notes.domainlayer.interactor.CreateNote;
import com.msgeekay.notes.model.data.NoteEntity;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.sql.language.Delete;

/**
 * Created by grigoriykatz on 18/09/17.
 */

@Database(name = NotesDatabase.NAME, version = NotesDatabase.VERSION,
        insertConflict = ConflictAction.IGNORE, updateConflict= ConflictAction.REPLACE)
public class NotesDatabase {
  static final String NAME = "notes2_db";

  static final int VERSION = 1;

  public static void clear() {
    Delete.tables(NoteEntity.class);

    //TODO implement
  }
}

