package com.msgeekay.notes.datalayer.repo;

import android.net.Uri;

import com.msgeekay.notes.datalayer.repo.datastore.DataStore;
import com.msgeekay.notes.datalayer.repo.datastore.DataStoreFactory;
import com.msgeekay.notes.domainlayer.repo.Repository;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.data.mapping.NoteEntityDataMapper;

import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;


/**
 * Created by grigoriykatz on 17/09/17.
 */
@Singleton
public class DataRepository implements Repository
{
  private final DataStoreFactory dataStoreFactory;

  @Inject
  public DataRepository(DataStoreFactory dataStoreFactory)
  {
    this.dataStoreFactory = dataStoreFactory;
  }

  @Override
  public Observable<List<Note>> notes()
  {
    final DataStore ds = dataStoreFactory.createNotesStore();
    return ds.noteEntities().map(noteEntities -> NoteEntityDataMapper.transform(noteEntities));
  }

  @Override
  public Observable<Note> note(int noteId)
  {
    return null;
  }

  @Override
  public Observable<Note> noteInsert(Note note)
  {
    final DataStore ds = dataStoreFactory.createNotesStore();
    return ds.noteEntityInsert(NoteEntityDataMapper.transform(note)).map(noteEntities -> NoteEntityDataMapper.transform(noteEntities));
  }

  @Override
  public Observable<Boolean> noteUpdate(Note note)
  {
    final DataStore ds = dataStoreFactory.createNotesStore();
    return ds.noteEntityUpdate(NoteEntityDataMapper.transform(note));
  }

  @Override
  public Observable<Note> noteRemove(Note note)
  {
    final DataStore ds = dataStoreFactory.createNotesStore();
    return ds.noteEntityRemove(NoteEntityDataMapper.transform(note)).map(noteEntities -> NoteEntityDataMapper.transform(noteEntities));
  }

  @Override
  public Observable<Uri> noteExport(Note note)
  {
    final DataStore ds = dataStoreFactory.createNotesStore(DataStoreFactory.STORE_TYPE_DISK);

    StringBuilder sb = new StringBuilder();
    sb.append(note.getNoteTitle());
    sb.append("\n");
    sb.append(new Date(note.getTimestamp()));
    sb.append("\n");
    sb.append(note.getNoteBody());

    return ds.dataExport(note.getNoteTitle(), sb.toString());
  }
}
