package com.msgeekay.notes.datalayer.exception;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class NotesException extends Exception
{
  public NotesException() {
    super();
  }

  public NotesException(final String message) {
    super(message);
  }

  public NotesException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public NotesException(final Throwable cause) {
    super(cause);
  }
}
