package com.msgeekay.notes.datalayer.repo.datastore;

import android.content.Context;
import android.provider.ContactsContract;

import com.msgeekay.notes.datalayer.cache.DataCache;
import com.msgeekay.notes.datalayer.db.DBApi;
import com.msgeekay.notes.datalayer.db.DBApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by grigoriykatz on 18/09/17.
 *
 * Factory for different store implementations
 */

@Singleton
public class DataStoreFactory
{
  public final static int STORE_TYPE_DB    = 0;
  public final static int STORE_TYPE_DISK  = 1;

  private final Context context;
  private final DataCache dataCache;

  @Inject
  public DataStoreFactory(Context context, DataCache dataCache)
  {
    if (context == null) {
      throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
    }
    this.context = context.getApplicationContext();
    this.dataCache = dataCache;
  }

  /**
   * Create {@link DataStore} for notes.
   */
  public DataStore createNotesStore()
  {
    return createNotesStore(STORE_TYPE_DB);
  }


  public DataStore createNotesStore(int storeType)
  {
    DataStore notesDataStore;

    //TODO: great place to put various implementations
    //TODO: depending on various conditions

    if (storeType == STORE_TYPE_DB)
    {
      notesDataStore = createDBNotesDataStore();
    }
    else if (storeType == STORE_TYPE_DISK)
    {
      notesDataStore = createDiskNotesDataStore();
    }
    else
      notesDataStore = createDBNotesDataStore();

    return notesDataStore;
  }

  private DataStore createDBNotesDataStore()
  {
    DBApi dbApi = new DBApiImpl();
    return new NotesDBDataStore(dbApi);
  }

  private DataStore createDiskNotesDataStore()
  {
    return new NotesDiskDataStore(this.dataCache);
  }
}
