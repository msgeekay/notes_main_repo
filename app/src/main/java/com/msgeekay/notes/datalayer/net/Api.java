package com.msgeekay.notes.datalayer.net;

import android.content.Context;

import com.google.gson.JsonSyntaxException;
import com.msgeekay.notes.datalayer.exception.NetworkConnectionException;
import com.msgeekay.notes.infrastructurelayer.receivers.ConnectionChangeReceiver;
import com.msgeekay.notes.model.Note;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 16/05/17.
 */
@Singleton
public class Api implements BaseApi
{
  //@Inject
  Context context;

  public Api(Context context)
  {
    this.context = context;
  }

  public Observable<List<Note>> notes()
  {
    final List<Note> responseNotes = new ArrayList<>();
    return Observable.create(new Observable.OnSubscribe<List<Note>>()
    {
      @Override
      public void call(Subscriber<? super List<Note>> subscriber)
      {
        if (ConnectionChangeReceiver.isInternetConnectedSilent(context))
        {
          try
          {
            //responseOrders.addAll(data);

            if (responseNotes != null)
            {
              subscriber.onNext(responseNotes);
              subscriber.onCompleted();

              //FIXME: implement real-world api

            }
            else
            {
              subscriber.onError(new NetworkConnectionException());
            }

          }
          catch (Exception e)
          {
            subscriber.onError(new NetworkConnectionException(e.getCause()));
          }
        }
        else
        {
          subscriber.onError(new NetworkConnectionException());
        }
      }
    });
  }

  public void method()
  {

  };

  public Observable<List<Note>> emptyNotes()
  {
    final List<Note> responseNotes = new ArrayList<>();
    return Observable.create(new Observable.OnSubscribe<List<Note>>()
    {
      @Override
      public void call(Subscriber<? super List<Note>> subscriber)
      {
        subscriber.onNext(responseNotes);
        subscriber.onCompleted();
      }
    });

  }


}
