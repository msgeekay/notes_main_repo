package com.msgeekay.notes.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.notes.datalayer.db.DBApi;
import com.msgeekay.notes.model.data.NoteEntity;

import java.net.URI;
import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class NotesDBDataStore implements DataStore
{
  private final DBApi dbApi;

  public NotesDBDataStore(DBApi dbApi)
  {
    this.dbApi = dbApi;
  }

  @Override
  public Observable<List<NoteEntity>> noteEntities()
  {
    if (dbApi == null)
      return Observable.error(new Exception("internal error"));

    return dbApi.noteEntities();
  }

  @Override
  public Observable<NoteEntity> noteEntity(int noteId)
  {
    if (dbApi == null)
      return Observable.error(new Exception("internal error"));

    return dbApi.noteEntity(noteId);
  }

  @Override
  public Observable<NoteEntity> noteEntityInsert(NoteEntity noteEntity)
  {
    if (dbApi == null || noteEntity == null)
      return Observable.error(new Exception("internal error"));
    return dbApi.noteInsert(noteEntity);
  }

  @Override
  public Observable<Boolean> noteEntityUpdate(NoteEntity noteEntity)
  {
    if (dbApi == null || noteEntity == null)
      return Observable.error(new Exception("internal error"));
    return dbApi.noteUpdate(noteEntity);
  }

  @Override
  public Observable<NoteEntity> noteEntityRemove(NoteEntity noteEntity)
  {
    if (dbApi == null || noteEntity == null)
      return Observable.error(new Exception("internal error"));
    return dbApi.noteEntityRemove(noteEntity);
  }

  @Override
  public Observable<Uri> dataExport(String containerName, String data)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }
}
