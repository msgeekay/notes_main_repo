package com.msgeekay.notes.datalayer.cache;

import android.net.Uri;

import java.net.URI;

import rx.Observable;

/**
 * Created by grigoriykatz on 19/09/17.
 */

public interface DataCache
{
  Observable<Uri> put(String containerName, String content);

}
