package com.msgeekay.notes.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.notes.datalayer.cache.DataCache;
import com.msgeekay.notes.model.data.NoteEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class NotesDiskDataStore implements DataStore
{
  private DataCache dataCache;

  public NotesDiskDataStore(DataCache dataCache)
  {
    this.dataCache = dataCache;
  }

  @Override
  public Observable<List<NoteEntity>> noteEntities()
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<NoteEntity> noteEntity(int noteId)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<NoteEntity> noteEntityInsert(NoteEntity noteEntity)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<Boolean> noteEntityUpdate(NoteEntity noteEntity)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<NoteEntity> noteEntityRemove(NoteEntity noteEntity)
  {
    throw new UnsupportedOperationException("Operation is not available!!!");
  }

  @Override
  public Observable<Uri> dataExport(String containerName, String data)
  {
    return this.dataCache.put(containerName, data);
  }
}
