package com.msgeekay.notes.datalayer.db;

import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.data.NoteEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public interface DBApi
{
  Observable<List<NoteEntity>> noteEntities();
  Observable<NoteEntity> noteEntity(final int noteId);

  Observable<NoteEntity> noteInsert(final NoteEntity note);
  Observable<Boolean> noteUpdate(final NoteEntity note);
  Observable<NoteEntity> noteEntityRemove(final NoteEntity note);

  List<Long> getEntitiesByParamsSync(long minTime);
  Boolean setEntitiesByValue(long id, double lat, double lon);

}
