package com.msgeekay.notes.datalayer.exception;

/**
 * Created by grigoriykatz on 16/05/17.
 * Exception throw by the application when a User can't login to app.
 */
public class UserNotFoundException extends Exception {

  public UserNotFoundException() {
    super();
  }

  public UserNotFoundException(final String message) {
    super(message);
  }

  public UserNotFoundException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public UserNotFoundException(final Throwable cause) {
    super(cause);
  }
}