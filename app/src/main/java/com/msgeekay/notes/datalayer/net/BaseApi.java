package com.msgeekay.notes.datalayer.net;

import com.msgeekay.notes.model.Note;

import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 14/05/17.
 */

public interface BaseApi
{
  public Observable<List<Note>> notes();
  public void method();

  public Observable<List<Note>> emptyNotes();

}
