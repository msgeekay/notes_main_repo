package com.msgeekay.notes.datalayer.repo.datastore;

import android.net.Uri;

import com.msgeekay.notes.model.data.NoteEntity;

import java.net.URI;
import java.util.List;
import rx.Observable;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface DataStore
{
  Observable<List<NoteEntity>> noteEntities();
  Observable<NoteEntity> noteEntity(final int noteId);

  Observable<NoteEntity> noteEntityInsert(final NoteEntity noteEntity);
  Observable<Boolean> noteEntityUpdate(final NoteEntity noteEntity);
  Observable<NoteEntity> noteEntityRemove(final NoteEntity noteEntity);

  Observable<Uri> dataExport(String containerName, String data);
}
