package com.msgeekay.notes.datalayer.cache;

import android.content.Context;
import android.net.Uri;

import com.msgeekay.notes.datalayer.exception.NotesException;

import java.io.File;
import java.net.URI;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 17/09/17.
 */

@Singleton
public class DataCacheImpl implements DataCache
{
  public Context context;
  public FileManager fileManager;

  @Inject
  public DataCacheImpl(Context context, FileManager fileManager)
  {
    this.context = context;
    this.fileManager = fileManager;
  }

  @Override
  public Observable<Uri> put(String containerName, String content)
  {
    return Observable.create(new Observable.OnSubscribe<Uri>()
    {
      @Override
      public void call(Subscriber<? super Uri> subscriber)
      {
        Uri retVal = DataCacheImpl.this.fileManager.writeData(containerName, content);

        if (retVal != null) {
          subscriber.onNext(retVal);
          subscriber.onCompleted();
        } else {
          subscriber.onError(new NotesException("Smth went wrong. Pls look into logs"));
        }

      }
    });

  }
}
