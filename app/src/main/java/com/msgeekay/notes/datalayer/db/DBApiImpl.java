package com.msgeekay.notes.datalayer.db;

import android.util.Log;

import com.msgeekay.notes.datalayer.exception.NotesException;
import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.data.NoteEntity;
//import com.msgeekay.notes.model.data.NoteEntity_Table;
import com.msgeekay.notes.model.data.NoteEntity_Table;
import com.msgeekay.notes.presentationlayer.utils.Parameters;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.list.FlowCursorList;
import com.raizlabs.android.dbflow.sql.language.Operator; //.Condition;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class DBApiImpl implements DBApi
{
  @Override
  public Observable<List<NoteEntity>> noteEntities()
  {
    return Observable.create(new Observable.OnSubscribe<List<NoteEntity>>()
    {
      @Override
      public void call(Subscriber<? super List<NoteEntity>> subscriber)
      {
        List<NoteEntity> list = new ArrayList<>();
        try
        {
          //FlowCursorList<NoteEntity> _list
          list = SQLite.select()
                      .from(NoteEntity.class)
                      .orderBy(NoteEntity_Table.timestamp.getNameAlias(), false)
                      .queryList();

          //list = _list.getAll();

          if (list != null)
          {
            subscriber.onNext(list);
            subscriber.onCompleted();
          }
          else
          {
            subscriber.onError(new NotesException());
          }
        }
        catch (Exception ex)
        {
          subscriber.onError(new NotesException(ex.getCause()));
        }

      }
    });
  }

  @Override
  public Observable<NoteEntity> noteEntity(int noteId)
  {

    return Observable.create(new Observable.OnSubscribe<NoteEntity>()
    {
      @Override
      public void call(Subscriber<? super NoteEntity> subscriber)
      {
        NoteEntity entity = null;
        try
        {
          //Condition.In in = Condition.column(NoteEntity_Table.noteId.getNameAlias()).in(noteId);
          Operator.In in = Operator.op(NoteEntity_Table.noteId.getNameAlias()).in(noteId);
          entity = SQLite.select()
                    .from(NoteEntity.class)
                    .where(in)
                    .querySingle();

          if (entity != null)
          {
            subscriber.onNext(entity);
            subscriber.onCompleted();
          }
          else
          {
            subscriber.onError(new NotesException());
          }
        }
        catch (Exception ex)
        {
          subscriber.onError(new NotesException(ex.getCause()));
        }
      }
    });
  }

  @Override
  public Observable<NoteEntity> noteInsert(final NoteEntity note)
  {

    return Observable.create(new Observable.OnSubscribe<NoteEntity>()
    {
      @Override
      public void call(Subscriber<? super NoteEntity> subscriber)
      {
        Boolean ret = new Boolean(false);
        NoteEntity _n = new NoteEntity(note);;
        try
        {
          FlowManager.getDatabase(NotesDatabase.class).executeTransaction(new ITransaction()
          {
            @Override
            public void execute(DatabaseWrapper databaseWrapper)
            {
              _n.save(databaseWrapper);
              //databaseWrapper

            }
          });
          subscriber.onNext(_n);
          subscriber.onCompleted();
        }
        catch (Exception ex)
        {
          subscriber.onError(new NotesException(ex.getCause()));
        }
      }
    });
  }

  @Override
  public Observable<Boolean> noteUpdate(NoteEntity note)
  {
    return Observable.create(new Observable.OnSubscribe<Boolean>()
    {
      @Override
      public void call(Subscriber<? super Boolean> subscriber)
      {
        Boolean ret = new Boolean(false);
        try
        {
          FlowManager.getDatabase(NotesDatabase.class).executeTransaction(new ITransaction()
          {
            @Override
            public void execute(DatabaseWrapper databaseWrapper)
            {
              note.update(databaseWrapper);

            }
          });
          ret = new Boolean(true);
          subscriber.onNext(ret);
          subscriber.onCompleted();
        }
        catch (Exception ex)
        {
          subscriber.onError(new NotesException(ex.getCause()));
        }
      }
    });
  }

  @Override
  public Observable<NoteEntity> noteEntityRemove(final NoteEntity note)
  {

    return Observable.create(new Observable.OnSubscribe<NoteEntity>()
    {
      @Override
      public void call(Subscriber<? super NoteEntity> subscriber)
      {

        NoteEntity _n = new NoteEntity(note);
        try
        {
          FlowManager.getDatabase(NotesDatabase.class).executeTransaction(new ITransaction()
          {
            @Override
            public void execute(DatabaseWrapper databaseWrapper)
            {
              boolean ret = _n.delete(databaseWrapper);
              if (ret)
                subscriber.onNext(note);
              else
                subscriber.onNext(null);
              subscriber.onCompleted();

            }
          });

        }
        catch (Exception ex)
        {
          subscriber.onError(new NotesException(ex.getCause()));
        }
      }
    });
  }

  @Override
  public List<Long> getEntitiesByParamsSync(long minTime)
  {
    List<Long> list = new ArrayList<>();
    try
    {
      //Condition cnd = Condition.column(NoteEntity_Table.timestamp.getNameAlias()).greaterThan(Long.valueOf(minTime));
      Operator cnd = Operator.op(NoteEntity_Table.timestamp.getNameAlias()).greaterThan(Long.valueOf(minTime));


      List<Double> cll = new ArrayList<>();
      cll.add(Double.valueOf(Parameters.defaultLat1));
      cll.add(Double.valueOf(Parameters.defaultLat2));
      cll.add(Double.valueOf(Parameters.defaultLon1));
      cll.add(Double.valueOf(Parameters.defaultLon2));
      Operator.In in1 = Operator.op(NoteEntity_Table.noteLocationLatitude.getNameAlias()).in(cll);
      Operator.In in2 = Operator.op(NoteEntity_Table.noteLocationLongitude.getNameAlias()).in(cll);

      //Condition.In in1 = Condition.column(NoteEntity_Table.noteLocationLatitude.getNameAlias()).in(cll);
      //Condition.In in2 = Condition.column(NoteEntity_Table.noteLocationLongitude.getNameAlias()).in(cll);
      List<NoteEntity> _list = SQLite.select()
              .from(NoteEntity.class)
              .where(cnd).and(in1).and(in2)
              .orderBy(NoteEntity_Table.timestamp.getNameAlias(), false)
              .queryList();

      if (_list != null && _list.size() > 0)
      {
        for (NoteEntity ne : _list)
        {
          if (ne != null)
            list.add(Long.valueOf(ne.getNoteId()));
        }
      }
      //list = _list.getAll();


    }
    catch (Exception ex)
    {
      Log.d("DBerr", ex.toString());
    }

    return list;
  }

  @Override
  public Boolean setEntitiesByValue(long id, double lat, double lon)
  {
    boolean retVal = false;

    try
    {

      Operator.In in = Operator.op(NoteEntity_Table.noteId.getNameAlias()).in(Long.valueOf(id));
      //Condition.In in = Condition.column(NoteEntity_Table.noteId.getNameAlias()).in(Long.valueOf(id));

      NoteEntity entity = SQLite.select()
              .from(NoteEntity.class)
              .where(in)
              .querySingle();
      if (entity == null)
        return false;

      entity.setNoteLocationLatitude(lat);
      entity.setNoteLocationLongitude(lon);

      FlowManager.getDatabase(NotesDatabase.class).executeTransaction(new ITransaction()
      {
        @Override
        public void execute(DatabaseWrapper databaseWrapper)
        {

          entity.update(databaseWrapper);

        }
      });
      retVal = true;
    }
    catch (Exception ex)
    {
      Log.d("DBImpl", ex.toString());
      retVal = false;
    }

    return Boolean.valueOf(retVal);
  }
}
