package com.msgeekay.notes.infrastructurelayer.location;

import android.location.Location;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public interface IMyLocationConsumer {
  /**
   * Call when a provider has a new location to consume. This can be called on any thread.
   */
  void onLocationChanged(Location location, IMyLocationProvider source);
}
