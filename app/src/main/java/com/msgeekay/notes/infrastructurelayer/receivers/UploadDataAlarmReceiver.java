package com.msgeekay.notes.infrastructurelayer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.msgeekay.notes.infrastructurelayer.DataUploader;

import timber.log.Timber;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class UploadDataAlarmReceiver extends BroadcastReceiver
{
  public static int RequestCode = 2354435;

  @Override
  public void onReceive(Context context, Intent intent)
  {
    try
    {
      Log.d(getClass().getName(), "onReceive");
      Intent runService = new Intent(context, DataUploader.class);
      context.startService(runService);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      Timber.e(e.getMessage());
      //CrashHelper.reportCrash(e);
    }
  }
}