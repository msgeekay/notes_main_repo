package com.msgeekay.notes.infrastructurelayer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.msgeekay.notes.R;
import com.msgeekay.notes.presentationlayer.Preferences;
import com.msgeekay.notes.presentationlayer.ui.main.MainActivity;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class NotificationHelper
{
  public static int NOTIFICATION_FOREGROUND = 110;

  public static int NOTIFICATION_TRACKING = 111;
  public static int NOTIFICATION_STOP_TRACKING = 112;
  public static int NOTIFICATION_LAST_ALARM = 113;
  public static boolean isNoRoute = true;

  //TODO: restore notifications
  /*
  public static RemoteViews getWidgetRemoteViews(double distance, int climb, Alarm alarm, Context context)
  {
    RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.lock_screen_app_widget);
    view.setViewVisibility(R.id.widget_toolbar, View.VISIBLE);
//    view.setTextViewText(R.id.top_toolbar_btn2, OsmMapFragment.getDistanceString(distance, context));
//    view.setTextViewText(R.id.top_toolbar_btn3, OsmMapFragment.getClimbString(climb, context));
    if (isNoRoute)
    {
      view.setTextViewText(R.id.widget_alarm_text, "");
      isNoRoute = false;
    }

    if (alarm != null && alarm.type != Alarm.Type.Noting)
    {
      view.setTextViewText(R.id.widget_alarm_text, AlarmsTextMapping.getTextByAlarm(alarm, context));
    }

    view.setOnClickPendingIntent(R.id.btnSosToolbar, LockScreenAppWidget.getSosButtonIntent(context));
    view.setOnClickPendingIntent(R.id.top_toolbar_arrow, LockScreenAppWidget.getAllWidgetClick(context));
    view.setOnClickPendingIntent(R.id.top_toolbar_btn2, LockScreenAppWidget.getAllWidgetClick(context));
    view.setOnClickPendingIntent(R.id.top_toolbar_btn3, LockScreenAppWidget.getAllWidgetClick(context));
    view.setOnClickPendingIntent(R.id.widget_alarm_text, LockScreenAppWidget.getAllWidgetClick(context));
    return view;
  }

  public static void showWidgetOnNotification(double distance, int climb, Alarm alarm, int notifyId, Context context)
  {
    RemoteViews widget = getWidgetRemoteViews(distance, climb, alarm, context);
    sendNotificationWithCustomView(widget, notifyId, context);
  }

  private static void sendNotificationWithCustomView(RemoteViews widget, int notifyId, Context context)
  {
    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    if (manager == null)
      return;

    NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_status2_24);
    builder.setContent(widget);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
      builder.setVisibility(Notification.VISIBILITY_PUBLIC);

    Notification notification = builder.build();
    notification.flags = Notification.FLAG_NO_CLEAR;
    manager.notify(notifyId, notification);
  }

  public static void sendUpdateToAppWidgetAndNotificationBar(double distance, int climb, Alarm alarm, Context context)
  {
    RemoteViews view = getWidgetRemoteViews(distance, climb, alarm, context);
    sendNotificationWithCustomView(view, NOTIFICATION_TRACKING, context);
    sendAlarmNotification(alarm, context);
    ComponentName thisWidget = new ComponentName(context, LockScreenAppWidget.class);
    AppWidgetManager manager = AppWidgetManager.getInstance(context);
    manager.updateAppWidget(thisWidget, view);
  }

  public static void sendAlarmNotification(Alarm alarm, Context context)
  {
    if (alarm == null || alarm.type == Alarm.Type.Noting)
    {
      return;
    }

    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    if (manager != null)
    {
      PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
      NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

      builder.setContentTitle(AlarmsTextMapping.getTextByAlarm(alarm, context))
              .setContentText("")
              .setSmallIcon(R.drawable.icon_hidden)
              .setContentIntent(contentIntent);
      if (Build.VERSION.SDK_INT >= 21)
        builder.setVisibility(Notification.VISIBILITY_PUBLIC);

      Notification notification = builder.build();
      notification.flags = Notification.FLAG_NO_CLEAR;
      manager.notify(NOTIFICATION_LAST_ALARM, notification);
    }
  }

  public static void sendNoRouteView(Context context)
  {
    RemoteViews view = getWidgetRemoteViews(0, 0, new Alarm(Alarm.Type.Noting), context);
    view.setViewVisibility(R.id.widget_toolbar, View.GONE);
    view.setTextViewText(R.id.widget_alarm_text, context.getString(R.string.no_route_text));

    isNoRoute = true;
    ComponentName thisWidget = new ComponentName(context, LockScreenAppWidget.class);
    AppWidgetManager manager = AppWidgetManager.getInstance(context);
    manager.updateAppWidget(thisWidget, view);
  }


  public static void checkNotification(Context context)
  {
    if (Preferences.getBoolean(Preferences.IS_TRACKING, context, false))
    {
      if (Preferences.getBoolean(Preferences.IS_NAVIGATING, context, false))
      {
        NotificationHelper.showWidgetOnNotification(0, 0, new Alarm(Alarm.Type.Noting), NOTIFICATION_TRACKING, context);
      }
      else
      {
        removeNotification(NOTIFICATION_LAST_ALARM, context);
        displayNotificationMessage(context.getString(R.string.service_text_start_tracking), NOTIFICATION_TRACKING, false, context, MainActivity.class);
        sendNoRouteView(context);
      }
    }
    else
    {
      removeNotification(NOTIFICATION_LAST_ALARM, context);
      removeNotification(NOTIFICATION_TRACKING, context);
    }
  }
*/

  public static void checkNotification(Context context)
  {
    if (Preferences.StaticAccess.getBoolean(Preferences.IS_TRACKING, context, false))
    {
      //do nothing
    }
    else
    {
      removeNotification(NOTIFICATION_LAST_ALARM, context);
      removeNotification(NOTIFICATION_TRACKING, context);
    }
  }

  public static void displayNotificationMessage(String message, int notifyId, boolean isCleanable,
                                                Context context, Class activityClassForPendingIntent)
  {
    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    if (manager != null)
    {
      PendingIntent contentIntent = PendingIntent.getActivity(context, 0, new Intent(context, activityClassForPendingIntent), 0);
      NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

      builder.setContentTitle(context.getString(R.string.service_title))
              .setContentText(message)
              .setSmallIcon(R.mipmap.ic_launcher_round)
              .setContentIntent(contentIntent);

      if (isCleanable)
        builder.setAutoCancel(true);

      Notification notification = builder.build();
      if (!isCleanable)
        notification.flags = Notification.FLAG_NO_CLEAR;
      manager.notify(notifyId, notification);
    }
  }

  public static void removeNotification(int id, Context context)
  {
    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    if (manager != null)
    {
      manager.cancel(id);
    }
  }

  public static Notification getDefaultNotification(Context context)
  {
    Intent notificationIntent = new Intent(context.getApplicationContext(), MainActivity.class);
    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    PendingIntent contentIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, notificationIntent, 0);

    return new NotificationCompat.Builder(context)
            .setContentTitle(context.getString(R.string.service_title))
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentIntent(contentIntent)
            .build();
  }

}

