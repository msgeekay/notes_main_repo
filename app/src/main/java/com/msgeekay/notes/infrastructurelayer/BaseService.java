package com.msgeekay.notes.infrastructurelayer;

import android.app.Service;

import com.msgeekay.notes.infrastructurelayer.location.IMyLocationConsumer;


/**
 * Created by grigoriykatz on 09/05/17.
 */

public abstract class BaseService extends Service implements IMyLocationConsumer, CheckStateCallback
{

//  /**
//   * Get the Main Service component for dependency injection.
//   *
//   * @return {@link ServiceComponent}
//   */
//  protected ServiceComponent getComponent() {
//    return ((MyApp)getApplication()).get().getServiceComponent();
//  }

}
