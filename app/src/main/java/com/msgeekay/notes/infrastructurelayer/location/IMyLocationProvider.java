package com.msgeekay.notes.infrastructurelayer.location;

import android.location.Location;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public interface IMyLocationProvider
{
  boolean startLocationProvider(IMyLocationConsumer myLocationConsumer);
  void stopLocationProvider();
  Location getLastKnownLocation();
}
