package com.msgeekay.notes.infrastructurelayer;

import android.app.NotificationManager;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;

import com.msgeekay.notes.datalayer.db.DBApi;
import com.msgeekay.notes.datalayer.db.DBApiImpl;
import com.msgeekay.notes.infrastructurelayer.location.GpsMyLocationProvider;
import com.msgeekay.notes.infrastructurelayer.location.IMyLocationProvider;
import com.msgeekay.notes.presentationlayer.Preferences;
import com.msgeekay.notes.presentationlayer.utils.LogUtils;


import timber.log.Timber;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class TrackingService extends BaseService
{

  private static final String TAG = "TrackingService";
  private static final long MINIMUM_UPDATE_TIME = 5 * 1000; // 5 secs in mills
  private static final float MINIMUM_UPDATE_DISTANCE = 5; // 5 meters

  public static final long CheckAlarmFrequencyTime = 5 * 1000; // 5 second

  public static final String INTENT_NEW_LOCATION =
          "com.msgeekay.notes.infrastructure.INTENT_NEW_LOCATION";

  public static final String EXTRA_LOCATION = "LOC";

  public static final String INTENT_LOCATION_WITH_DATA =
          "com.msgeekay.notes.infrastructure.INTENT_LOCATION_WITH_DATA";

  private NotificationManager mNotificationManager;
  private Scheduler scheduler;
  private GpsMyLocationProvider mProvider;

  private Location lastLocation;
  private boolean canWriteToBD = false;

  private boolean isTraking = false;
  private boolean isNavigating = false;
  private boolean isInitialized = false;
  final Messenger mMessenger = new Messenger(new IncomingHandler());

  private boolean isRunning;
  private boolean bound;
  private DataGPSManager dataGPSManager;

  public static final int MSG_START_TRACKING = 1;
  public static final int MSG_STOP_TRACKING = 2;
  public static final int MSG_STOP_SERVICE = 3;
  public static final int MSG_CALL_FOR_ROUTE = 13;
  public static final int MSG_GET_LOCATION = 14;
  public static final int MSG_COUNT_SUM_DATA = 15;
  public static final int MSG_CHECK_TO_STOP = 17;
  public static final int MSG_CHANGE_ALARM_TYPE = 18;

  public static final int MSG_SET_IN_APP_PREFERENCES = 19;
  public static final int MSG_SET_IN_APP_PREFERENCES_IN_NATIVELIB = 20;

  public static final int MSG_ACQUIRE_FOREGROUND = 21;
  public static final int MSG_DROP_FOREGROUND = 22;
  public static final int MSG_DOWNLOAD_SMTH = 23;
  public static final int MSG_PREPARE_UPLOAD_SMTH = 24;


  public static final int MSG_REQUEST_USER_POINTS = 1000;
  public static final int MSG_ADD_USER_POINT = 1001;
  public static final int MSG_EDIT_USER_POINT = 1002;
  public static final int MSG_REMOVE_USER_POINT = 1003;

  public static final String SVC_PARAM_TRACK_ID = "trackId";

  public static final String SVC_FOREGROUND_ALLOWED_SETTING_ID = "svcForegroundAllowedSettingId";

  private static final long ALARM_TICKER_DELAY = CheckAlarmFrequencyTime + 1000L;

  private Handler mAlarmTickerHandler = new Handler();
  private Runnable mAlarmTicker = new Runnable() {
    @Override
    public void run() {
      onAlarmTickerEvent();
    }
  };

  private void onAlarmTickerEvent()
  {

    Location location = lastLocation;
    handleNewLocation(location);

    //postAlarmTicker();
  }

  private void postAlarmTicker()
  {
    mAlarmTickerHandler.removeCallbacks(mAlarmTicker);
    mAlarmTickerHandler.postDelayed(mAlarmTicker, ALARM_TICKER_DELAY);
  }

  @Override
  public void onLocationChanged(Location location, IMyLocationProvider source)
  {
    handleNewLocation(location);
  }

  private void handleNewLocation(Location location)
  {
    if (canWriteToBD && location != null)
    {
      //addNewPointToActiveTrack(location);
    }
    lastLocation = location;

    postAlarmTicker();

    sendNewLocationBroadcast(location);
    updateAppWidgetAndNotificationBar(location);
    if (dataGPSManager != null)
      dataGPSManager.updateLocation(location);

  }

  private void updateAppWidgetAndNotificationBar(Location location)
  {
    //TODO: show or hide notification
  }

  private void sendNewLocationBroadcast(Location location)
  {
    sendBroadcast(new Intent(INTENT_NEW_LOCATION).putExtra(EXTRA_LOCATION, location));
    if (location == null)
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "Location = " + location);
    else
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(),
              "Location: lat = " + location.getLatitude() + ", lon = " + location.getLongitude());
  }

  public void checkServiceState()
  {
    if (!isRunning)
    {
      startService(new Intent(this, TrackingService.class));
      setAsForeground();
      isRunning = true;
    }
  }

  public void startTracking()
  {
    if (!isTraking)
    {
      isTraking = true;
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "start tracking");
    }

    continueTracking();
  }

  private void continueTracking()
  {
    if (isTraking)
    {
      //startNewTrackInDBIfNeeded();
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "continue tracking");
      if (!mProvider.startLocationProvider(this))
      {
        //TODO schedule restart of providers
      }
      //NotificationHelper.checkNotification(this); // TODO remove comment if want notification when tracking

      checkDataGPSManager();
    }
  }

  public void stopTracking()
  {
    if (isTraking)
    {
      mProvider.stopLocationProvider();
      // TODO Uncomment if want to show this notification on stop
      //displayNotificationMessage(getString(R.string.service_text_stop_tracking), NOTIFICATION_STOP_TRACKING, true);
      isTraking = false;
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "stop tracking");
    }
  }

  private void setAsForeground()
  {
    if (!Preferences.StaticAccess.getBoolean(SVC_FOREGROUND_ALLOWED_SETTING_ID, this, false)) return;

    LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "Foreground initialized");

    startForeground(NotificationHelper.NOTIFICATION_FOREGROUND,
            NotificationHelper.getDefaultNotification(this));
  }

  @Override
  public void onCheckStateCallback()
  {
    checkToStop();
  }

  private void checkDataGPSManager()
  {
    if (dataGPSManager == null)
      dataGPSManager = new DataGPSManager();

  }

  private void checkToStop()
  {

    if (!bound && canStop())
    {
      Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, this, false);
      stopService();
    }
  }

  private boolean canStop()
  {

    boolean retVal = false;

    long time = System.currentTimeMillis();
    long time0 = time - DataUploader.PERIOD_FOR_UPLOAD;

//    if (mDbHelper.getAllNotSendPointsCountFromTime(time) > 0)
//      retVal = false;
//    else
//      retVal = true;

    return retVal;
  }

  public void stopService()
  {
    isRunning = false;
    stopForeground(true);
    stopSelf();
  }

  @Override
  public void onDestroy()
  {
    stopTracking();


    Timber.d("service onDestroy");
    scheduler.stop();
    super.onDestroy();
    android.os.Process.killProcess(android.os.Process.myPid());
  }

  @Override
  public IBinder onBind(Intent intent)
  {
    bound = true;
    return mMessenger.getBinder();
  }

  @Override
  public boolean onUnbind(Intent intent)
  {
    bound = false;
    return super.onUnbind(intent);

    //return true;
  }

  @Override
  public void onRebind(Intent intent)
  {
    super.onRebind(intent);
    bound = true;
  }

  private void removeNotification(int id)
  {
    if (mNotificationManager != null)
    {
      mNotificationManager.cancel(id);
    }
  }

  class IncomingHandler extends Handler
  {
    @Override
    public void handleMessage(Message msg)
    {
      LogUtils.Timberlg(TrackingService.this.getApplicationInfo().processName,
              getClass().getName(), "service handleMessage " + String.valueOf(msg.what));
      switch (msg.what)
      {
        case MSG_REQUEST_USER_POINTS:
          //usecase to bd
          break;
        case MSG_ADD_USER_POINT:
          //usecase to bd
          break;
        case MSG_EDIT_USER_POINT:
          //usecase to bd
          break;
        case MSG_REMOVE_USER_POINT:
          //usecase to bd
          break;
        case MSG_START_TRACKING:
          checkServiceState();
          startTracking();
          break;
        case MSG_STOP_TRACKING:
          checkServiceState();
          stopTracking();
          break;
        case MSG_STOP_SERVICE:
          stopService();
          break;
        case MSG_DOWNLOAD_SMTH:
          checkServiceState();
          break;
        case MSG_PREPARE_UPLOAD_SMTH:
          checkServiceState();
          break;
        case MSG_CALL_FOR_ROUTE:
          checkServiceState();
          break;
        case MSG_GET_LOCATION:
          mProvider.startLocationProvider(TrackingService.this);
          sendNewLocationBroadcast(lastLocation);
          break;
        case MSG_COUNT_SUM_DATA:
          long trackId = msg.getData().getLong(SVC_PARAM_TRACK_ID);
          //collectSumData(trackId);
          break;
        case MSG_CHECK_TO_STOP:
          checkServiceState();
          checkToStop();
          break;
        case MSG_SET_IN_APP_PREFERENCES_IN_NATIVELIB:
          break;
        case MSG_SET_IN_APP_PREFERENCES:

          boolean param = msg.getData().getBoolean("");
          if (param)
          {
            //Preferences.StaticAccess.getBoolean("");
          }
          break;
        case MSG_ACQUIRE_FOREGROUND:
          setAsForeground();
          break;
        case MSG_DROP_FOREGROUND:
          stopForeground(true);
          break;
        default:
          super.handleMessage(msg);
      }
    }
  }


  @Override
  public void onCreate() {
    LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "service onCreate");
    super.onCreate();
    postAlarmTicker();

    if (!isInitialized)
    {
      LogUtils.Timberlg(this.getApplicationInfo().processName, getClass().getName(), "service onCreate initializing");

      scheduler = new Scheduler(this);

      mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      mProvider = new GpsMyLocationProvider(this);
      mProvider.setLocationUpdateMinTime(MINIMUM_UPDATE_TIME);
      mProvider.setLocationUpdateMinDistance(MINIMUM_UPDATE_DISTANCE);


//      mDbHelper = new TrackDBHelper(this);


      isInitialized = true;
      isTraking = Preferences.StaticAccess.getBoolean(Preferences.IS_TRACKING, this, false);


      //isCheckAlarms = true; // TODO now always true. think is this need  Preferences.getBoolean(SERVICE_FLAG_START_TRACK_ALARMS, this, false);
      //continueTracking(); Called by UI side when needed
    }

    stopForeground(true); // When the service restarts itself, let's just be invisible, UI side will request foreground and tracking if needed

  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    return super.onStartCommand(intent, flags, startId);
  }
}
