package com.msgeekay.notes.infrastructurelayer;

import android.location.Location;
import android.util.Log;

import com.msgeekay.notes.datalayer.db.DBApi;
import com.msgeekay.notes.datalayer.db.DBApiImpl;
import com.msgeekay.notes.presentationlayer.utils.Parameters;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

/**
 * Created by grigoriykatz on 19/09/17.
 *
 * Manager checks if DB GPS data has correct values (i.e. not equals to default)
 * Incorrect data will be changed by actual GPS values (once).
 */

public class DataGPSManager
{
  private static final String TAG = DataGPSManager.class.getName();

  private DBApi mDBApi;
  private final BlockingQueue<Long> entriesToUpdateDBQueue = new LinkedBlockingQueue<>();
  private HashSet<Long> updatdDBEntries = new HashSet<>();
  private long startTimeStamp;
  private long offsetToCheck = 3 * 60 * 60 * 1000;

  private static int countBDQueries = 0;

  private Subscription dbSubscription;
  private Subscription queueSubscription;

  private Location loc = null;

  public DataGPSManager()
  {
    mDBApi = new DBApiImpl();
    startTimeStamp = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
    scheduleDBCheck();
  }

  public void updateLocation(Location location)
  {
    this.loc = location;
  }

  public void addDBEntryToQueue(Long id)
  {
    if (!updatdDBEntries.contains(id))
    {
      entriesToUpdateDBQueue.add(id);
      if (queueSubscription == null || queueSubscription.isUnsubscribed())
      {
        scheduleQueueCheck();
      }
    }
  }

  public void scheduleDBCheck()
  {
    startTimeStamp = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();

    Action0 act = new Action0()
    {
      @Override
      public void call()
      {
        try
        {
          Log.d(TAG,"periodic db check for new entries...");


          try
          {
            long time = Calendar.getInstance(Locale.getDefault()).getTimeInMillis() - offsetToCheck;
            List<Long> list = mDBApi.getEntitiesByParamsSync(time);

            if (list != null && list.size() > 0)
            {
              for (Long l : list)
              {
                addDBEntryToQueue(l);
              }
              countBDQueries = 0;
            }
            else
              countBDQueries++;
          }
          catch (Exception ex)
          {
            if (Parameters.DEBUG)
              Log.d(TAG, ex.toString());
          }

          if (countBDQueries >= 5)
            dbSubscription.unsubscribe();

        }
        catch (Exception ex)
        {
          Log.d(getClass().getName(), ex.toString());
        }
      }
    };

    dbSubscription = Schedulers.newThread().createWorker().schedulePeriodically(act, 0, (long)5, TimeUnit.MINUTES);

  }

  public void scheduleQueueCheck()
  {
    Action0 act = new Action0()
    {
      @Override
      public void call()
      {
        try
        {
          Log.d(TAG,"periodic queue check for gps to update...");

          if (loc == null)
            return;

          try
          {
            if (entriesToUpdateDBQueue.size() == 0)
              queueSubscription.unsubscribe();

            Iterator<Long> it = entriesToUpdateDBQueue.iterator();

            while (it.hasNext())
            {
              Long l = it.next();
              Boolean result = mDBApi.setEntitiesByValue(l.longValue(), loc.getLatitude(), loc.getLongitude());
              if (result.booleanValue())
                it.remove();
            }



          }
          catch (Exception ex)
          {
            if (Parameters.DEBUG)
              Log.d(TAG, ex.toString());
          }


        }
        catch (Exception ex)
        {
          Log.d(getClass().getName(), ex.toString());
        }
      }
    };

    queueSubscription = Schedulers.newThread().createWorker().schedulePeriodically(act, 0, (long)5, TimeUnit.MINUTES);

  }

  public boolean isActive()
  {
    boolean retVal = true;
    if ((queueSubscription == null || (queueSubscription != null && queueSubscription.isUnsubscribed()))
      &&
            (dbSubscription == null || (dbSubscription != null && dbSubscription.isUnsubscribed()))
            )
      retVal = false;

    return retVal;
  }
}
