package com.msgeekay.notes.infrastructurelayer;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.os.AsyncTaskCompat;
import android.util.Log;

import com.msgeekay.notes.infrastructurelayer.receivers.ConnectionChangeReceiver;

/**
 * Created by grigoriykatz on 09/05/17.
 */

//TODO: refactor
public class StatusDownloader extends Service
{
  private static final String TAG = StatusDownloader.class.getSimpleName();

  public static final String INTENT_STATUS_WAS_UPDATED =
          "com.msgeekay.notes.STATUS_WAS_UPDATED";

  public static final String INTENT_STATUS_FINISHED =
          "com.msgeekay.notes.STATUS_FINISHED";

  public static final String EXTRA_SKI_AREA = "SKI_AREA";

  private volatile long jobsRunning = 0;
  // Only one downloading at a time !
  private long skiAreaId;

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    if (intent != null)
    {
      skiAreaId = intent.getLongExtra(EXTRA_SKI_AREA, -1);
      if (skiAreaId != -1)
        startStatusDownloading();
    }
    return super.onStartCommand(intent, flags, startId);
  }

  @Override
  public IBinder onBind(Intent intent)
  {
    return null;
  }

  private void startStatusDownloading()
  {
    synchronized (StatusDownloader.this)
    {
      // Check if service was stopped by self
      if (jobsRunning == -1)
        return;

      // Check if previous call was not finished yet
      if (jobsRunning++ != 0)
      {
        Log.w(StatusDownloader.class.getName(), "Another download process already running");
      }
    }

    AsyncTask<Void, Void, Void> upload = new AsyncTask<Void, Void, Void>()
    {

      @Override
      protected void onPreExecute()
      {
        //SnowballApplication.getContext().getAppState();
      }

      @Override
      protected Void doInBackground(Void... params)
      {
        if (!ConnectionChangeReceiver.isInternetConnectedSilent(getApplicationContext()))
        {
          Log.d(TAG, "no internet, stop status downloading");
          return null;
        }

        //statusDownloading();

        synchronized (StatusDownloader.this)
        {
          if (--jobsRunning <= 0)
          {
            jobsRunning = -1;
            Intent statusUpdatedIntent = new Intent(INTENT_STATUS_FINISHED);
            sendBroadcast(statusUpdatedIntent);
            StatusDownloader.this.stopSelf();

          }
        }

        return null;
      }
    };

    AsyncTaskCompat.executeParallel(upload);
  }

//  private void statusDownloading()
//  {
//    long topmostId = skiAreaId;
//    SkiAreaDB db = new SkiAreaDB(this);
//    try
//    {
//      SkiAreaUtils areaUtils = new SkiAreaUtils(db);
//      topmostId = areaUtils.getTopmostId(skiAreaId);
//    }
//    finally
//    {
//      db.close();
//    }
//
//    SoapService service = new SoapService(null, Parameters.serviceUrl);
//    Credentials credentials = new Credentials();
//    credentials.login = Parameters.systemLogin;
//    credentials.password = Parameters.systemPassword;
//
//    if (Parameters.DEBUG) Log.d(TAG, "Trying to retrieve layer data statuses for areaId " + topmostId + " ...");
//
//    List<LayerStatus> statuses = service.getLayerDataStatus(
//            credentials, topmostId);
//
//    int closedCount = 0;
//
//    if (statuses != null)
//    {
//      for (LayerStatus layerStatus: statuses)
//      {
//        if (layerStatus.status == 0)
//        {
//          closedCount++;
//        }
//      }
//    }
//
//    if (BuildConfig.DEBUG)
//    {
//      Log.d(TAG, "Got layer data statuses. " + (statuses == null ?
//              ("Statuses are null.") : (statuses.size() + " entries. " + closedCount + " objects are closed.")));
//    }
//
//    db = new SkiAreaDB(this);
//    try
//    {
//      if (statuses != null)
//      {
//        db.updateLayerStatus(statuses);
//        Navigator.pushLayerDatasStatus2Navlib(statuses);
//        sendBroadcast(new Intent(INTENT_STATUS_WAS_UPDATED));
//      }
//
//    }
//    finally
//    {
//      db.close();
//    }
//
//  }

}
