package com.msgeekay.notes.infrastructurelayer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class ConnectionChangeReceiver extends BroadcastReceiver
{
  private List<Callback> mCallbacks = new ArrayList<Callback>();
  private List<Callback> toRemove = new ArrayList<Callback>();

  public enum State
  {
    CONNECTED, NOT_CONNECTED
  }

  public interface Callback
  {
    void onConnectionChange(State state);

    boolean removeAfterFire();
  }

  public void addCallback(Callback callback)
  {
    mCallbacks.add(callback);
  }

  public void removeCallback(Callback callback)
  {
    mCallbacks.remove(callback);
  }

  @Override
  public void onReceive(Context context, Intent intent)
  {
    for (Callback callback : mCallbacks)
    {
      callback.onConnectionChange(isInternetConnectedSilent(context) ? State.CONNECTED : State.NOT_CONNECTED);
      if (callback.removeAfterFire())
        toRemove.add(callback);
    }

    mCallbacks.removeAll(toRemove);
    toRemove.clear();
  }

  public static boolean isInternetConnectedSilent(Context context)
  {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    if (cm == null)
    {
      return false;
    }

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork != null && activeNetwork.isConnectedOrConnecting())
    {
      return true;
    }

    return false;
  }
}
