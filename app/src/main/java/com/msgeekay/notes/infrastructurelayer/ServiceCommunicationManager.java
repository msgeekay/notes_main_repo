package com.msgeekay.notes.infrastructurelayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.msgeekay.notes.presentationlayer.Preferences;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

/**
 * Created by grigoriykatz on 16/05/17.
 */
@Singleton
public class ServiceCommunicationManager
{

  @Inject
  Preferences prefs;
  private Messenger mService = null;
  private boolean isBoundToService = false;
  private Context mContext;
  private List<Message> msgQueue = new ArrayList<Message>();

  public ServiceCommunicationManager(Context context)
  {
    mContext = context;
  }

  protected void finalize() throws Throwable
  {
    try
    {
      unbindService();
    }
    finally
    {
      super.finalize();
    }
  }

  /**
   * Defines callbacks for service binding, passed to bindService()
   */
  private ServiceConnection mConnection = new ServiceConnection()
  {

    @Override
    public void onServiceConnected(ComponentName className, IBinder service)
    {
      synchronized (this)
      {
        Log.d(getClass().getName(), "onServiceConnected");
        mService = new Messenger(service);
        isBoundToService = true;
        List<Message> toRemove = new ArrayList<Message>();
        for (Message msq : msgQueue)
        {
          if (sendMsgToService(msq.what, msq.getData()))
            toRemove.add(msq);
        }

        msgQueue.removeAll(toRemove);
      }
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0)
    {
      Timber.w(getClass().getName(), "onServiceDisconnected");
      mService = null;
      isBoundToService = false;
    }
  };

  public void bindService()
  {
    Intent intent = new Intent(mContext, TrackingService.class);
    mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
  }

  public boolean sendMsgToService(int msgID, Bundle data)
  {
    synchronized (this)
    {
      // Create and send a message to the service, using a supported 'what' value
      Message msg = Message.obtain(null, msgID, 0, 0);
      if (data != null)
        msg.setData(data);

      if (!isBoundToService || mService == null)
      {
        msgQueue.add(msg);
        Timber.w(getClass().getName(), "fail sendMsgToService. add to queue " + String.valueOf(msg.what));
        return false;
      }

      try
      {
        mService.send(msg);
        Timber.w(getClass().getName(), "sendMsgToService " + String.valueOf(msg.what));
        return true;
      }
      catch (RemoteException e)
      {
        Timber.w(getClass().getName(), e.toString());
        //CrashHelper.reportCrash(e);
      }

      return false;
    }
  }

  public void startTracking()
  {
    sendMsgToService(TrackingService.MSG_START_TRACKING, null);
    Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, true);
  }

  public void stopTracking()
  {
    sendMsgToService(TrackingService.MSG_STOP_TRACKING, null);
    Preferences.StaticAccess.setBoolean(Preferences.IS_TRACKING, mContext, false);
  }

  public void checkToStop()
  {
    sendMsgToService(TrackingService.MSG_CHECK_TO_STOP, null);
  }

  private void stopService()
  {
    sendMsgToService(TrackingService.MSG_STOP_SERVICE, null);
    unbindService();
  }

  public void unbindService()
  {
    if (isBoundToService)
    {
      mContext.unbindService(mConnection);
      isBoundToService = false;
      Timber.w(getClass().getName(), "unbindService");
    }
  }
}