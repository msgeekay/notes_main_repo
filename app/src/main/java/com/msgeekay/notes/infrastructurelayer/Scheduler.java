package com.msgeekay.notes.infrastructurelayer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.msgeekay.notes.infrastructurelayer.receivers.ConnectionChangeReceiver;
import com.msgeekay.notes.infrastructurelayer.receivers.UploadDataAlarmReceiver;
import com.msgeekay.notes.presentationlayer.utils.LogUtils;

import timber.log.Timber;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Scheduler instance located in TrackerService.
 * It works with events which should not be processed via AlarmManager
 */

public class Scheduler
{
  private Context ctx;
  private Handler repeater;
  private ConnectionChangeReceiver mReceiver;
  private boolean isUploadSheduled = false;

  private int status_check_count = 5;
  private final static long PERIOD_STATUS_CHECK = 10 * 1000; // 10 sec in msecs
  //  private final static long PERIOD_STATUS_UPDATING = 60 * 60 * 1000; // 1 hour in msecs
  private final static long PERIOD_STATUS_UPDATING = 5 * 60 * 1000; // 5min

  private Long lastSkiAreaId;
  private Long lastSkiAreaTimestamp;

  public Scheduler(Context ctx)
  {
    this.ctx = ctx;
    this.repeater = new RepeaterHandler(ctx);
    ctx.registerReceiver(receiver, new IntentFilter(RepeaterHandler.INTENT_REPEAT_STATUS_DOWNLOAD));
    ctx.registerReceiver(receiver, new IntentFilter(RepeaterHandler.INTENT_REPEAT_CHECK_SRVC_ACTIVITY));

    mReceiver = new ConnectionChangeReceiver();
    registerNetworkUpdate(mReceiver);

    scheduleUploadService();
    addCallbackToOnConnectionChanged(new ConnectionChangeReceiver.Callback()
    {
      @Override
      public void onConnectionChange(ConnectionChangeReceiver.State state)
      {
        Log.d(getClass().getName(), "onConnectionChange " + state);
        if (state == ConnectionChangeReceiver.State.CONNECTED)
        {
          scheduleUploadService();
        }
        else
        {
          removeUploadFromSchedule();
        }
      }

      @Override
      public boolean removeAfterFire()
      {
        return false;
      }
    });
    repeatStatusCheck();
  }

  public void stop()
  {
    ctx.unregisterReceiver(receiver);
    repeater.removeMessages(RepeaterHandler.ACTION_REPEAT_STATUS_DOWNLOAD);
  }

  private void registerNetworkUpdate(BroadcastReceiver receiver)
  {
    final IntentFilter filter = new IntentFilter();
    filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
    ctx.registerReceiver(receiver, filter);
  }

  public void addCallbackToOnConnectionChanged(ConnectionChangeReceiver.Callback callback)
  {
    if (mReceiver != null)
    {
      mReceiver.addCallback(callback);
    }
  }

  public void removeCallbackFromOnConnectionChanged(ConnectionChangeReceiver.Callback callback)
  {
    if (mReceiver != null)
    {
      mReceiver.removeCallback(callback);
    }
  }

  public boolean getIsUploadScheduled() {
    return isUploadSheduled;
  }

  public void scheduleUploadService()
  {
    if (isUploadSheduled)
    {
      LogUtils.Timberlg(ctx.getApplicationInfo().processName, getClass().getName(), "UploadService already scheduled");
      Log.d(getClass().getName(), "UploadService already scheduled");
      return;
    }

    removeUploadFromSchedule();
    Log.d(getClass().getName(), "scheduleUploadService");
    Intent intent = new Intent(ctx, UploadDataAlarmReceiver.class);
    AlarmManager alarmMgr = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
    PendingIntent pending = PendingIntent.getBroadcast(ctx, UploadDataAlarmReceiver.RequestCode, intent, 0);
    alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5,
            DataUploader.PERIOD_FOR_UPLOAD, pending);
    isUploadSheduled = true;
    status_check_count = 5;
  }

  private void removeUploadFromSchedule()
  {
    Log.d(getClass().getName(), "removeUploadFromSchedule");
    AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
    Intent intent = new Intent(ctx, UploadDataAlarmReceiver.class);
    PendingIntent pendingUpdateIntent = PendingIntent.getBroadcast(ctx, UploadDataAlarmReceiver.RequestCode, intent,
            PendingIntent.FLAG_CANCEL_CURRENT);

    try
    {
      alarmManager.cancel(pendingUpdateIntent);
      isUploadSheduled = false;
      status_check_count = 5;
    }
    catch (Exception e)
    {
      Timber.e(e.getMessage());
      //CrashHelper.reportCrash(e);
    }
  }

  private void onSkiAreaChanged(Long skiAreaId)
  {
    lastSkiAreaId = skiAreaId;
    lastSkiAreaTimestamp = System.currentTimeMillis();
    if (skiAreaId != null)
    {
      runStatusDownloader(skiAreaId);
      repeater.sendEmptyMessageDelayed(
              RepeaterHandler.ACTION_REPEAT_STATUS_DOWNLOAD, PERIOD_STATUS_UPDATING);
    }
  }

  private void repeatStatusCheck() {
    repeater.sendEmptyMessageDelayed(
            RepeaterHandler.ACTION_REPEAT_CHECK_SRVC_ACTIVITY, PERIOD_STATUS_CHECK);
  }

  private void runStatusDownloader(long skiAreaId)
  {
    Intent serviceIntent = new Intent(ctx, StatusDownloader.class);
    serviceIntent.putExtra(StatusDownloader.EXTRA_SKI_AREA, skiAreaId);
    ctx.startService(serviceIntent);
  }

  private BroadcastReceiver receiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      if (intent.getAction().equals(RepeaterHandler.INTENT_REPEAT_STATUS_DOWNLOAD))
      {
        if ((lastSkiAreaId != null) &&
                (System.currentTimeMillis() >= lastSkiAreaTimestamp + PERIOD_STATUS_UPDATING))
        {
          onSkiAreaChanged(lastSkiAreaId);
        }
      }
      else if (intent.getAction().equals(RepeaterHandler.INTENT_REPEAT_CHECK_SRVC_ACTIVITY))
      {
        if (status_check_count-- < 0) {

          if (ctx instanceof CheckStateCallback) {
            CheckStateCallback csck = (CheckStateCallback) ctx;
            csck.onCheckStateCallback();
          }
        }

        repeatStatusCheck();
      }
    }
  };

  private static class RepeaterHandler extends Handler
  {
    public static final int ACTION_REPEAT_STATUS_DOWNLOAD = 0;
    public static final int ACTION_REPEAT_CHECK_SRVC_ACTIVITY = 1;
    public static final String INTENT_REPEAT_STATUS_DOWNLOAD =
            "com.msgeekay.notes.SCHEDULER_REPEAT_STATUS_DOWNLOAD";

    public static final String INTENT_REPEAT_CHECK_SRVC_ACTIVITY =
            "com.msgeekay.notes.SCHEDULER_REPEAT_CHECK_SRVC_ACTIVITY";
    private Context context;

    public RepeaterHandler(Context context)
    {
      this.context = context;
    }

    @Override
    public void handleMessage(Message msg)
    {
      if (msg.what == ACTION_REPEAT_STATUS_DOWNLOAD)
      {
        Intent repeatStatusDownload = new Intent(INTENT_REPEAT_STATUS_DOWNLOAD);
        context.sendBroadcast(repeatStatusDownload);

      } else if (msg.what == ACTION_REPEAT_CHECK_SRVC_ACTIVITY)
      {
        Intent repeatStatusCheckSrvc = new Intent(INTENT_REPEAT_CHECK_SRVC_ACTIVITY);
        context.sendBroadcast(repeatStatusCheckSrvc);
      }
    }

  }
}
