package com.msgeekay.notes.infrastructurelayer;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.os.AsyncTaskCompat;
import android.util.Log;

import com.msgeekay.notes.infrastructurelayer.receivers.ConnectionChangeReceiver;
import com.msgeekay.notes.model.UserPoint;
import com.msgeekay.notes.presentationlayer.utils.GPXUtils;
import com.msgeekay.notes.presentationlayer.utils.TimeUtils;

import java.util.List;

/**
 * Created by grigoriykatz on 09/05/17.
 */

public class DataUploader extends Service
{
  //private TrackDBHelper mDbHelper;
  private boolean isInitialized = false;
  private volatile long jobsRunning = 0;
  public static int PERIOD_FOR_UPLOAD = 5 * 60 * 1000; //5 minutes in milliseconds

  @Override
  public void onCreate()
  {
    super.onCreate();
    Log.d(getClass().getName(), "onCreate");
    if (!isInitialized)
    {
      //mDbHelper = new TrackDBHelper(this);
      isInitialized = true;
    }
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    Log.d(getClass().getName(), "onStartCommand");
    startUpload();
    return super.onStartCommand(intent, flags, startId);
  }

  @Override
  public void onDestroy()
  {
//    if (mDbHelper != null)
//    {
//      //mDbHelper.close();
//    }

    super.onDestroy();
  }


  @Override
  public IBinder onBind(Intent intent)
  {
    return null;
  }

  private void startUpload()
  {
    synchronized(DataUploader.this)
    {
      // Check if service was stopped by self
      if (jobsRunning == -1)
        return;

      if (jobsRunning != 0)
      {
        Log.d(DataUploader.class.getName(), "Wrong usage: simultaneous startUpload() calls were detected");
        return;
      }

      jobsRunning++;
    }

    //TODO: refactor
    final boolean needToSendFB = true; //Preferences.needToSendFBUserInfo(getApplicationContext());
    final String fbInfo = ""; //needToSendFB ? Preferences.getFBUserInfo(getApplicationContext()) : "";

    AsyncTask<Void, Void, Void> upload = new AsyncTask<Void, Void, Void>()
    {

      @Override
      protected Void doInBackground(Void... params)
      {
        Log.d(DataUploader.this.getClass().getName(), "startUpload");
        if (!ConnectionChangeReceiver.isInternetConnectedSilent(getApplicationContext()))
        {
          Log.d(getClass().getName(), "no internet, exit from startUpload");
          return null;
        }

        if (needToSendFB)
        {
          if (sendFBInfoToServer(fbInfo))
          {
            //TODO: refactor
            //Preferences.setNeedToSendFBUserInfo(getApplicationContext(), false);
          }
        }
        else
        {

          long time = System.currentTimeMillis();
          //TODO: refactor
          //startUploadFrom(time, Preferences.getLong(Preferences.ALL_POINTS_SEND_TILL, getApplicationContext(), 0));
        }

        synchronized (DataUploader.this)
        {
          if (--jobsRunning <= 0)
          {
            jobsRunning = -1;
            DataUploader.this.stopSelf();
          }
        }

        return null;
      }
    };

    AsyncTaskCompat.executeParallel(upload);
  }

  //track data uploading
  public void startUploadFrom(long time, long till)
  {
    Log.d(getClass().getName(), "startUploadFrom " + TimeUtils.convertToLogs(time) + " till " + TimeUtils.convertToLogs(till));
    List<UserPoint> pointsToSend;
    long period = PERIOD_FOR_UPLOAD;
    if (time - till < PERIOD_FOR_UPLOAD)
    {
      period = time - till;
    }

    long firstTime = time;
    while (true)
    {
      Log.d(getClass().getName(), "startUploadFrom time: " + TimeUtils.convertToLogs(time));
      //TODO: refactor
      pointsToSend = null; //mDbHelper.getNotSendPointsInTimeRange(time, period);
      Log.d(getClass().getName(), "in startUploadFrom getNotSendPointsInTimeRange from " + TimeUtils.convertToLogs(time) + " in range " + String.valueOf(period)
              + " returned " + String.valueOf(pointsToSend.size()) + " points");

      if (pointsToSend.size() == 0)
      {
        //TODO: refactor
        UserPoint point = new UserPoint(); //mDbHelper.getNearestNotSentPoint(time);
        if (point != null)
        {
          time = point.getUserPointTimestamp();
          Log.d(getClass().getName(), "startUploadFrom getNearestNotSentPoint: " + TimeUtils.convertToLogs(time));
          continue;
        }
        else
        {
          //Preferences.setLong(Preferences.ALL_POINTS_SEND_TILL, firstTime, getApplicationContext());
          Log.d(getClass().getName(), "startUploadFrom getNearestNotSentPoint(time) point == null  set ALL_POINTS_SEND_TILL " + TimeUtils.convertToLogs(firstTime));
          break;
        }
      }
      else
      {
        if (!sendPointsToServer(pointsToSend))
        {
          break;
        }
      }

      if (time < till)
      {
        //Preferences.setLong(Preferences.ALL_POINTS_SEND_TILL, firstTime, getApplicationContext());
        Log.d(getClass().getName(), "startUploadFrom time < till set ALL_POINTS_SEND_TILL " + TimeUtils.convertToLogs(firstTime));
        break;
      }

      if (!ConnectionChangeReceiver.isInternetConnectedSilent(getApplicationContext()))
      {
        return;
      }

      time -= period;
    }
  }

  private boolean sendPointsToServer(List<UserPoint> points)
  {
    Log.d(getClass().getName(), "sendPointsToServer point count " + String.valueOf(points.size()));

    int result = sendToServer("gpx", GPXUtils.ExportToString(points));
    if (result != -1)
    {
      //mDbHelper.setItemsSend(points);
      Log.d(getClass().getName(), "setItemsSend from  "
              + TimeUtils.convertToLogs(points.get(0).getUserPointTimestamp())
              + " till "
              + TimeUtils.convertToLogs(points.get(points.size() - 1).getUserPointTimestamp()));
      Log.d(getClass().getName(), "sendPointsToServer succesfully send to server "
              + String.valueOf(points.size())
              + " points");
      return true;
    }

    Log.d(getClass().getName(), "sendPointsToServer return " + String.valueOf(result));
    return false;
  }

  private boolean sendFBInfoToServer(String fbInfo)
  {
    Log.d(getClass().getName(), "sendFBInfoToServer info = " + fbInfo);

    int result = sendToServer("fbp", fbInfo);
    if (result != -1)
    {
      Log.d(getClass().getName(), "sendFBInfoToServer completed successgully");
      return true;
    }

    Log.d(getClass().getName(), "sendFBInfoToServer return " + String.valueOf(result));

    return false;

  }

  //TODO: implement via UseCases
  private int sendToServer(String format, String data)
  {
//    SoapService service = new SoapService(null, Parameters.serviceUrl);
//    Credentials credentials = new Credentials();
//    credentials.login = Parameters.systemLogin;
//    credentials.password = Parameters.systemPassword;
//    return service.saveUserData(credentials, SnowballApplication.getDeviceID(), format, data);
    return 1;

  }


}
