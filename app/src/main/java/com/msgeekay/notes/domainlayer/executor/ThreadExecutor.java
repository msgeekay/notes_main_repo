package com.msgeekay.notes.domainlayer.executor;

import java.util.concurrent.Executor;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Executor implementation can be based on different frameworks or techniques of asynchronous
 * execution, but every implementation will execute the
 * {@link UseCase} out of the UI thread.
 */
public interface ThreadExecutor extends Executor
{}

