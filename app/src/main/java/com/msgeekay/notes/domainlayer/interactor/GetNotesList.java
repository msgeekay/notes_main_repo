package com.msgeekay.notes.domainlayer.interactor;

import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.domainlayer.repo.Repository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class GetNotesList extends UseCase<Void>
{
  private Repository repo;

  @Inject
  public GetNotesList(Repository repo, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.repo = repo;
  }
  @Override
  protected Observable buildUseCaseObservable(Void param)
  {
    return repo.notes();
  }
}
