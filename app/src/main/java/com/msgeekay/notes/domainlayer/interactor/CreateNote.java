package com.msgeekay.notes.domainlayer.interactor;

import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.domainlayer.repo.Repository;
import com.msgeekay.notes.model.Note;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class CreateNote extends UseCase<Note>
{
  private Repository repo;

  @Inject
  public CreateNote(Repository repo, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.repo = repo;
  }

  @Override
  protected Observable buildUseCaseObservable(Note note)
  {
    return repo.noteInsert(note);
  }
}
