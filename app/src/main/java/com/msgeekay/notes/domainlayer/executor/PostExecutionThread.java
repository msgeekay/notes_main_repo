package com.msgeekay.notes.domainlayer.executor;

import rx.Scheduler;

/**
 * Created by grigoriykatz on 09/05/17.
 *
 * Thread abstraction created to change the execution context from any thread to any other thread.
 * Useful to encapsulate a UI Thread for example, since some job will be done in background, an
 * implementation of this interface will change context and update the UI.
 */
public interface PostExecutionThread {
  Scheduler getScheduler();
}
