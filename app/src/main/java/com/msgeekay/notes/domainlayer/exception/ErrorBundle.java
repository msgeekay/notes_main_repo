package com.msgeekay.notes.domainlayer.exception;

/**
 * Created by grigoriykatz on 15/05/17.
 *
 * Interface to represent a wrapper around an {@link java.lang.Exception} to manage errors.
 */
public interface ErrorBundle {
  Exception getException();

  String getErrorMessage();
}
