package com.msgeekay.notes.domainlayer.interactor;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.presentationlayer.ui.main.map.custom.OnMapAndLayoutReady;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 18/09/17.
 */
@Deprecated
public class GetGoogleMapFullyReady extends UseCase<SupportMapFragment>
{
  @Inject
  public GetGoogleMapFullyReady(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
  }

  @Override
  protected Observable buildUseCaseObservable(SupportMapFragment map)
  {
    return OnMapAndLayoutReady.onMapAndLayoutReadyObservable(map);
  }
}
