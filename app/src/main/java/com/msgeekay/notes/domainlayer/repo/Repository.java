package com.msgeekay.notes.domainlayer.repo;

import android.net.Uri;

import com.msgeekay.notes.model.Note;

import java.net.URI;
import java.util.List;

import rx.Observable;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public interface Repository
{
  Observable<List<Note>> notes();
  Observable<Note> note(final int noteId);

  Observable<Note> noteInsert(final Note note);
  Observable<Boolean> noteUpdate(final Note note);
  Observable<Note> noteRemove(final Note note);

  Observable<Uri> noteExport(final Note note);

}
