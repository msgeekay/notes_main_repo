package com.msgeekay.notes.domainlayer.interactor;

import com.msgeekay.notes.domainlayer.executor.PostExecutionThread;
import com.msgeekay.notes.domainlayer.executor.ThreadExecutor;
import com.msgeekay.notes.domainlayer.repo.Repository;
import com.msgeekay.notes.model.Note;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by grigoriykatz on 19/09/17.
 */

public class ExportNote extends UseCase<Note>
{
  private Repository repo;

  @Inject
  public ExportNote(Repository repo, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread)
  {
    super(threadExecutor, postExecutionThread);
    this.repo = repo;
  }

  @Override
  protected Observable buildUseCaseObservable(Note param)
  {
    return repo.noteExport(param);
  }
}
