package com.msgeekay.notes.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by grigoriykatz on 18/09/17.
 */

public class Bounds
{
  @SerializedName("northeast") Northeast northeast;
  @SerializedName("southwest") Southwest southwest;

  public LatLng getSouthwestLatLng() {
    return new LatLng(Double.parseDouble(southwest.getLat()), Double.parseDouble(southwest.getLng()));
  }

  public LatLng getNortheastLatLng() {
    return new LatLng(Double.parseDouble(northeast.getLat()), Double.parseDouble(northeast.getLng()));
  }

  public Southwest getSouthwest() {
    return southwest;
  }

  public Northeast getNortheast() {
    return northeast;
  }

  public class Northeast {

    @SerializedName("lat") String lat;
    @SerializedName("lng") String lng;

    public String getLat() {
      return lat;
    }

    public Double getLatD() {
      return Double.parseDouble(lat);
    }

    public String getLng() {
      return lng;
    }
  }

  public class Southwest {

    @SerializedName("lat") String lat;
    @SerializedName("lng") String lng;

    public String getLat() {
      return lat;
    }

    public String getLng() {
      return lng;
    }

    public Double getLatD() {
      return Double.parseDouble(lat);
    }

    public void setLat(final String lat) {
      this.lat = lat;
    }
  }
}
