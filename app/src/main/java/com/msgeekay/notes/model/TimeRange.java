package com.msgeekay.notes.model;

import java.util.Locale;

/**
 * Created by grigoriykatz on 11/05/17.
 *
 * TimeRange - TimeRangeId
 * 00:00-01:00 - #0
 * 01:00-02:00 - #1
 * 02:00-03:00 - #2
 * 03:00-04:00 - #3
 * 04:00-05:00 - #4
 * 05:00-06:00 - #5
 * 06:00-07:00 - #6
 * ...
 * 21:00-22:00 - #21
 * 22:00-23:00 - #22
 * 23:00-24:00 - #23
 *
 */

public class TimeRange
{
  /**
   *
   * @param dayTime - time of day in milliseconds
   * @return timeRangeId
   */
  public static long getTimeRangeIdByDayTime(long dayTime)
  {
    long range = -1;

    range = (int)((float)dayTime/3600000);

    return range;
  }

  public static String getTimeRangeAsStringByTimeRangeId(long rangeId) throws Exception
  {
    if (rangeId > 23 || rangeId < 0)
      throw new Exception("Unsupported TimeRangeId !!!");

    long endOfPeriod = rangeId + 1;

    return String.format(Locale.getDefault(), "%d%n:00 - %d%n:00", rangeId, endOfPeriod);

  }
}

