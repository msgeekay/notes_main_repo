package com.msgeekay.notes.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public class UserPoint extends BaseEntitySynchronizer
{
  private long userPointId;
  private long userPointUserId;
  private double userPointLongitude;
  private double userPointLatitude;
  private long userPointTimestamp;


  public double getUserPointLongitude()
  {
    return userPointLongitude;
  }

  public double getUserPointLatitude()
  {
    return userPointLatitude;
  }

  public long getUserPointTimestamp()
  {
    return userPointTimestamp;
  }

  public void setUserPointLongitude(double userPointLongitude)
  {
    this.userPointLongitude = userPointLongitude;
  }

  public void setUserPointLatitude(double userPointLatitude)
  {
    this.userPointLatitude = userPointLatitude;
  }

  public void setUserPointTimestamp(long userPointTimestamp)
  {
    this.userPointTimestamp = userPointTimestamp;
  }
}
