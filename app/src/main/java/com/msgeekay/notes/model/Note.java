package com.msgeekay.notes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.msgeekay.notes.presentationlayer.utils.Parameters;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class Note implements Parcelable
{
  private long noteId;

  private String noteTitle;
  private String noteBody;

  private NotePriority priority = NotePriority.none;

  private String imageURI;

  private LatLng noteLocation;
  private String noteLocationName;

  private long timestamp;
  private long timestampUpdate;

  public Note()
  {
    this.noteId = -1;
    this.noteTitle = "";
    this.noteBody = "";
    this.priority = NotePriority.none;
    this.imageURI = "";
    this.noteLocation = new LatLng(Parameters.defaultLat1, Parameters.defaultLon1);
    this.noteLocationName = "";
    this.timestamp = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
    this.timestampUpdate = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
  }

  public Note(Note note)
  {
    this.noteId = note.getNoteId();
    this.noteTitle = note.getNoteTitle();
    this.noteBody = note.getNoteBody();
    this.priority = note.getPriority();
    this.imageURI = note.getImageURI();
    this.noteLocation = note.getNoteLocation();
    this.noteLocationName = note.getNoteLocationName();
    this.timestamp = note.getTimestamp();
    this.timestampUpdate = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
  }

  public Note(long id, String title, String body, int priority, String imageURI,
              double noteLocationLatitude, double noteLocationLongitude, String noteLocationName,
              long timestamp)
  {
    this.noteId = id;
    this.noteTitle = title;
    this.noteBody = body;
    this.priority = NotePriority.getByPriority(priority);
    this.imageURI = imageURI;
    this.noteLocation = (noteLocationLatitude != 0 && noteLocationLongitude != 0)
                        ? new LatLng(noteLocationLatitude, noteLocationLongitude)
                        : null;
    this.noteLocationName = noteLocationName;
    this.timestamp = timestamp;
    this.timestampUpdate = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
  }

  public long getNoteId()
  {
    return noteId;
  }

  public void setNoteId(long noteId)
  {
    this.noteId = noteId;
  }

  public String getNoteTitle()
  {
    return noteTitle;
  }

  public void setNoteTitle(String noteTitle)
  {
    this.noteTitle = noteTitle;
  }

  public String getNoteBody()
  {
    return noteBody;
  }

  public void setNoteBody(String noteBody)
  {
    this.noteBody = noteBody;
  }

  public String getImageURI()
  {
    return imageURI;
  }

  public void setImageURI(String imageURI)
  {
    this.imageURI = imageURI;
  }

  public NotePriority getPriority()
  {
    return priority;
  }

  public void setPriority(NotePriority priority)
  {
    this.priority = priority;
  }

  public LatLng getNoteLocation()
  {
    return noteLocation;
  }

  public void setNoteLocation(LatLng noteLocation)
  {
    this.noteLocation = noteLocation;
  }

  public String getNoteLocationName()
  {
    return noteLocationName;
  }

  public void setNoteLocationName(String noteLocationName)
  {
    this.noteLocationName = noteLocationName;
  }

  public long getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(long timestamp)
  {
    this.timestamp = timestamp;
  }

  public long getTimestampUpdate()
  {
    return timestampUpdate;
  }

  public void setTimestampUpdate(long timestampUpdate)
  {
    this.timestampUpdate = timestampUpdate;
  }

  @Override
  public int describeContents()
  {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags)
  {
    dest.writeLong(noteId);
    dest.writeString(noteTitle);
    dest.writeString(noteBody);
    dest.writeString(imageURI);
    dest.writeInt(priority.getId());
    if (noteLocation == null)
    {
      //lat
      dest.writeDouble(0);
      //lon
      dest.writeDouble(0);
    }
    else
    {
      //lat
      dest.writeDouble(noteLocation.latitude);
      //lon
      dest.writeDouble(noteLocation.longitude);
    }
    dest.writeString(noteLocationName);
    dest.writeLong(timestamp);
    dest.writeLong(timestampUpdate);
  }

  protected Note(Parcel in)
  {
    this.noteId = in.readLong();
    this.noteTitle = in.readString();
    this.noteBody = in.readString();
    this.imageURI = in.readString();
    int pr = in.readInt();
    this.priority = NotePriority.getByPriority(pr);
    double lat = in.readDouble();
    double lon = in.readDouble();
    if (lat != 0 && lon != 0)
    {
      this.noteLocation = new LatLng(lat, lon);
    }
    this.noteLocationName = in.readString();
    this.timestamp = in.readLong();
    this.timestampUpdate = in.readLong();

  }

  public static final Creator<Note> CREATOR = new Creator<Note>() {
    @Override
    public Note createFromParcel(Parcel source) {
      return new Note(source);
    }

    @Override
    public Note[] newArray(int size) {
      return new Note[size];
    }
  };
}

