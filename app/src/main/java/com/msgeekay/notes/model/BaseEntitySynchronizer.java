package com.msgeekay.notes.model;

/**
 * Created by grigoriykatz on 11/05/17.
 */

public abstract class BaseEntitySynchronizer
{
  public enum SynchronizationState {
    stateOk,
    stateNeedsSync,
    stateFailedSync
  }

  private long entitySynchronizationState;
  private long entitySynchronizationTimestamp;

  public long getEntitySynchronizationState()
  {
    return entitySynchronizationState;
  }

  public void setEntitySynchronizationState(long entitySynchronizationState)
  {
    this.entitySynchronizationState = entitySynchronizationState;
  }

  public long getEntitySynchronizationTimestamp()
  {
    return entitySynchronizationTimestamp;
  }

  public void setEntitySynchronizationTimestamp(long entitySynchronizationTimestamp)
  {
    this.entitySynchronizationTimestamp = entitySynchronizationTimestamp;
  }
}
