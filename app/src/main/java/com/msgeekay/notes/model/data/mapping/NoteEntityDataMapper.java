package com.msgeekay.notes.model.data.mapping;

import com.msgeekay.notes.model.Note;
import com.msgeekay.notes.model.NotePriority;
import com.msgeekay.notes.model.data.NoteEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public class NoteEntityDataMapper
{
  public static List<Note> transform(Collection<NoteEntity> noteEntityCollection)
  {
    List<Note> noteList = new ArrayList<>();

    if (noteEntityCollection != null)
    {
      for (NoteEntity noteEntity : noteEntityCollection)
      {
        Note n = transform(noteEntity);
        if (n != null)
          noteList.add(n);
      }
    }

    return noteList;
  }

  public static Note transform(NoteEntity noteEntity)
  {
    Note retVal = null;

    if (noteEntity != null)
      retVal = new Note(noteEntity.getNoteId(), noteEntity.getNoteTitle(), noteEntity.getNoteBody(),
              noteEntity.getPriority(), noteEntity.getImageURI(),
              noteEntity.getNoteLocationLatitude(),
              noteEntity.getNoteLocationLongitude(),
              noteEntity.getNoteLocationName(), noteEntity.getTimestamp());

    return retVal;
  }

  public static NoteEntity transform(Note note)
  {
    NoteEntity retVal = null;

    if (note != null)
      retVal = new NoteEntity(note.getNoteId(), note.getNoteTitle(), note.getNoteBody(), note.getPriority().getId(),
              note.getImageURI(), note.getNoteLocation(), note.getNoteLocationName(),
              note.getTimestamp(),note.getTimestampUpdate());

    return retVal;
  }
}
