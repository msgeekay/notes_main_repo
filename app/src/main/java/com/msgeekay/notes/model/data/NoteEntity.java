package com.msgeekay.notes.model.data;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.msgeekay.notes.datalayer.db.NotesDatabase;
import com.msgeekay.notes.model.data.mapping.NoteEntityDataMapper;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by grigoriykatz on 17/09/17.
 */

@Table(database = NotesDatabase.class)
public class NoteEntity extends BaseModel
{
  @PrimaryKey(autoincrement=true)
  @SerializedName("id")
  @Column
  private long noteId;

  @Column
  @SerializedName("noteTitle")
  private String noteTitle;

  @Column
  @SerializedName("noteBody")
  private String noteBody;

  @Column
  @SerializedName("priority")
  private int priority;

  @Column
  @SerializedName("imageURI")
  private String imageURI;

  @Column
  @SerializedName("lon")
  private double noteLocationLongitude;

  @Column
  @SerializedName("lat")
  private double noteLocationLatitude;

  @Column
  @SerializedName("locationName")
  private String noteLocationName;

  @Column
  @SerializedName("timestamp")
  private long timestamp;

  @Column
  @SerializedName("timestampUpd")
  private long timestampUpdate;

  public NoteEntity()
  {

  }

  public NoteEntity(NoteEntity noteEntity)
  {
    this.noteId = noteEntity.noteId;
    this.noteTitle = noteEntity.getNoteTitle();
    this.noteBody = noteEntity.getNoteBody();
    this.priority = noteEntity.getPriority();
    this.imageURI = noteEntity.getImageURI();
    this.noteLocationLatitude = noteEntity.getNoteLocationLatitude();
    this.noteLocationLongitude = noteEntity.getNoteLocationLongitude();

    this.noteLocationName = noteEntity.getNoteLocationName();
    this.timestamp = noteEntity.getTimestamp();
    this.timestampUpdate = noteEntity.getTimestampUpdate();
  }

  public NoteEntity(long id, String noteTitle, String noteBody, int priority,
                    String imageURI, LatLng loc, String locationName,
                    long timestamp, long timestampUpdate)
  {
    this.noteId = id;
    this.noteTitle = noteTitle;
    this.noteBody = noteBody;
    this.priority = priority;
    this.imageURI = imageURI;
    if (loc == null)
    {
      this.noteLocationLatitude = 0;
      this.noteLocationLongitude = 0;
    }
    else
    {
      this.noteLocationLatitude = loc.latitude;
      this.noteLocationLongitude = loc.longitude;
    }

    this.noteLocationName = locationName;
    this.timestamp = timestamp;
    this.timestampUpdate = timestampUpdate;
  }

  public long getNoteId()
  {
    return noteId;
  }

  public void setNoteId(long noteId)
  {
    this.noteId = noteId;
  }

  public String getNoteTitle()
  {
    return noteTitle;
  }

  public void setNoteTitle(String noteTitle)
  {
    this.noteTitle = noteTitle;
  }

  public String getNoteBody()
  {
    return noteBody;
  }

  public void setNoteBody(String noteBody)
  {
    this.noteBody = noteBody;
  }

  public int getPriority()
  {
    return priority;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }

  public String getImageURI()
  {
    return imageURI;
  }

  public void setImageURI(String imageURI)
  {
    this.imageURI = imageURI;
  }

  public String getNoteLocationName()
  {
    return noteLocationName;
  }

  public void setNoteLocationName(String noteLocationName)
  {
    this.noteLocationName = noteLocationName;
  }

  public double getNoteLocationLatitude()
  {
    return noteLocationLatitude;
  }

  public void setNoteLocationLatitude(double noteLocationLatitude)
  {
    this.noteLocationLatitude = noteLocationLatitude;
  }

  public double getNoteLocationLongitude()
  {
    return noteLocationLongitude;
  }

  public void setNoteLocationLongitude(double noteLocationLongitude)
  {
    this.noteLocationLongitude = noteLocationLongitude;
  }

  public long getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(long timestamp)
  {
    this.timestamp = timestamp;
  }

  public long getTimestampUpdate()
  {
    return timestampUpdate;
  }

  public void setTimestampUpdate(long timestampUpdate)
  {
    this.timestampUpdate = timestampUpdate;
  }
}
