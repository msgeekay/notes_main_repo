package com.msgeekay.notes.model;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;

/**
 * Created by grigoriykatz on 17/09/17.
 */

public enum NotePriority
{
  none(0, Color.parseColor("#FFFFFF")),
  low(1, Color.parseColor("#fbff00")),
  middle(2, Color.parseColor("#3dff2f")),
  high(3, Color.parseColor("#fd3232"));

  private int id;
  private @ColorInt int color;

  NotePriority(int id, @ColorInt int color)
  {
    this.id = id;
    this.color = color;
  }

  public int getId() { return id; }
  public void setId(int n) { id = n; }

  public @ColorInt int getColor() { return color; }

  public static NotePriority getByPriority(int priority)
  {
    switch (priority)
    {
      case 0:
        return NotePriority.none;
        //break;
      case 1:
        return NotePriority.low;
        //break;
      case 2:
        return NotePriority.middle;
        //break;
      case 3:
        return NotePriority.high;
        //break;
      default:
        return NotePriority.none;
        //break;

    }
  }
}
